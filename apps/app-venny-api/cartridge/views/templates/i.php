<html>

  <body>

    <h1>It works!</h1>

    <ul>
      <li>CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
      <li>GRANT ALL PRIVILEGES ON *.* TO 'tweeter'@'localhost' IDENTIFIED BY 'B1@thering!';
      <li>GRANT type_of_permission ON database_name.table_name TO 'username'@'localhost';
      <li>GRANT ALL PRIVILEGES ON database_name.table_name TO 'username'@'localhost';
      <li>ALTER USER 'tweeter'@'localhost' IDENTIFIED BY 'LFrnq5u7vWYFWVwr';
      <li>SELECT User FROM mysql.user;
      <li>ALTER TABLE users ADD UNIQUE (username);
      <li>FLUSH PRIVILEGES;
<hr>
      <li>ALTER TABLE mytbl ADD UNIQUE (columnName);
      <li>ALTER TABLE `users` ADD INDEX `username` (`username`);
<hr>
      <li>mysqldump db_name > backup-file.sql
<hr>
      <li>mysql> use db_name;
      <li>mysql> source backup-file.sql;
    </ul>

    <ul>
      <li>Add eslint
      <li>You need PHP 5.5.0 or newer, with session support, the Standard PHP Library (SPL) extension, hash, ctype, and JSON support.
      <li>The mbstring extension (see mbstring) is strongly recommended for performance reasons.
      <li>To support uploading of ZIP files, you need the PHP zip extension.
      <li>You need GD2 support in PHP to display inline thumbnails of JPEGs (“image/jpeg: inline”) with their original aspect ratio.
      <li>When using the cookie authentication (the default), the openssl extension is strongly suggested.
      <li>To support upload progress bars, see 2.9 Seeing an upload progress bar.
      <li>To support XML and Open Document Spreadsheet importing, you need the libxml extension.
      <li>To support reCAPTCHA on the login page, you need the openssl extension.
      <li>To support displaying phpMyAdmin's latest version, you need to enable allow_url_open in your php.ini or to have the curl extension.
    </ul>

    <?php phpinfo(); ?>

  </body>

</html>
