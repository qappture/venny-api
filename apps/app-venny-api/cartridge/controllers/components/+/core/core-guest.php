<?php

	// Turns on output buffering
	ob_start();

	// Begins sessions
	session_start();

	//
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	//
	$timezone = date_default_timezone_set("America/New_York");

	//
  require 'form_handlers/register_handler.php';
  require 'form_handlers/login_handler.php';

	//
	require 'core/core-environment.php';
	require 'core/core-access.php';

	//
	require 'functions/function-core.php';
	require 'functions/function-object.php';
	require 'functions/function-session.php';
	require 'functions/function-user.php';

	//
  require 'properties/properties.php';

?>
