<?php

  //
  require 'classes/Edit.php';

  //
  $db_handle = new Edit();
  $message_obj = new Message($db, $userLoggedIn);

  //
  if(isset($_SESSION['username'])) {

    $userLoggedIn = $_SESSION['username'];
    $user_details_query = mysqli_query($db, "SELECT * FROM users WHERE username='$userLoggedIn'");
    $user = mysqli_fetch_array($user_details_query);

  }

  else {

    header("Location: register.php");

  }

  if(isset($_GET['alias'])) {
  	$username = $_GET['alias'];
  	$user_details_query = mysqli_query($db, "SELECT * FROM users WHERE username='$username'");
  	$user_array = mysqli_fetch_array($user_details_query);

  	$num_friends = (substr_count($user_array['friend_array'], ",")) - 1;
  }

  if(isset($_POST['remove_friend'])) {
  	$user = new User($db, $userLoggedIn);
  	$user->removeFriend($username);
  }

  if(isset($_POST['add_friend'])) {
  	$user = new User($db, $userLoggedIn);
  	$user->sendRequest($username);
  }
  if(isset($_POST['respond_request'])) {
  	header("Location: requests.php");
  }

  if(isset($_POST['post_message'])) {
    if(isset($_POST['message_body'])) {
      $body = mysqli_real_escape_string($db, $_POST['message_body']);
      $date = date("Y-m-d H:i:s");
      $message_obj->sendMessage($username, $body, $date);
    }

    $link = '#profileTabs a[href="#messages_div"]';
    echo "<script>
            $(function() {
                $('" . $link ."').tab('show');
            });
          </script>";


  }

  /////////////////
  $v = '1.5';

  if($v='') {

    $version = '?v='. $v;

  }

  else {

    $version = '';

  }
  /////////////////////

  /////////////////////
  if(empty($_SESSION['member_ID'])) {

    //
    $entitlement = getEntitlementsbyUserID($_SESSION['user_ID'],'user');
    $_SESSION['person_ID'] = $entitlement['person'];
    $_SESSION['guest_ID'] = $entitlement['guest'];
    $_SESSION['user_ID'] = $entitlement['user'];
    $_SESSION['member_ID'] = $entitlement['member'];
    $_SESSION['profile_ID'] = $entitlement['profile'];
    $_SESSION['partner_ID'] = $entitlement['partner'];

    //echo 'member: ' . $_SESSION['member_ID'] . '<br/>';
    //echo 'partner: ' . $_SESSION['partner_ID'] . '<br/>';

    //exit;

  }
  /////////////////////

  //
  $hostname = APP_ENV_SRVR;
  $path = APP_ST_NAME;
  $version = "v1"."/";
  $resource = "member";
  $key = APP_API_KEY;

  //
  if(!isset($_GET['alias'])){$alias=$_SESSION['username'];}else{$alias=$_GET['alias'];}

  $api = $hostname . $path . $version . $resource . "?" . "token=" . $key . "&" . "alias=" . $alias;
  //echo $api;
  //exit;
  //
  $jsonData = file_get_contents($api);
  //print_r($jsonData);
  //exit;

  $user = json_decode($jsonData,true);
  //echo $user;
  //exit;

  $user_ID = $_SESSION['user_ID'];

  //
  $my_ID = $_SESSION['member_ID'];
  $their_ID = $user['results'][0]['member']['member_ID'];

  //
  if($my_ID == $their_ID) {

    $mine = true;

  }

  else {

    $mine = NULL;

  }

  $pic_results = query("SELECT profile_pic FROM users WHERE user_ID = '$user_ID'");
  $userPic = fetch_array($pic_results);
  //print_r($userPic);exit;
  $profile_pic = getImagebyCDN($userPic['profile_pic']);

?>
