<?php

use Aws\S3\Exception\S3Exception;

//
require 'aws/start.php';

	//
	if(isset($_POST['upload'])) {

		//
		if(isset($_REQUEST['type'])){$image_type = $_REQUEST['type'];}
		if(isset($_REQUEST['image'])){$image_ID = $_REQUEST['image'];}
		if(isset($_REQUEST['object'])){$object_ID = $_REQUEST['object'];}
		if(isset($_SESSION['member_ID'])){$member_ID = $_SESSION['member_ID'];}
		if(isset($_REQUEST['caption'])){$image_caption = clean($_REQUEST['caption']);}

		//$image_ID
		$image_type;
		$image_status = 1;
		$image_primary = 0;
		//$object_ID
		//$image_caption = NULL;
		//$image_filename = NULL;
		$image_location = NULL;
		$image_latitude = 0;
		$image_longitude = 0;
		//$member_ID

		$image_extension = @explode('/', $_FILES['file']['type']);
		//echo $image_extension[1];
		//exit;
		$extension = $image_extension[1]; //file type

		// Upload directory
		$upload_directory = APP_ENV_ROOT.APP_ST_ALIS.APP_ST_INCL.APP_ST_UPLD;
		$download_directory = APP_ENV_SRVR.APP_ST_NAME./*APP_ST_INCL.*/APP_ST_UPLD;
		//$upload_directory =

		//Check if the file is well uploaded
		if($_FILES['file']['error'] > 0) {

			die('Error during uploading, try again');

		}

		if($_FILES['file']['size'] > 8000000) {

			//echo ini_get('upload_max_filesize');
	    die('Whoops... File uploaded exceeds maximum upload size.');

		}

		//One of the best things to do when uploading is to ensure that it is an actual image that is being uploaded, you can do this by passing the uploaded file through a PHP image function such as getimagesize(), this will return false if the file is not an image:
		if(!getimagesize($_FILES['file']['tmp_name'])) {

	    die('Please ensure you are uploading an image.');

		}

		//
		list($image_width, $image_height, $image_kind, $image_attr) = getimagesize($_FILES['file']['tmp_name']);

		//We won't use $_FILES['file']['type'] to check the file extension for security purpose

		//Set up valid image extensions
		$extsAllowed = array('jpg','jpeg','png','gif');

		//Extract extention from uploaded file
			//substr return ".jpg"
			//Strrchr return "jpg"

		$extUpload = strtolower(substr(strrchr($_FILES['file']['name'], '.') ,1) ) ;
		//Check if the uploaded file extension is allowed

		//
		if (in_array($extUpload, $extsAllowed)) {

			//
			$image_filename = $member_ID . "_" . $object_ID . "_" . $image_ID . "." . $extension;

			//Upload the file on the server
			$upload = $upload_directory . $image_type . "/" . $image_filename;//$_FILES['file']['name'];
			$download = $download_directory . $image_type . "/" . $image_filename;
			$result = move_uploaded_file($_FILES['file']['tmp_name'], $upload);
			chmod($upload, 0777);

			try {
				$s3->putObject([
					'Bucket' => $config['s3']['bucket'],
					'Key' => "v1/images/posts/{$image_filename}",
					'Body' => fopen($upload,'rb'),
					'ACL' => "public-read"
				]);

				// Remove the file...
				unlink($upload);

			} catch(S3Exception $e){
				die("There was an error uploading this file.");
			}

			// Add to database
			$query = query(

				"INSERT INTO images (

					image_ID,
					image_type,
					image_status,
					image_primary,
					image_object,
					image_caption,
					image_filename,
					image_location,
					image_latitude,
					image_longitude,
					member_ID

						) VALUES (

					'$image_ID',
					'$image_type',
					'$image_status',
					'$image_primary',
					'$object_ID',
					'$image_caption',
					'$image_filename',
					'$image_location',
					'$image_latitude',
					'$image_longitude',
					'$member_ID'

				)"

			);

			$add_image_query = query("UPDATE posts SET images=CONCAT(images, '$image_ID,') WHERE post_ID='$object_ID'");

			if($result) {

				echo $image_filename . "<br />";
				echo "Width: " . $image_width . "<br />";
				echo "Height: " . $image_height . "<br />";
				echo "Kind: " .$image_kind. "<br />";
				echo "Attribute: " .$image_attr. "<br />";
				echo "<img src='http://gradient-cdn.imgix.net/images/posts/{$image_filename}' width='300' />";
				$_SESSION['image_ID'] = $image_ID;

			}

		} else {

			echo 'File is not valid. Please try again';

		}

	}

?>
