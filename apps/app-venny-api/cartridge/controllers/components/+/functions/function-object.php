<?php

	/**************************************/


	/**************************************/

  // Create URL dynanically. Needed because we're not sure what the URLs will look like...
  function getURL($scope,$domain,$parameters) {

		// Sometimes parameters will be sent sometimes not. If not, don't include them in the constructed URL.
		if(!empty($parameters)) {

    	$URL = 'template' . '-' . $scope . '-' . $domain . '.php' . '?' . $parameters;

		}

		else {

			$URL = 'template' . '-' . $scope . '-' . $domain . '.php';

		}

    //
    //
    //
    // Target

    /*

    // Looks like
    // <?php link($user,$home); ?>

    */

    echo $URL;

  }

  function site($resource) {

    $url = APP_ST_URL . $resource . '.php';

    return $url;

  }

  // Creates redirect links throughout the website. We needed an easy way to replace URLs when the .htaccess updates (URL shortening) happen
  function gotoURL($scope,$domain,$parameters) {

    $scope = htmlentities($scope);
    $domain = htmlentities($domain);
    $parameters = htmlentities($parameters);

    // if parameters exist then append to the URL... if not, don't...
    if(!empty($parameters)) {

      $URL = 'template' . '-' . $scope . '-' . $domain . '.php' . '?' . $parameters;

    }

    else {

      $URL = 'template' . '-' . $scope . '-' . $domain . '.php';

    }

    redirect($URL);

  }

  //
  function img($url,$width,$height,$caption = NULL) {

    if($width==0){$width='';}else{$width='width="' . $width . '" ';}
    if($height==0){$height='';}else{$height='height="' . $height . '" ';}

    //
    $output  = '';
    $output .= '<img ';
    $output .= 'src="' . $url . '" ';
    $output .= $width;
    $output .= $height;
    $output .= 'alt="' . $caption . '" ';
    $output .= ' />';

    return $output;

  }

  // Create URL dynanically. Needed because we're not sure what the URLs will look like...
  function gotURL($scope,$domain,$parameters) {

		// Sometimes parameters will be sent sometimes not. If not, don't include them in the constructed URL.
		if(!empty($parameters)) {

    	$URL = 'template' . '-' . $scope . '-' . $domain . '.php' . '?' . $parameters;

      /*
      gotURL('user','notes','key1=value1,key2=value2,key3=value3');
      - split pairs based on ","
      - then split values from keys using "="
      - echo "?" before printing first parameter
      - the echo
      */

		}

		else {

			$URL = 'template' . '-' . $scope . '-' . $domain . '.php';

		}

    echo $return;

  }

  // takes legacy full profile image path and converts for use on new cdn
  function getImagebyCDN($filename,$type='profiles') {

    if(isset($filename) && $filename!=NULL && $filename!='') {

      //
      //$image = explode('/',$path);
      //$image = end($image);

      //echo fetch_CDN().'images'.'/'.$type.'/'.$path.'?trk=maker';
      //$image_url = '_assets/'.'images'.'/'.$type.'/'.$filename.'?trk=maker';
      $image_url = APP_ST_CDN . 'images' . '/' . $type . '/' . $filename . '?trk=CDN';

      return $image_url;

    }

    else {

      return NULL;

    }

  }

  // retrieve existing hastags
  function gethashtags($msg) {

    // Match the hashtags
    preg_match_all('/(^|[^a-z0-9_])#([a-z0-9_]+)/i', $msg, $matchedHashtags);
    $hashtag = '';

    // For each hashtag, strip all characters but alnum
    if(!empty($matchedHashtags[0])) {

      foreach($matchedHashtags[0] as $match) {

        $hashtag .= preg_replace("/[^a-z0-9]+/i", "", $match).',';

      }

    }

    //to remove last comma in a string
    return rtrim($hashtag, ',');

  }

  // Converts on ['body'] content to social content like #hashtags and http://links.com
  function converttoSocial($body) {

    /*
    $regex = "/#+([a-zA-Z0-9_]+)/";

    $body = preg_replace($regex, '<a href="template-user-tag.php?tID=$1">$0</a>', $str);

    return($body);
    */

    $parsedMessage = preg_replace(array('/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/', '/(^|[^a-z0-9_])@([a-z0-9_]+)/i', '/(^|[^a-z0-9_])#([a-z0-9_]+)/i'), array('<a href="$1" target="_blank" rel="nofollow">$1</a>', '$1<a href="">@$2</a>', '$1<a href="template-user-tag.php?tID=$2">#$2</a>'), $body);
    return $parsedMessage;

  }

  // conver_links from captured note to clickable links.
  /*function convert_links($message) {

    $parsedMessage = preg_replace(array('/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))/', '/(^|[^a-z0-9_])@([a-z0-9_]+)/i', '/(^|[^a-z0-9_])#([a-z0-9_]+)/i'), array('<a href="$1" target="_blank" rel="nofollow">$1</a>', '$1<a href="">@$2</a>', '$1<a href="index.php?hashtag=$2">#$2</a>'), $message);
    return $parsedMessage;

  }
  */

  //
  function getMentions($content) {

    //
    global $DB;

    $mention_regex = '/@\[([a-zA-Z0-9]+)\]/i'; //mention regrex to get all @texts

    //
    if (preg_match_all($mention_regex, $content, $matches)) {

      //
      foreach ($matches[1] as $match) {

        $match_user = $DB->row("SELECT * FROM users WHERE person_ID = ?",array($match));

        $match_search = '@[' . $match . ']';

        $match_replace = '<a target="_blank" href="' . $match_user['username'] . '">@' . $match_user['username'] . '</a>';

        //
        if (isset($match_user['person_ID'])) {

          $content = str_replace($match_search, $match_replace, $content);

        }

      }

    }

    return $content;

  }

  //
  function assets($group,$type,$resource) {

    //$group
    //$type
    //$resource

    if(!empty($type)) {
      $type = $type . '/';
    }else{
      $type = '';
    }

    $url = '_assets/web/' . $group . '/' . $type . $resource;

    echo $url;

  }

  //
  function fetch_assets($domain,$type,$resource) {

    // classifications
    $domain; // image, script, style, etc.
    $type; // directory inside of domain
    $resource; // filename being requested

    //
    if(!empty($type)) {

      $type = $type . '/';

    }

    else {

      $type = '';

    }

    $url = APP_ST_CDN . $group . '/' . $type . $resource;

    echo $url;

  }

  //
  function fetch_CDN() {

    $url = APP_ST_CDN;

    echo $url;

  }

  //
  function fetch_asset() {

    $url = APP_ST_URL . '_assets/';

    echo $url;

  }

  //
  function getObjectImages($member,$object,$type) {

    $member;
    $object;
    $type;

    $sql = "SELECT image_ID,image_type,image_filename FROM images WHERE member_ID = '{$member}' AND image_object = '{$object}' AND image_type = '{$type}' AND active = 1 ";

    $get_images = query($sql);

    $images = fetch_array($get_images);

    foreach ($images as $key => $value) {
      # code...
    }

    echo json_encode($images);

  }

  function getImagesofProject($project_ID) {

    //
    $sql_project_images = query("SELECT project_images FROM projects WHERE project_ID = '{$project_ID}' ORDER BY project_images ASC ");
    $project_image = fetch_array($sql_project_images);
    $project_images_list = $project_image['project_images'];

    //echo $project_images;
    //exit;

    $project_images_list = trim($project_images_list,','); // remove outward commas

    //echo $project_images;
    //exit;

    $project_images = explode(',',$project_images_list); // you have array now

    //echo $project_image;
    //exit;

    krsort($project_images); // forces friends to return with the latest friend first

    //print_r($friends);

    //exit;

    //echo "<ul id='" . $project_ID . "_images' class='object_images'>";

    //
    foreach($project_images as $project_image) {

      echo "<img src='";
      getImage($project_image,0,0);
      echo "'/>";

    }

    //echo '</ul>';

  }

  //function getImage($image_ID) {
  function getImage($image_ID,$image_width,$image_height) {

    $p = '';
    if(!$image_width==0 || !$image_height==0){$p.='?';}
    if($image_width==0){$p.='';}else{$p.='w='.$image_width;}
    if(!$image_width==0 && !$image_height==0){$p.='&';}
    if($image_height==0){$p.='';}else{$p.='h='.$image_height;}
    if($image_width==0 && $image_height==0){$p='';}

    $sql_image = "SELECT image_type,image_filename FROM images WHERE image_ID = '{$image_ID}' AND active = 1 LIMIT 1";

    $get_image = query($sql_image);

    $image = fetch_array($get_image);

    $image_hostname = APP_ST_CDN;
    $image_group = 'images' . '/';
    $image_type = $image['image_type'] . '/';
    $image_filename = $image['image_filename'];

    $image_url = $image_hostname . $image_group . $image_type . $image_filename . $p;

    echo $image_url;

  }

  function domain_ID($domain,$trimby = -1) {

    if(isset($trimby)) {
      //$trimby=$trimby;
    }else{
      $trimby=-1;
    }

    $domain = substr($domain,0,$trimby) . "_ID";

    return $domain;

  }

?>
