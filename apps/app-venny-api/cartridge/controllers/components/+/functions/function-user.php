<?php

	//
	function clean($string) {

		//return htmlentities($string);
		return $string;

	}

	//
	function cleanforsearch($string) {

		if($string == "" || $string == NULL) {

			//echo "Your value is blank.";
			return $string;

		}

		$string = addslashes($string);

		//echo "CLEAN!";
		return urlencode(htmlentities($string));

	}

	//
	function redirect($location) {

		return header("Location: " . APP_ST_NAME . $location);

	}

	//
	function set_message($message) {

		if(!empty($message)) {

			$_SESSION['message'] = $message;

		}

		else {

			$message = "";

		}

	}


	//
	function display_message() {

		if(isset($_SESSION['bb_message'])) {

			echo $_SESSION['bb_message'];

			unset($_SESSION['bb_message']);

		}

	}

	/*
	//
	function confirm() {

		if(isset($_SESSION['bb_message'])) {

			echo $_SESSION['bb_message'];

			unset($_SESSION['bb_message']);

		}

	}
	*/

	//
	function token_generator() {

		$token = $_SESSION['bb_token'] = md5(uniqid(mt_rand(), true));

		return $token;

	}

	//
	function validation_errors($error_message) {

		$error_message =
<<<DELIMITER
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
  <strong>Warning!</strong>
	$error_message
</div>
DELIMITER;

		return $error_message;

	}

	//
	function email_exists($email) {

		$sql = "SELECT id FROM users WHERE email = '$email'";

		$result = query($sql);

		if(row_count($result) == 1 ) {

			return true;

		}

		else {

			return false;

		}

	}

	//
	function email_valid($email) {

		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

			return true;

		}

	}

	//
	function username_exists($username) {

		$sql = "SELECT id FROM users WHERE username = '$username' LIMIT 1";

		$result = query($sql);

		if(row_count($result) == 1 ) {

			return true;

		}

		else {

			return false;

		}

	}

	/****************Validation functions ********************/

	//
	function validate_user_registration() {

		$errors = [];

		$min = 3;
		$max = 30;

		if($_SERVER['REQUEST_METHOD'] == "POST") {

			$first_name = clean($_POST['first_name']);
			$first_name = strtolower($first_name); // Uppercase first letter

			/*
			$last_name = clean($_POST['last_name']);
			$last_name = strtolower($last_name); // Uppercase first letter
			*/

			$username = clean($_POST['username']); // Clean
			$username = strtolower($username); // Uppercase first letter
			$username = str_replace(' ', '', $username); // Remove spaces

			$email = clean($_POST['email']);
			$password	= clean($_POST['password']);
			//$confirm_password	= clean($_POST['confirm_password']);

			$_SESSION['ghost_first_name'] = $first_name; //Stores first name into session variable
			/* $_SESSION['last_name'] = $last_name; //Stores first name into session variable */
			$_SESSION['ghost_email'] = $email; //Stores first name into session variable
			$_SESSION['ghost_username'] = $username; //Stores first name into session variable
			// using the ghost_ prefix so as not to be confused with the ['email'] permanent session variable

			//
			if(strlen($first_name) < $min) {

				$errors[] = "Your first name cannot be less than {$min} characters";

			}

			//
			if(strlen($first_name) > $max) {

				$errors[] = "Your first name cannot be more than {$max} characters";

			}

			//
			if(preg_match('/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/', $username)) {
			//if(preg_match('/[^A-Za-z0-9]/', $password)) {

				$errors[] = "Your username cannot contain any special characters";

			}

			//
			if(!preg_match('/^(?=.*\d)(?=.*\W+)(?=.*[a-z])(?=.*[A-Z])/', $password)) {
			//if(preg_match('/[^A-Za-z0-9]/', $password)) {

				$errors[] = "Your password must contain at least 1 uppercase letter, 1 lowercase letter, 1 number and 1 special chracter";

			}

			//
			if(strlen($password > 30 || strlen($password) < 6)) {

				$errors[] = "Your password must be between 6 and 30 characters";

			}

			/* Last name will not be used as part of onboarding
			if(strlen($last_name) < $min) {

				$errors[] = "Your Last name cannot be less than {$min} characters";

			}
			*/

			/* Last name will not be used as part of onboarding
			if(strlen($last_name) > $max) {

				$errors[] = "Your Last name cannot be more than {$max} characters";

			}
			*/

			//
			if(strlen($username) < $min) {

				$errors[] = "Your Username cannot be less than {$min} characters";

			}

			if(strlen($username) > 15) { // since I'll be using Twitter oAuth

				$errors[] = "Your Username cannot be more than 15 characters";

			}

			if(username_exists($username)) {

				$errors[] = "Sorry that username is already is taken";

			}

			if(!email_valid($email)) {

				//Check if email is in valid format
				if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

					$email = filter_var($email, FILTER_VALIDATE_EMAIL);

				}

				$errors[] = "Sorry that email is not formatted correctly";

			}

			if(email_exists($email)) {

				$errors[] = "Sorry that email already is registered";

			}

			if(strlen($email) < $min) {

				$errors[] = "Your email cannot less than {$min} characters";

			}

			if(strlen($email) > $max) {

				$errors[] = "Your email cannot be more than {$max} characters";

			}

			/* Confirm password may not be NEEDED.
			if($password !== $confirm_password) {

				$errors[] = "Your password fields do not match";

			}
			*/

			if(!empty($errors)) {

				foreach ($errors as $error) {

					echo validation_errors($error);

				}

			}

			else {

				/* if(register_user($first_name, $last_name, $username, $email, $password)) { */
				if(register_user($first_name,$username,$email,$password)) {

				set_message("<div class='alert alert-success text-center alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Please check your email or spam folder for an activation link</div>");

				$_SESSION['ghost_first_name'] = '';
				$_SESSION['ghost_username'] = '';
				$_SESSION['ghost_email'] = '';
				/* $_SESSION['ghost_last_name'] = ""; */

				//redirect("them.php");
				gotoURL('guest','sign-in',NULL);

				}

				else {

					set_message("<div class='alert alert-danger text-center alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Sorry we could not register the user</div>");

					//redirect("index.php");
					gotoURL('guest','sign-in','TRK=validate_user_registration_elseif');

				}

			}

		} // post request

	} // function

	/**************** Register user functions ********************/

	function register_user($first_name,$username,$email,$password) {
	/* function register_user($first_name, $last_name, $username, $email, $password) { */

		$first_name = escape($first_name);
		/* $last_name = escape($last_name); */
		$username = escape($username);
		$email = escape($email);
		$password = escape($password);

		$signup_date = ""; //Sign up date
		$signup_date = date("Y-m-d h:i:sa"); //Current date

		// Profile picture assignment
		$rand = rand(1, 2); // Random number between 1 and 2

		if($rand == 1) {

			$profile_pic = "head_deep_blue.png";

		}

		else if($rand == 2) {

			$profile_pic = "head_emerald.png";

		}

		if(email_exists($email)) {

			return false;

		}

		else if (username_exists($username)) {

			return false;

		}

		else {

			$password = md5($password);

			// Create person_ID to replace username through scope
			$person_ID = substr(md5(uniqid(microtime(true),true)),0,8);

			$validation_code = md5($username + microtime());

			$sql = "INSERT INTO users (id,first_name,username,email,password,validation_code,user_closed,signup_date,profile_pic,num_posts,num_likes,num_notes,friend_array,active,person_ID)";
			//$sql .= " VALUES (NULL,'$first_name','$username','$email','$password','$validation_code','yes',NOW(),'$profile_pic','0','0','0',',','0','$person_ID')";
			$sql .= " VALUES (NULL,'$first_name','$username','$email','$password','0','no',NOW(),'$profile_pic','0','0','0',',','1','$person_ID')";
			$result = query($sql);
			confirm($result);

			// Event
			//$event_ID = $person_ID;
			//$event_type = __FUNCTION__;
			//$event_user = $username;
			//$event_closed = date("Y-m-d h:i:s");
			//$event = mysqli_query($this->con, "INSERT INTO events (ID,event,type,user,closed) VALUES (NULL, '$event_ID', '$event_type', '$event_user', '$event_closed')");
			//$event_ID = 'Person';
			//$event_type = 'Register';
			//$event_user = 'Qappture';
			//$event_closed = date("Y-m-d h:i:sa");
			//$event = mysqli_query($this->con, "INSERT INTO events (ID,event,type,user,closed) VALUES (NULL, '$event_ID', '$event_type', '$event_user', NOW())");

			$user_first_name = $first_name;
			$user_username = $username;
			$user_validation_code = $validation_code;
			$user_email = $email;

			//$subject = 'Confirm your Bribe account now, ' . $user_first_name . " " . date("Y-m-d h:i:sa");
			$subject = 'Confirm your Bribe account now, ' . ucwords($user_first_name);
			$message =
<<<DELIMITER
											<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'><html style='width:100%;padding:0px;margin:0px;'> <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><title>Confirm your Notearise account now</title><style type="text/css"></style></head><body style='width:100%;padding:0px;margin:0px;background-color:none;'> <div style='width:100%;'> <table style='width:100%;background-color:none;' border='0'> <tbody style='width:100%;'> <tr style='width:100%;'> <td style='width:100%;'> <!--// NZDV830003 - BEGIN //--> <table style="width:100%;max-width:768px;background-color:#eeeeee;padding:20px;margin:0 auto;color:#9b9b9b;font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;" border='0'> <tbody style='width:100%;max-width:768px;'> <tr style='width:100%;max-width:768px;'> <td>&nbsp;</td> <td style='width:100%;max-width:768px;'> <center> <a href='https://www.bribe.bar' target='_blank' title='Notearise'><img width='30' src='https://www.bribe.bar/static/images/email/notearise-email-logo.png' alt='Notearise'></a> </center> <br/> <!--// NZDV830003 - BEGIN //--> <table style='width:100%;max-width:768px;background-color:#ffffff;padding:12px 25px;margin:0;border-radius:5px;border:1px solid #dddddd;'> <tbody style='width:100%;max-width:768px;'> <tr style='width:100%;max-width:768px;'> <td style='width:100%;max-width:768px;'> <span><h1>Final Step...</h1></span> <span><h2>$user_first_name, please confirm your email address to complete your Notearise account <a style='text-decoration:underline;color:inherit;' href='https://www.bribe.bar/$user_username'>@$user_username</a>.</h2></span> <span>It's easy - just <a href='https://www.bribe.bar/template-guest-activate.php?code=$user_validation_code&email=$user_email' style='text-decoration:underline;color:inherit;' target='_blank'>click the link below</a> to confirm.</span><br/> <span style='width:100%;'><a style='border-radius:5px;text-align:center;background-color:#e42e55;width:200px;color:#ffffff;border:0px solid #348eda;display:inline-block;padding:20px 25px;margin:10px auto;text-align:center;font-weight:bold;text-decoration:none;' href='https://www.bribe.bar/template-guest-activate.php?code=$user_validation_code&email=$user_email' target='_blank'>Confirm Now</a></span><br/> <span style='color:inherit;'>Thank you for choosing Notearise!</span><br/><br/> <span style='font-size:0.75em;text-align:center;width:100%;'>&copy; 2017 Notearise, Inc. All rights reserved.</span><br/><br/> <span style='font-size:0.75em;text-align:center;width:100%'><a href='http://facebook.com/notearise' style='text-decoration:underline;color:inherit;' target='_blank'>Contact Us</a> &#183; <a href='https://www.bribe.bar/terms-of-service' style='text-decoration:underline;color:inherit;' target='_blank'>Terms of Use</a> &#183; <a href='https://www.bribe.bar/privacy-policy' style='text-decoration:underline;color:inherit;' target='_blank'>Privacy Policy</a></span><br/><br/> </td> </tr> </tbody> </table> <!--// NZDV830003 - END //--> </td> <td>&nbsp;</td> </tr> </tbody> </table> <!--// NZDV830003 - END //--> </td> </tr> </tbody> </table> </body></html>
DELIMITER;

			$email_subject = $subject;
			$email_message = $message;

			// Send email with Mailgun
			send_email($user_email,$user_first_name,$user_username,$user_validation_code,$email_subject,$email_message);



























































			return true;

		}

	}

/****************Activate user functions ********************/

	function activate_user() {

		if($_SERVER['REQUEST_METHOD'] == "GET") {

			if(isset($_GET['email'])) {

				$email = clean($_GET['email']);

				$validation_code = clean($_GET['code']);

				$sql = "SELECT id FROM users WHERE email = '" . escape($_GET['email']) . "' AND validation_code = '" . escape($_GET['code']) . "' LIMIT 1";
				$result = query($sql);
				confirm($result);

				if(row_count($result) == 1) {

					$sql2 = "UPDATE users SET active = 1, user_closed = 'no', validation_code = 0 WHERE email = '" . escape($email) . "' AND validation_code = '" . escape($validation_code) . "'";
					$result2 = query($sql2);
					confirm($result2);

					set_message("<div class='alert alert-success alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Your account has now been activated</div>");

					gotoURL('guest','sign-in','activated');

				}

				else {

					set_message("<div class='alert alert-danger alert-dismissible'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Sorry your account could not be activated</div>");

					gotoURL('guest','sign-in','notyetactivated'.$validation_code.$email);

				}

			}

		}

	} // function

/**************** Validate user login functions ********************/

	function validate_user_login() {

		$errors = [];

		$min = 3;
		$max = 30;

		//
		if($_SERVER['REQUEST_METHOD'] == "POST") {

			$login = clean($_POST['login']);
			$password = clean($_POST['password']);
			$remember = isset($_POST['remember']);

			$_SESSION['ghost_login'] = $login; //Stores login information for login to prevent retyping on failure

			//
			if(empty($login)) {

				$errors[] = "Email or Username field cannot be empty";

			}

			//
			if(empty($password)) {

				$errors[] = "Password field cannot be empty";

			}

			//
			if(!empty($errors)) {

				foreach ($errors as $error) {

					echo validation_errors($error);

				}

			}

			else {

				//
				if(login_user($login, $password, $remember)) {

					// update last login...
					$UUID = $_SESSION['bb_UUID'];
					$update_last_login = query("UPDATE users SET last_login = NOW() WHERE person_ID = '$UUID'");

					//set_message('Logged last on: '. $last_login);
					gotoURL('user','home','TRK=login_user');

				}

				else {

					echo validation_errors("There was a problem with your login credentials");

				}

			}

		}

	} // function

/**************** User login functions ********************/

	function login_user($login, $password, $remember) {

		$sql = "SELECT password,id,username,active,person_ID,user_closed FROM users WHERE ( username = '" . escape($login) . "' OR email = '" . escape($login) . "' ) LIMIT 1";

		/*

			OLD $sql = "SELECT password,id,username,active,person_ID FROM users WHERE email = '" . escape($email) . "'";
			Changes login to allow user to login with
			either their username or password. A change
			will need to be made to the HTML validation
			on the login page as well considering the
			login form is expecting email.
			https://stackoverflow.com/questions/10419137/login-with-username-or-email-address-in-php

		*/

		$result = query($sql);

		//
		if(row_count($result) == 1) {

			unset($_SESSION['bb_UUID']);

			unset($_SESSION['bb_username']);

			$row = fetch_array($result);

			$UUID = $row['person_ID'];
			$username = $row['username'];
			$user_closed = $row['user_closed'];
			$db_password = $row['password'];

			//
			if(md5($password) === $db_password) {

				//
				if($row['active'] == 1) {

					// open closed accounts for backsliders
					if($user_closed == 'yes') {

						//echo 'SHHHEEEEEEEIT';
						$reopen_account = mysqli_query($db, "UPDATE users SET user_closed = 'no' WHERE uuid = '$UUID'");

					}

					//
					if($remember == "on") {

						$cookie_name = 'bb_UUID';
						$cookie_value = $UUID;
						$cookie_expire = time() + 60 * 60 * 24 * 30; // 60 seconds * 60 minutes * 24 hours * 30 days
						$cookie_path = ''; // MUST UPDATE FOR PROD $_SERVER['SERVER_NAME']
						$cookie_domain = ''; // MUST UPDATE FOR PROD $_SERVER['HTTP_HOST']
						//$cookie_path = '/bribe.bar/'; // MUST UPDATE FOR PROD $_SERVER['SERVER_NAME']
						//$cookie_domain = 'localhost'; // MUST UPDATE FOR PROD $_SERVER['HTTP_HOST']
						$cookie_secure = TRUE; // should be true in PROD
						$cookie_httponly = TRUE;

						unset($_COOKIE[$cookie_name]);

						setcookie(

							$cookie_name,
							$cookie_value,
							$cookie_expire,
							$cookie_path,
							$cookie_domain,
							$cookie_secure,
							$cookie_httponly

						);

					}

					//unset($_SESSION[$UUID]);
					$_SESSION['bb_UUID'] = $UUID;

					//unset($_SESSION[$username]);
					$_SESSION['bb_username'] = $username;
					//$_SESSION['username'] = $username;

					return true;

				}

				else {

					echo validation_errors("Before logging in, you'll need to check your email to activate your account");

					return false;

				}

			}

			else {

				return false;

			}

			return true;

		}

		else {

			return false;

		}

	} // end of function

	/**************** logged in function ********************/

	function logged_in() {

		//if(isset($_SESSION['bb_UUID']) || isset($_COOKIE['bb_UUID'])) {
		if(isset($_SESSION['bb_UUID'])) {

			set_message("<div class='alert-success text-center alert-dismissible'>You are now logged in.</div>");

			return true;

		}

		else {

			return false;

		}

	}	// functions

	/**************** Recover Password function ********************/

	function recover_password() {

		//
		if($_SERVER['REQUEST_METHOD'] == "POST") {

			//
			if(isset($_SESSION['bb_token']) && $_POST['bb_token'] === $_SESSION['bb_token']) {

				$email = clean($_POST['email']);

				//
				if(email_exists($email)) {

					//$validation_code = md5($email + microtime());
					$validation_code = substr(md5(uniqid(microtime(true),true)),0,6);

					//setcookie('h0tNw4jNp28', $validation_code, time() + 86400);
					setcookie('bb_temp_access_code', $validation_code, time() + 86400);

					$sql_update_validation_code = "UPDATE users SET validation_code = '".escape($validation_code)."' WHERE email = '".escape($email)."'";
					$sql_update_validation_code_result = query($sql_update_validation_code);

					// BEGIN - Get user data
					$get_user_data_query = "SELECT first_name,username FROM users WHERE email = '$email' LIMIT 1";
					$get_user_data_result = query($get_user_data_query);
					$get_user_data_array = mysqli_fetch_array($get_user_data_result);

					$username = $get_user_data_array['username'];
					$first_name = $get_user_data_array['first_name'];
					$user_username = $username;
					$user_first_name = $first_name;

					// END - Get user data

					$subject = "Reset your Notearise password, " . ucwords($user_first_name);
					$message =
<<<DELIMITER
													<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'><html style='width:100%;padding:0px;margin:0px;'> <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><title>Reset your Notearise password</title><style type="text/css"></style></head><body style='width:100%;padding:0px;margin:0px;background-color:none;'> <div style='width:100%;'> <table style='width:100%;background-color:none;' border='0'> <tbody style='width:100%;'> <tr style='width:100%;'> <td style='width:100%;'> <!--// NZDV830004 - BEGIN //--> <table style="width:100%;max-width:768px;background-color:#eeeeee;padding:20px;margin:0 auto;color:#9b9b9b;font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;" border='0'> <tbody style='width:100%;max-width:768px;'> <tr style='width:100%;max-width:768px;'> <td>&nbsp;</td> <td style='width:100%;max-width:768px;'> <center> <a href='https://www.bribe.bar' target='_blank' title='Notearise'><img width='30' src='https://www.bribe.bar/static/images/email/notearise-email-logo.png' alt='Notearise'></a> </center> <br/> <!--// NZDV830003 - BEGIN //--> <table style='width:100%;max-width:768px;background-color:#ffffff;padding:12px 25px;margin:0;border-radius:5px;border:1px solid #dddddd;'> <tbody style='width:100%;max-width:768px;'> <tr style='width:100%;max-width:768px;'> <td style='width:100%;max-width:768px;'> <span><h1>Reset your password?</h1></span> <span><h2>{$user_first_name}, if you requested a password reset for <a style='text-decoration:underline;color:inherit;' href='https://www.bribe.bar/$user_username'>@{$user_username}</a>, click the button below and the validation code. If you didn't make this request, ignore this email.</h2></span> <span>Please click the link below <a href='https://www.bribe.bar/template-guest-enter-code.php?email={$email}&code={$validation_code}' style='text-decoration:underline;color:inherit;' target='_blank'>to reset your password</a>. The link will only be active for 24 hours from now.</span><br/><br/> <span>Validation Code: <b>{$validation_code}</b></span><br/><br/> <span style='width:100%;'><a style='border-radius:5px;text-align:center;background-color:#e42e55;width:200px;color:#ffffff;border:0px solid #348eda;display:inline-block;padding:20px 25px;margin:10px auto;text-align:center;font-weight:bold;text-decoration:none;' href='https://www.bribe.bar/template-guest-enter-code.php?email={$email}&code={$validation_code}' target='_blank'>Reset Password</a></span><br/> <span style='color:inherit;'>Thank you for choosing Notearise!</span><br/><br/> <span style='font-size:0.75em;text-align:center;width:100%;'>&copy; 2017 Notearise, Inc. All rights reserved.</span><br/><br/> <span style='font-size:0.75em;text-align:center;width:100%'><a href='http://facebook.com/notearise' style='text-decoration:underline;color:inherit;' target='_blank'>Contact Us</a> &#183; <a href='https://www.bribe.bar/terms-of-service' style='text-decoration:underline;color:inherit;' target='_blank'>Terms of Use</a> &#183; <a href='https://www.bribe.bar/privacy-policy' style='text-decoration:underline;color:inherit;' target='_blank'>Privacy Policy</a></span><br/><br/> </td> </tr> </tbody> </table> <!--// NZDV830004 - END //--> </td> <td>&nbsp;</td> </tr> </tbody> </table> <!--// NZDV830004 - END //--> </td> </tr> </tbody> </table> </body></html>
DELIMITER;

					$user_email = $email;
					$user_validation_code = $validation_code;
					$email_message = $message;
					$email_subject = $subject;

					// Send email with Mailgun
					send_email($user_email,$user_first_name,$user_username,$user_validation_code,$email_subject,$email_message);

					set_message("<p class='bg-success text-center'>Please check your email or spam folder for a password reset code</p>");

					//redirect("sign-in.php");
					gotoURL('guest','sign-in','TRK=recover_password');

				}

				else {

					echo validation_errors("This emails does not exist");

				}

			}

			else {

				//redirect("sign-in.php");
				gotoURL('guest','sign-in','tokennotsynced');

			}

			// token checks

			if(isset($_POST['submit_cancel'])) {

				//redirect("sign-in.php");
				gotoURL('guest','sign-in','submit_cancel');

			}

		} // post request

	} // functions

	/**************** Code  Validation ********************/

	function validate_code() {

		if(isset($_COOKIE['bb_temp_access_code'])) {

			if(!isset($_GET['email']) && !isset($_GET['code'])) {

				//redirect("index.php");
				gotoURL('guest','sign-in','TRK=validate_code_if');

			}

			else if (empty($_GET['email']) || empty($_GET['code'])) {

				//redirect("index.php");
				gotoURL('guest','sign-in','TRK=validate_code_elseif');

			}

			else {

				if(isset($_POST['code'])) {

					$email = clean($_GET['email']);

					$validation_code = clean($_POST['code']);

					$sql = "SELECT id FROM users WHERE validation_code = '".escape($validation_code)."' AND email = '".escape($email)."'";
					$result = query($sql);

					if(row_count($result) == 1) {

						setcookie('bb_temp_access_code', $validation_code, time()+ 84600);

						//redirect("reset.php?email=$email&code=$validation_code");
						gotoURL('guest','password-reset','email=' . $email . '&code=' . $validation_code);

					}

					else {

						echo validation_errors("Sorry wrong validation code");

					}

				}

			}

		}

		else {

			set_message("<p class='bg-danger text-center'>Sorry your validation cookie was expired</p>");

			//redirect("recover.php");
			gotoURL('guest','password-reset','TRK=validate_code_else');

		}

	}

	/**************** Password Reset Function ********************/

	//
	function password_reset() {

		//
		if(isset($_COOKIE['bb_temp_access_code'])) {

			//
			if(isset($_GET['email']) && isset($_GET['code'])) {

				//
				if(isset($_SESSION['bb_token']) && isset($_POST['token'])) {

					//
					if($_POST['token'] === $_SESSION['bb_token']) {

						//
						if($_POST['password']=== $_POST['password_confirm'])  {

							$updated_password = md5($_POST['password']);

							$sql = "UPDATE users SET password = '".escape($updated_password)."', validation_code = 0 WHERE email = '".escape($_GET['email'])."'";
							query($sql);

							set_message("<p class='bg-success text-center'>You passwords has been updated, please login</p>");

							//redirect("login.php");
							gotoURL('guest','sign-in','TRK=password_reset');

						}

						else {

							echo validation_errors("Password fields don't match");

						}

					}

				}

			}

		}

		else {

			set_message("<p class='bg-danger text-center'>Sorry your time has expired</p>");

			//redirect("recover.php");
			gotoURL('guest','sign-in','TRK=timeexpired');

		}

	}

	//
	function getProfile($user,$domain,$detail) {

		/*
		$sql = "INSERT INTO users (id,first_name,username,email,password,validation_code,user_closed,signup_date,profile_pic,num_posts,num_likes,num_notes,friend_array,active,person_ID)";
		$sql .= " VALUES (NULL,'$first_name','$username','$email','$password','$validation_code','yes',NOW(),'$profile_pic','0','0','0',',','0','$person_ID')";
		$result = query($sql);
		confirm($result);
		*/

		//
		switch($domain) {

			case 'notes':

				// Database call...
				$notes_count_query  = "SELECT num_notes FROM users";
				$notes_count_query .= " WHERE username = '$user' AND active = 1 ";
				$notes_count_result = query($notes_count_query);

				// Isolate important items from database call...
				$notes_count = mysqli_fetch_array($notes_count_result);
				$profile_notes_count = $notes_count['num_notes'];

				// Pass back usable result...
				return $profile_notes_count;

				// Get outta here...
				break;

			case 'discussions':

				// Database call...
				$discussions_count_query  = "SELECT * FROM notifications";
				$discussions_count_query .= " WHERE user_to = '$user' ";
				$discussions_count_result = query($discussions_count_query);

				// Isolate important items from database call...
				$discussions_count = mysqli_num_rows($discussions_count_result);
				$profile_discussions_count = $discussions_count;

				// Pass back usable result...
				return $profile_discussions_count;

				// Get outta here...
				break;

			case 'friends':

				// Database call...
				$friends_count_query  = "SELECT friend_array FROM users";
				$friends_count_query .= " WHERE username = '$user' ";
				$friends_count_result = query($friends_count_query);

				// Isolate important items from database call...
				$friends_count_result_string = mysqli_fetch_array($friends_count_result);
				$friends_count_array = $friends_count_result_string['friend_array'];
				$friends_count = (substr_count($friends_count_array,',')) - 1;

				/* TOO MANY STEPS TO GET COUNT... just found substr earlier today... DOPE!
				$friends_count_array = trim($friends_count_array,','); // remove outward commas
				$friends_count_array = explode(',',$friends_count_array); // you have array now
				$friends_count = count($friends_count_array);
				*/

				$profile_friends_count = $friends_count;

				// Pass back usable result...
				return $profile_friends_count;

				// Get outta here...
				break;

		}

	}

?>
