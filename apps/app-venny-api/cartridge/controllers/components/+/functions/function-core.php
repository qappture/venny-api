<?php

	//
	function escape($string) {

		global $db;

		return mysqli_real_escape_string($db, $string);

	}

	//
	function confirm($result) {

		global $db;

		if(!$result) {

			die("QUERY FAILED - " . mysqli_error($db));

		}

	}

	// https://www.bestwebframeworks.com/tutorials/php/152/create-php-alternative-for-mysql-result-in-mysqli/
	function mysqli_result($result, $row, $field = 0) {
	    // Adjust the result pointer to that specific row
	    $result->data_seek($row);
	    // Fetch result array
	    $data = $result->fetch_array();

	    return $data[$field];
	}

	//
	function query($query) {

		global $db;

		$result = mysqli_query($db, $query);

		confirm($result);

		return $result;

	}

	//
	function fetch_array($result) {

		global $db;

		return mysqli_fetch_array($result);

	}

  //
  function row_count($result) {

    return mysqli_num_rows($result);

  }

  //
  function good_api_token($token) {

    $token_query = query("SELECT token_key FROM tokens WHERE token_key = '{$token}' AND active = 1 LIMIT 1");

    if(row_count($token_query) == 1 ) {

      return true;

    } else {

      return false;

    }

  }

  //
  function good_api_domain($domain) {

    if($domain == 'persons' || 'users' || 'partners' || 'businesses' || 'locations' || 'searches' || 'events' || 'catalogs' || 'products' || 'orders' || 'orderitems' || 'likes' || 'comments' || 'images' || 'tokens' || 'apps'){
      return true;
    }

  }

  //
  function good_api_request($request) {

  }

	// Keys tables... IDs table...
	function new_ID($type = NULL,$event = NULL) {

		//
		switch ($type) {

			case 'academics': $prefix='ad'; break;
			case 'accomplishments': $prefix='ac'; break;
			case 'activities': $prefix='av'; break;
			case 'apps': $prefix='ap'; break;
			case 'categories': $prefix='ct'; break;
			case 'comments': $prefix='cm'; break;
			case 'events': $prefix='ev'; break;
			case 'friend_requests': $prefix='fr'; break;
			case 'images': $prefix='im'; break;
			case 'institutions': $prefix='iu'; break;
			case 'interests': $prefix='in'; break;
			case 'likes': $prefix='lk'; break;
			case 'members': $prefix='mb'; break;
			case 'messages': $prefix='ms'; break;
			case 'notifications': $prefix='nt'; break;
			case 'opportunities': $prefix='op'; break;
			case 'organizations': $prefix='or'; break;
			case 'partners': $prefix='pr'; break;
			case 'people': $prefix='pp'; break;
			case 'persons': $prefix='ps'; break;
			case 'positions': $prefix='po'; break;
			case 'posts': $prefix='pt'; break;
			case 'profiles': $prefix='pf'; break;
			case 'projects': $prefix='pj'; break;
			case 'recordings': $prefix='rc'; break;
			case 'requirements': $prefix='rq'; break;
			case 'responsibilities': $prefix='rp'; break;
			case 'skills': $prefix='sk'; break;
			case 'tags': $prefix='tg'; break;
			case 'tokens': $prefix='tk'; break;
			case 'trends': $prefix='tr'; break;
			case 'users': $prefix='ur'; break;
			case 'verifications': $prefix='vf'; break;
			default: $prefix=''; break;

		}

		// Choose ID
		$body = substr(md5(uniqid(microtime(true),true)),0,8);

		//
		$ID = $prefix . $body;

		if(!isset($event)) {
			$event = substr(md5(uniqid(microtime(true),true)),0,8);
		}
		// Eventually we'll want this to end up searching a "keys" table to ensure NO IDs are the same. This is because how we're juggling verfications, likes and comments... it benefits us to be able to ensure there is no possibility of a duplicate

		$sql = "SELECT object_ID FROM IDs";
		$query = query($sql);
		if(row_count($query)>0){$unique=FALSE;}else{$unique=TRUE;}

		if($unique=TRUE) {
				query("INSERT INTO IDs (object_ID,event) VALUES ('$ID','$event')");
				return $ID;
		} else {
			return NULL;
		}

	}

  //
	//function create_api_event() {
	function create_api_event($object=NULL,$type=NULL,$token=NULL) {

    $event_ID = substr(md5(uniqid(microtime(true),true)),0,8);
    $event_ID = base64_encode($event_ID);

		$sql_event = "INSERT INTO events (event_ID,event_type,event_token,event_object) VALUES ('$event_ID','$type','$token','$object')";
		$log_event = query($sql_event);

		return $event_ID;
  }

	//
	function getSelectbyYear($name) {

		// use this to set an option as selected (ie you are pulling existing values out of the database)
		$already_selected_value=2018;
		$earliest_year=1970;

		print "<select name='" . $name . "' id='" . $name . "'>";
		print "<option disabled selected>Year</option>";

		//
		foreach (range(date('Y'), $earliest_year) as $x) {

		  print '<option value="'.$x.'"'.($x === $already_selected_value ? ' selected="selected"' : '').'>'.$x.'</option>';

		}

		print "</select>";

	}

	//
	function getSelectbyMonth($name) {

		print "<select name='" . $name . "' id='" . $name . "'>";
		print "<option disabled selected>Month</option>";
		print "<option value='01'>January</option>";
		print "<option value='02'>February</option>";
		print "<option value='03'>March</option>";
		print "<option value='04'>April</option>";
		print "<option value='05'>May</option>";
		print "<option value='06'>June</option>";
		print "<option value='07'>July</option>";
		print "<option value='08'>August</option>";
		print "<option value='09'>September</option>";
		print "<option value='10'>October</option>";
		print "<option value='11'>November</option>";
		print "<option value='12'>December</option>";
		print "</select>";

	}

	//
	function getSelectbyCategory($name=NULL) {

		print "<select name='category' id='category'>";
		print "<option disabled selected>Select category...</option>";

		$sql = "SELECT category_ID, category_name FROM categories WHERE active = 1 ORDER BY category_name ASC";
		$query = query($sql);

		//
		while($row = fetch_array($query)) {

			$options = "<option value='" . $row['category_ID'] . "'>" . $row['category_name'] . "</option>";
			echo $options;

		}

		print "</select>";

	}

	//
	function getSelectbyAccess() {

		print "<select name='access' id='access'>";
		print "<option value='2' selected>All users</option>";
		print "<option value='1'>Specific users</option>";
		print "<option value='0'>Me only</option>";
		print "</select>";

	}

	//
	function getSelectbyOrganization($name) {

		//
		print "<select name='" . $name . "' id='" . $name . "'>";
		print "<option disabled selected>Select organization...</option>";

		//
		$sql = "SELECT organization_ID, organization_name FROM organizations ORDER BY organization_name ASC";
		$query = query($sql);

		//
		while($row = fetch_array($query)) {

			$options = "<option value='" . $row['organization_ID'] . "'>" . $row['organization_name'] . "</option>";
			echo $options;

		}

		print "</select>";

	}

	//
	function getSelectbyMedia($name) {

		//
		print "<select name='" . $name . "' id='" . $name . "'>";
		print "<option disabled selected>Select media...</option>";

		//
		$sql = "SELECT media_ID, media_label FROM media ORDER BY media_label ASC";
		$query = query($sql);

		//
		while($row = fetch_array($query)) {

			$options = "<option value='" . $row['media_ID'] . "'>" . $row['media_label'] . "</option>";
			echo $options;

		}

		print "</select>";

	}

  //
  function email_unique($email) {
    $sql = "SELECT person_email FROM persons WHERE person_email = '{$email}' LIMIT 1 ";
    $result = query($sql);

    if(row_count($result) == 1 ) {

      return false;

    } else {

      return true;

    }

  }

  // TEST MODULE
  if ($_SERVER['PHP_SELF'] == '/www.bribe.bar/apps/core/app.php'/* || $_SERVER['DOCUMENT_ROOT'] == '/app'*/) {

    echo "DOCUMENT_ROOT: " . $_SERVER['DOCUMENT_ROOT'] . "<hr/>";
    echo "PHP_SELF: " . $_SERVER['PHP_SELF'] . "<hr/>";
    echo "SCRIPT_FILENAME: " . $_SERVER["SCRIPT_FILENAME"] . "<hr/>";
    echo "SERVER_ADMIN: " . $_SERVER["SERVER_ADMIN"] . "<hr/>";
    echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<hr/>";
    echo "SERVER_ADDR: " . $_SERVER["SERVER_ADDR"] . "<hr/>";
    echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<hr/>";

    echo "include_path:" . get_include_path() . "<hr/>";

    $query = query("SELECT * FROM persons");

    $results = fetch_array($query);

		if(row_count($query) < 1) {

			echo "<center><b>Oops, <span class='query'>{$query}</span> wasn't found in any of our dishes!</b></center>";

		}

    else {

      while($results = fetch_array($query)) {

        //
        $person_ID = $results['person_ID'];

        $person = "

          <ul>

            <li>{$person_ID}</li>

          </ul>

        ";

        echo $person;

      }

    }

  }

?>
