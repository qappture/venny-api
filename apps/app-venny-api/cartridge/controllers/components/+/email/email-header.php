<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>

<html style='width:100%;padding:0px;margin:0px;'>

  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>Reset your Gradient password</title>
    <style type="text/css"></style>
  </head>

  <body style='width:100%;padding:0px;margin:0px;background-color:none;'>
    <div style='width:100%;'>

      <table style='width:100%;background-color:none;border=0';>
        <tbody style='width:100%;'>
          <tr style='width:100%;'>
            <td style='width:100%;'> <!--// NZDV830004 - BEGIN //-->
              <table style="width:100%;max-width:768px;background-color:#eeeeee;padding:20px;margin:0 auto;color:#9b9b9b;font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;" border='0'>
                <tbody style='width:100%;max-width:768px;'>
                  <tr style='width:100%;max-width:768px;'>
                    <td>&nbsp;</td>
                    <td style='width:100%;max-width:768px;'>
                      <center><a href='https://www.gradient.global' target='_blank' title='Gradient'><img width='30' src='https://www.gradient.global/static/images/email/gradient-email-logo.png' alt='Gradient'></a></center><br/> <!--// NZDV830003 - BEGIN //-->
                      <table style='width:100%;max-width:768px;background-color:#ffffff;padding:12px 25px;margin:0;border-radius:5px;border:1px solid #dddddd;'>
                        <tbody style='width:100%;max-width:768px;'>
                          <tr style='width:100%;max-width:768px;'>
                            <td style='width:100%;max-width:768px;'>
