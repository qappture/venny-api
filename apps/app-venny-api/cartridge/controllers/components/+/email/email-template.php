<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>

<html style='width:100%;padding:0px;margin:0px;'>

  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <title>Reset your Gradient password</title>
    <style type="text/css"></style>
  </head>

  <body style='width:100%;padding:0px;margin:0px;background-color:none;'>
    <div style='width:100%;'>

      <table style='width:100%;background-color:none;border=0';>
        <tbody style='width:100%;'>
          <tr style='width:100%;'>
            <td style='width:100%;'> <!--// NZDV830004 - BEGIN //-->
              <table style="width:100%;max-width:768px;background-color:#eeeeee;padding:20px;margin:0 auto;color:#9b9b9b;font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;" border='0'>
                <tbody style='width:100%;max-width:768px;'>
                  <tr style='width:100%;max-width:768px;'>
                    <td>&nbsp;</td>
                    <td style='width:100%;max-width:768px;'>
                      <center><a href='https://www.gradient.global' target='_blank' title='Gradient'><img width='30' src='https://www.gradient.global/static/images/email/gradient-email-logo.png' alt='Gradient'></a></center><br/> <!--// NZDV830003 - BEGIN //-->
                      <table style='width:100%;max-width:768px;background-color:#ffffff;padding:12px 25px;margin:0;border-radius:5px;border:1px solid #dddddd;'>
                        <tbody style='width:100%;max-width:768px;'>
                          <tr style='width:100%;max-width:768px;'>
                            <td style='width:100%;max-width:768px;'>
                              <span><h1>Reset your password?</h1></span>
                              <span><h2>{$user_first_name}, if you requested a password reset for <a style='text-decoration:underline;color:inherit;' href='https://www.gradient.global/$user_username'>@{$user_username}</a>, click the button below and the validation code. If you didn't make this request, ignore this email.</h2></span>
                              <span>Please click the link below <a href='https://www.gradient.global/template-guest-enter-code.php?email={$email}&code={$validation_code}' style='text-decoration:underline;color:inherit;' target='_blank'>to reset your password</a>. The link will only be active for 24 hours from now.</span><br/><br/>
                              <span>Validation Code: <b>{$validation_code}</b></span><br/><br/>
                              <span style='width:100%;'><a style='border-radius:5px;text-align:center;background-color:#e42e55;width:200px;color:#ffffff;border:0px solid #348eda;display:inline-block;padding:20px 25px;margin:10px auto;text-align:center;font-weight:bold;text-decoration:none;' href='https://www.gradient.com/template-guest-enter-code.php?email={$email}&code={$validation_code}' target='_blank'>Reset Password</a></span><br/>
                              <span style='color:inherit;'>Thank you for choosing Gradient!</span><br/><br/>
                              <span style='font-size:0.75em;text-align:center;width:100%;'>&copy; 2018 Gradient Global. All rights reserved.</span><br/><br/>
                              <span style='font-size:0.75em;text-align:center;width:100%'><a href='http://facebook.com/gradient' style='text-decoration:underline;color:inherit;' target='_blank'>Contact Us</a> &#183; <a href='https://www.gradient.global/terms-of-service' style='text-decoration:underline;color:inherit;' target='_blank'>Terms of Use</a> &#183; <a href='https://www.gradient.global/privacy-policy' style='text-decoration:underline;color:inherit;' target='_blank'>Privacy Policy</a></span><br/><br/>
                            </td>
                          </tr>
                        </tbody>
                      </table> <!--// NZDV830004 - END //-->
                    </td>
                    <td>&nbsp;</td>
                  </tr>
                </tbody>
              </table> <!--// NZDV830004 - END //-->
            </td>
          </tr>
        </tbody>
      </table>
    </body>
  </html>
