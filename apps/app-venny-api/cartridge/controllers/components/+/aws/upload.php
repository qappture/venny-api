<?php

  use Aws\S3\Exception\S3Exception;

  //
  require 'aws/start.php';

  if(isset($_FILES['file'])) { 

    $file = $_FILES['file'];

    // File details
    $name = $file['name'];
    $tmp_name = $file['tmp_name'];

    $extension = explode('.',$name);
    $extension = strtolower(end($extension));
    // var_dump($extension);

    // Temp details
    $key = md5(uniqid());
    $tmp_file_name = "{$key}.{$extension}";
    $tmp_file_path = "files/{$tmp_file_name}";
    //var_dump($tmp_file_path);

    // Move the file
    move_uploaded_file($tmp_name,$tmp_file_path);

    try {
      $s3->putObject([
        'Bucket' => $config['s3']['bucket'],
        'Key' => "v1/profiles/{$name}",
        'Body' => fopen($tmp_file_path,'rb'),
        'ACL' => "public-read"
      ]);

      // Remove the file...
      unlink($tmp_file_path);

    } catch(S3Exception $e){
      die("There was an error uploading this file.");
    }

  }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <form action="" enctype="multipart/form-data" method="post">
      <input type="file" name="file" value="">
      <input type="submit" name="" value="Upload">
    </form>
  </body>
</html>
