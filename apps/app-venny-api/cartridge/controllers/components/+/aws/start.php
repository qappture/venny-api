<?php

  use Aws\S3\S3Client;

  //
  require 'autoload.php';

  $config = require('aws/config.php');

  // S3
  $s3 = S3Client::factory([

    'credentials' => [
      'key' => $config['s3']['key'],
      'secret' => $config['s3']['secret']
    ],
    'bucket' => 'gradient-live',
    'version' => 'latest',
    'region'  => 'us-east-1'

  ]);

  /*
  $s3 = new Aws\S3\S3Client([
      'bucket' => 'gradient-live',
      'version' => 'latest',
      'region'  => 'us-east-2',
      'credentials' => [
        'key' => 'AKIAIUY6HK3C4BLK2NUA',
        'secret' => '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'
      ]
  ]);
  */

?>
