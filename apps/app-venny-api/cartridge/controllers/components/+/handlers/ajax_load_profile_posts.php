<?php
include("core/core-user.php");
include("classes/User.php");
include("classes/Post.php");

$limit = 5; //Number of posts to be loaded per call

$posts = new Post($db, $_REQUEST['userLoggedIn']);
$posts->loadProfilePosts($_REQUEST, $limit);
?>
