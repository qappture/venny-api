<?php

	/**************** composer *****************/

	require 'autoload.php';
	use Mailgun\Mailgun;

	/**************** helper functions ********************/

	//
	// Send email with Mailgun
	function send_email($member,$type) {

		$member = getSendEmailbyMemberID($member);

	  # First, instantiate the SDK with your API credentials
	  $mg = Mailgun::create(APP_EM_KEY);

		//
		$to = $member['email'];

		//
		$bcc = 'trygradient@gmail.com';

		//
		$subject = 'Email Subject' . ' - ' . date("Y-m-d h:i:sa") . ' - ' . APP_ENV;

		//
		$text = 'Test message here...';

		switch ($type) {
			case 'member-signup':
				$template = '_engine/email/email-member-signup.php';
				break;

			case 'member-resetpassword':
				$template = '_engine/email/email-member-resetpassword.php';
				break;

			case 'member-notification':
				$template = '_engine/email/email-member-notification.php';
				break;

			default:
				$template = '_engine/email/email-default.php';
				break;
		}

		/////// HTML //////////
		$html = file_get_contents(APP_ENV_SRVR . APP_ST_NAME . '_engine/email/email-header.php');
		$html .= file_get_contents(APP_ENV_SRVR . APP_ST_NAME . $template);
		$html .= file_get_contents(APP_ENV_SRVR . APP_ST_NAME . '_engine/email/email-footer.php');
		$html = str_replace('{{user_first_name}}', 'Al', $html);
		$html = str_replace('{{user_username}}', 'sonofadolphus', $html);
		$html = str_replace('{{email}}', 'al@notearise.com', $html);
		$html = str_replace('{{validation_code}}', '0000000008CX3', $html);
		/////// HTML //////////

		/*
		switch ($type) {

			//
			case 'member-signup':

				require '';
				# code...
				break;

			//
			default:
				# code...
				break;
		}
		*/

		// Get time
	  $user_current_time = date("Y-m-d h:i:sa");

	  # Now, compose and send your message.
	  # $mg->messages()->send($domain, $params);
	  $mg->messages()->send(APP_EM_HOST, [
	    'from'    => 'The ' . APP_NAME . ' Team ' . '<' . APP_EM_FROM . '>',
			'to'      => $to,
			'bcc'     => $bcc,
	    'subject' => $subject . ' - ' . date("Y-m-d h:i:sa") . ' - ' . APP_ENV,
	    'text'    => $text, // PLEASE UPDATE!!!
	    'html'    => $html,

	  ]);

		//echo 'try this - ' . $user_current_time; exit;

		//print_r($mg);

		return true;

	}

?>
