<?php

	//
	function escape($string) {

		global $db;

		return mysqli_real_escape_string($db, $string);

	}

	//
	function confirm($result) {

		global $db;

		if(!$result) {

			die("QUERY FAILED - " . mysqli_error($db));

		}

	}

	// https://www.bestwebframeworks.com/tutorials/php/152/create-php-alternative-for-mysql-result-in-mysqli/
	function mysqli_result($result, $row, $field = 0) {
	    // Adjust the result pointer to that specific row
	    $result->data_seek($row);
	    // Fetch result array
	    $data = $result->fetch_array();

	    return $data[$field];
	}

	//
	function query($query) {

		global $db;

		$result = mysqli_query($db, $query);

		confirm($result);

		return $result;

	}

	//
	function fetch_array($result) {

		global $db;

		return mysqli_fetch_array($result);

	}

  //
  function row_count($result) {

    return mysqli_num_rows($result);

  }

  //
  function good_api_token($token) {

    $token_query = query("SELECT token_key FROM tokens WHERE token_key = '{$token}' AND active = 1 LIMIT 1");

    if(row_count($token_query) == 1 ) {

      return true;

    } else {

      return false;

    }

  }

  //
  function good_api_domain($domain) {

    if($domain == 'persons' || 'users' || 'partners' || 'businesses' || 'locations' || 'searches' || 'events' || 'catalogs' || 'products' || 'orders' || 'orderitems' || 'likes' || 'comments' || 'images' || 'tokens' || 'apps'){
      return true;
    }

  }

  //
  function good_api_request($request) {

  }

	//
	function generate_unique_ID($type) {

		//
		switch ($type) {

			case 'academics': $prefix='ad'; break;
			case 'accomplishments': $prefix='ac'; break;
			case 'activities': $prefix='av'; break;
			case 'apps': $prefix='ap'; break;
			case 'attachments': $prefix='at'; break;
			case 'categories': $prefix='ct'; break;
			case 'comments': $prefix='cm'; break;
			case 'crowds': $prefix='cw'; break;
			case 'events': $prefix='ev'; break;
			case 'excerpts': $prefix='ex'; break;
			case 'friend_requests': $prefix='fr'; break;
			case 'followships': $prefix='fs'; break;
			case 'ideas': $prefix='id'; break;
			case 'images': $prefix='im'; break;
			case 'institutions': $prefix='iu'; break;
			case 'interests': $prefix='in'; break;
			case 'likes': $prefix='lk'; break;
			case 'members': $prefix='mb'; break;
			case 'messages': $prefix='ms'; break;
			case 'notifications': $prefix='nt'; break;
			case 'opportunities': $prefix='op'; break;
			case 'organizations': $prefix='or'; break;
			case 'partners': $prefix='pr'; break;
			case 'people': $prefix='pp'; break;
			case 'persons': $prefix='ps'; break;
			case 'positions': $prefix='po'; break;
			case 'posts': $prefix='pt'; break;
			case 'profiles': $prefix='pf'; break;
			case 'projects': $prefix='pj'; break;
			case 'recordings': $prefix='rc'; break;
			case 'requirements': $prefix='rq'; break;
			case 'responsibilities': $prefix='rp'; break;
			case 'skills': $prefix='sk'; break;
			case 'stages': $prefix='st'; break;
			case 'tags': $prefix='tg'; break;
			case 'tokens': $prefix='tk'; break;
			case 'threads': $prefix='td'; break;
			case 'trends': $prefix='tr'; break;
			case 'users': $prefix='ur'; break;
			case 'uniques': $prefix='uq'; break;
			case 'verifications': $prefix='vf'; break;
			default: $prefix=''; break;

		}

		// Choose ID
		$body = substr(md5(uniqid(microtime(true),true)),0,9);

		//
		$ID = $prefix . "_" . $body;

		return $ID;

	}

	//
	function is_unique_ID_unique($ID) {

		$sql = "SELECT unique_ID FROM uniques WHERE active = 1 AND unique_ID = '$ID'";
		$query = query($sql);

		//
		if(row_count($query)>0) {

			$unique=FALSE;

		}

		else {

			$unique=TRUE;

		}

		return $unique;

	}

	// Keys tables... IDs table...
	function new_ID($type = NULL, $process = NULL) {

		//
		if(isset($process)) {

			$event = substr(md5(uniqid(microtime(true),true)),0,9);

		}

		$ID = generate_unique_ID($type);

		// Eventually we'll want this to end up searching a "keys" table to ensure NO IDs are the same. This is because how we're juggling verfications, likes and comments... it benefits us to be able to ensure there is no possibility of a duplicate

		$unique = is_unique_ID_unique($ID);

		if($unique=TRUE) {

			query("INSERT INTO uniques (unique_ID,unique_type,event_ID,process_ID) VALUES ('$ID','$type','$event','$process')");
			return $ID;

		} else {
			$adjustedID = $ID . "YO";//substr(md5(uniqid(microtime(true),true)),0,4);
			query("INSERT INTO uniques (unique_ID,unique_type,event_ID,process_ID) VALUES ('$adjustedID','$type','$event','$process')");
			return $ID;


		}

	}

	//function create_api_event() {
	function create_api_event($object = NULL, $type = NULL, $token = NULL) {

    $event_ID = substr(md5(uniqid(microtime(true),true)),0,9);
    $event_ID = base64_encode($event_ID);

		$sql_event = "INSERT INTO events (event_ID,event_type,event_token,event_object) VALUES ('$event_ID','$type','$token','$object')";
		$log_event = query($sql_event);

		return $event_ID;

  }

	//function create_api_process() {
	function create_api_process($object = NULL, $type = NULL, $token = NULL) {

		$process_ID = substr(md5(uniqid(microtime(true),true)),0,12);
		$process_ID = base64_encode($process_ID);

		$sql_process = "INSERT INTO processes (process_ID,process_action,app_ID) VALUES ('$process_ID','$type','$token')";
		$log_process = query($sql_process);

		return $process_ID;

	}

  //
  function email_unique($email) {
    $sql = "SELECT person_email FROM persons WHERE person_email = '{$email}' LIMIT 1 ";
    $result = query($sql);

    if(row_count($result) == 1 ) {

      return false;

    } else {

      return true;

    }

  }

?>
