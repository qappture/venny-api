<?php

  /*
  $config = parse_ini_file('../../app.ini');
  $dev_db_host = trim($config["dev_db_host"]);
  $dev_db_user = trim($config["dev_db_user"]);
  $dev_db_pass = trim($config["dev_db_pass"]);
  $dev_db_name = trim($config["dev_db_name"]);
  $dev_app_token = trim($config["dev_app_token"]);
  $dev_em_key = trim($config["dev_em_key"]);

  $test_db_host = trim($config["test_db_host"]);
  $test_db_user = trim($config["test_db_user"]);
  $test_db_pass = trim($config["test_db_pass"]);
  $test_db_name = trim($config["test_db_name"]);
  $test_app_token = trim($config["test_app_token"]);
  $test_em_key = trim($config["test_em_key"]);

  $prod_db_host = trim($config["prod_db_host"]);
  $prod_db_user = trim($config["prod_db_user"]);
  $prod_db_pass = trim($config["prod_db_pass"]);
  $prod_db_name = trim($config["prod_db_name"]);
  $prod_app_token = trim($config["prod_app_token"]);
  $prod_em_key = trim($config["prod_em_key"]);

  echo $dev_db_host . ' | ';
  echo $dev_db_user . ' | ';
  echo $dev_db_pass . ' | ';
  echo $dev_db_name . ' | ';
  echo $dev_app_token . ' | ';
  echo $dev_em_key . ' | ';
  echo '<br/>';
  echo $test_db_host . ' | ';
  echo $test_db_user . ' | ';
  echo $test_db_pass . ' | ';
  echo $test_db_name . ' | ';
  echo $test_app_token . ' | ';
  echo $test_em_key . ' | ';
  echo '<br/>';
  echo $prod_db_host . ' | ';
  echo $prod_db_user . ' | ';
  echo $prod_db_pass . ' | ';
  echo $prod_db_name . ' | ';
  echo $prod_app_token . ' | ';
  echo $prod_em_key . ' | ';

  //exit;
  */

  // Determine scope...
  if($_SERVER['HTTP_HOST'] == 'localhost') {

    define ('APP_NAME',     'Venny'); // app
    define ('APP_ENV',      'DEVELOPMENT'); // app environment
		define ('APP_ENV_ROOT', '/Library/WebServer/Documents/'); // app environment
    define ('APP_ENV_SRVR', 'http://localhost/'); // Site path
    define ('APP_ST_NAME',  'www.venny.io/');
    define ('APP_ST_ALIS',  APP_ST_NAME);
		define ('APP_ST_URL',   APP_ENV_SRVR . APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/app-venny-api/'); // include path
		define ('APP_ST_UPLD',  'assets/images/'); // directory for all image uploads
    $constantly = parse_ini_file(APP_ENV_ROOT.APP_ST_NAME.APP_ST_INCL.'app.ini');
    define ('APP_ST_CDN',   'http://venny-web.imgix.net/'); // directory for all image uploads
		define ('APP_DB_HOST',  trim($constantly["dev_db_host"])); // db hostname
    define ('APP_DB_PORT',  '3306'); // db port
    define ('APP_DB_NAME',  trim($constantly["dev_db_name"])); // db name
    define ('APP_DB_USER',  trim($constantly["dev_db_user"])); // db username
    define ('APP_DB_PASS',  trim($constantly["dev_db_pass"])); // db password
    define ('APP_API_KEY',  trim($constantly["dev_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@venny.co'); // app
    define ('APP_EM_HOST',  'email.venny.co'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["dev_em_key"])); // Private API Key
    define ('APP_GG_TAG',   'UA-116723436-1'); // Google Tag Manager ID

		/*

    echo 'ENV: ' . APP_ENV . '<br/>';
    echo 'APP_DB_HOST: ' . APP_DB_HOST . '<br/>';
    echo 'APP_DB_PORT: ' . APP_DB_PORT . '<br/>';
    echo 'APP_DB_NAME: ' . APP_DB_NAME . '<br/>';
    echo 'APP_DB_USER: ' . APP_DB_USER . '<br/>';
    echo 'APP_DB_PASS: ' . APP_DB_PASS . '<br/>';

		echo "DOCUMENT_ROOT: " . $_SERVER['DOCUMENT_ROOT'] . "<br/>";
		echo "PHP_SELF: " . $_SERVER['PHP_SELF'] . "<br/>";
		echo "SCRIPT_FILENAME: " . $_SERVER["SCRIPT_FILENAME"] . "<br/>";
		echo "SERVER_ADMIN: " . $_SERVER["SERVER_ADMIN"] . "<br/>";
		echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
		echo "SERVER_ADDR: " . $_SERVER["SERVER_ADDR"] . "<br/>";
		echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
		echo "include_path:" . get_include_path() . "<br/>";

		*/

  }

  elseif($_SERVER['HTTP_HOST'] == 'test.gradient.global') {

    define ('APP_NAME',     'Gradient'); // app environment
    define ('APP_ENV',      'STAGING'); // app environment
    define ('APP_ENV_SRVR', 'http://'); // Site path
    define ('APP_ENV_ROOT', '/app/'); // app environment
    define ('APP_ST_NAME',  'test.gradient.global/');
    define ('APP_ST_ALIS',  '');
    define ('APP_ST_URL',   APP_ENV_SRVR.APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/gradient-web/'); // include path
		define ('APP_ST_UPLD',  '_assets/images/'); // directory for all image uploads
    $constantly = parse_ini_file(APP_ENV_ROOT.'app.ini');
    define ('APP_ST_CDN',   'http://gradient-cdn.imgix.net/'); // directory for all image uploads
    define ('APP_DB_HOST',  trim($constantly["test_db_host"])); // Clear MySQL db hostname
    define ('APP_DB_PORT',  '3307'); // Clear MySQL db port
    define ('APP_DB_NAME',  trim($constantly["test_db_name"])); // Clear MySQL db name
    define ('APP_DB_USER',  trim($constantly["test_db_user"])); // Clear MySQL db username
    define ('APP_DB_PASS',  trim($constantly["test_db_pass"])); // Clear MySQL db password
    define ('APP_API_KEY',  trim($constantly["test_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@gradient.global'); // app
    define ('APP_EM_HOST',  'email.gradient.global'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["test_em_key"])); // Private API Key
    define ('APP_GG_TAG',   'UA-116723436-1'); // Google Tag Manager ID
		/*

		echo 'ENV: ' . APP_ENV . '<br/>';
    echo 'APP_DB_HOST: ' . APP_DB_HOST . '<br/>';
    echo 'APP_DB_PORT: ' . APP_DB_PORT . '<br/>';
    echo 'APP_DB_NAME: ' . APP_DB_NAME . '<br/>';
    echo 'APP_DB_USER: ' . APP_DB_USER . '<br/>';
    echo 'APP_DB_PASS: ' . APP_DB_PASS . '<br/>';

		echo "DOCUMENT_ROOT: " . $_SERVER['DOCUMENT_ROOT'] . "<br/>";
		echo "PHP_SELF: " . $_SERVER['PHP_SELF'] . "<br/>";
		echo "SCRIPT_FILENAME: " . $_SERVER["SCRIPT_FILENAME"] . "<br/>";
		echo "SERVER_ADMIN: " . $_SERVER["SERVER_ADMIN"] . "<br/>";
		echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
		echo "SERVER_ADDR: " . $_SERVER["SERVER_ADDR"] . "<br/>";
		echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
		echo "include_path:" . get_include_path() . "<br/>";

		*/

  }

  else {

    define ('APP_NAME',     'Gradient'); // app environment
    define ('APP_ENV',      'PRODUCTION'); // app environment
    define ('APP_ENV_SRVR', 'https://'); // Site path
    define ('APP_ENV_ROOT', '/app/'); // app environment
    define ('APP_ST_NAME',  'www.gradient.global/');
    define ('APP_ST_ALIS',  '');
    define ('APP_ST_URL',   APP_ENV_SRVR.APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/gradient-web/'); // include path
    define ('APP_ST_UPLD',  '_assets/images/'); // directory for all image uploads
    $constantly = parse_ini_file(APP_ENV_ROOT.'app.ini');
    define ('APP_ST_CDN',   'https://venny-web.imgix.net/'); // directory for all image uploads
    define ('APP_DB_HOST',  trim($constantly["prod_db_host"])); // Clear MySQL db hostname
    define ('APP_DB_PORT',  '3306'); // Clear MySQL db port
    define ('APP_DB_NAME',  trim($constantly["prod_db_name"])); // Clear MySQL db name
    define ('APP_DB_USER',  trim($constantly["prod_db_user"])); // Clear MySQL db username
    define ('APP_DB_PASS',  trim($constantly["prod_db_pass"])); // Clear MySQL db password
    define ('APP_API_KEY',  trim($constantly["prod_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@gradient.global'); // app
    define ('APP_EM_HOST',  'email.gradient.global'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["prod_em_key"])); // Private API Key
    define ('APP_GG_TAG',   'UA-116723436-2'); // Google Tag Manager ID
    /*

    echo 'ENV: ' . APP_ENV . '<br/>';
    echo 'APP_DB_HOST: ' . APP_DB_HOST . '<br/>';
    echo 'APP_DB_PORT: ' . APP_DB_PORT . '<br/>';
    echo 'APP_DB_NAME: ' . APP_DB_NAME . '<br/>';
    echo 'APP_DB_USER: ' . APP_DB_USER . '<br/>';
    echo 'APP_DB_PASS: ' . APP_DB_PASS . '<br/>';

    echo "DOCUMENT_ROOT: " . $_SERVER['DOCUMENT_ROOT'] . "<br/>";
    echo "PHP_SELF: " . $_SERVER['PHP_SELF'] . "<br/>";
    echo "SCRIPT_FILENAME: " . $_SERVER["SCRIPT_FILENAME"] . "<br/>";
    echo "SERVER_ADMIN: " . $_SERVER["SERVER_ADMIN"] . "<br/>";
    echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
    echo "SERVER_ADDR: " . $_SERVER["SERVER_ADDR"] . "<br/>";
    echo "SERVER_NAME: " . $_SERVER["SERVER_NAME"] . "<br/>";
    echo "include_path:" . get_include_path() . "<br/>";

    */

  }

?>
