<?php

	// Turns on output buffering
	ob_start();

	// Begins sessions
	session_start();

	//
	ini_set('display_errors', 1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL);

	//
	$timezone = date_default_timezone_set("America/New_York");

	//
	require 'controllers/components/core/core-environment.php';
	require 'controllers/components/core/core-access.php';

	//
	require 'controllers/components/functions/function-object.php';
	require 'controllers/components/functions/function-session.php';
	require 'controllers/components/functions/function-user.php';
	//require 'controllers/components/functions/function-messaging.php';

	//
  require 'controllers/components/properties/property-global.php';

?>
