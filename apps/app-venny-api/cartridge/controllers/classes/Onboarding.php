<?php

  //
  class Onboarding {

    //
    public function createPerson($first_name, $last_name = NULL, $email, $phone = NULL) {

      // Create unique ID
      $ID = new_ID($domain,$process = create_api_process());
      $person_ID = $ID;
      $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

      //
      if(isset($_REQUEST['first_name'])){$person_first_name = clean($_REQUEST['first_name']);}else{$person_first_name=NULL;}
      if(isset($_REQUEST['last_name'])){$person_last_name = clean($_REQUEST['last_name']);}else{$person_last_name=NULL;}
      if(isset($_REQUEST['email'])){$person_email = clean($_REQUEST['email']);}else{$person_email=NULL;}
      if(isset($_REQUEST['phone'])){$person_phone = clean($_REQUEST['phone']);}else{$person_phone=NULL;}

      // BEGIN CUSTOMIZATIONS

      // END CUSTOMIZATIONS

      $query = query(

        "INSERT INTO {$domain} (

          person_ID,
          person_first_name,
          person_last_name,
          person_email,
          person_phone

        ) VALUES (

          '$person_ID',
          '$person_first_name',
          '$person_last_name',
          '$person_email',
          '$person_phone'

        )"

      );

      //
      $successful = mysqli_insert_id($db);

      if($successful) {

        //Insert notification
        /*
        if($user_to != 'none') {

          $notification = new Notification($this->con, $added_by);
          $notification->insertNotificationNote($returned_id, $user_to, "like");

        }
        */

        //
        $response = array(

          $t_api_key_message => "The " . $domain . " entry " . $ID . " was created successfully.",
          $t_api_key_status => $t_api_value_statussuccess,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        return $response;

      }

      else {

        //
        $response = array(

          $t_api_key_message => NULL,
          $t_api_key_status => $t_api_value_statusfailed,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        return $response;

      }

    }

    //
    public function createUser($person_ID, $username, $password) {

      //
      $salt = openssl_random_pseudo_bytes(20);
      $secured_password = sha1($password . $salt);

      // STEP 1. Declare parms of user inf
      // if GET or POST are empty
      if (empty($_REQUEST["username"]) || empty($_REQUEST["password"]) || empty($_REQUEST["email"]) || empty($_REQUEST["fullname"])) {

        $returnArray["status"] = "400";
        $returnArray["message"] = "Missing required information";

        header('Content-Type: application/json');
        echo json_encode($returnArray);

        return;

      }

      // Securing information and storing variables
      $username = htmlentities($_REQUEST["username"]);
      $password = htmlentities($_REQUEST["password"]);
      $email = htmlentities($_REQUEST["email"]);
      $fullname = htmlentities($_REQUEST["fullname"]);

      // secure password
      $salt = openssl_random_pseudo_bytes(20);
      $secured_password = sha1($password . $salt);

      // STEP 2. Build connection
      // Secure way to build conn
      //$file = parse_ini_file("venny.ini");

      // store in php var inf from ini var
      //$host = trim($file["dbhost"]);
      //$user = trim($file["dbuser"]);
      //$pass = trim($file["dbpass"]);
      //$name = trim($file["dbname"]);

      // include access.php to call func from access.php file
      require ("access.php");
      $access = new access($host, $user, $pass, $name);
      $access->connect();

      // STEP 3. Insert user information
      $result = $access->registerUser($username, $secured_password, $salt, $email, $fullname);

      // successfully registered
      if ($result) {

        // get current registered user information and store in $user
        $user = $access->selectUser($username);

        // declare information to feedback to user of App as json
        $returnArray["status"] = "200";
        $returnArray["message"] = "Successfully registered";
        $returnArray["id"] = $user["id"];
        $returnArray["username"] = $user["username"];
        $returnArray["email"] = $user["email"];
        $returnArray["fullname"] = $user["fullname"];
        $returnArray["ava"] = $user["ava"];

        // STEP 4. Emailing
        // include email.php
        require ("email.php");

        // store all class in $email var
        $email = new notification();

        // store generated token in $token var
        $token = $email->generateToken(20);

        // save inf in 'emailTokens' table
        $access->saveToken("emailTokens", $user["id"], $token);

        // refer emailing information
        $details = array();
        $details["subject"] = "Email confirmation on Venny";
        $details["to"] = $user["email"];
        $details["fromName"] = "sonofadolphus";
        $details["fromEmail"] = "sonofadolphus@gmail.com";

        // access template file
        $template = $email->confirmationTemplate();

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{token}", $token, $template);

        $details["body"] = $template;

        $email->sendEmail($details);

      }

      else {

          $returnArray["status"] = "400";
          $returnArray["message"] = "Could not register with provided information";

      }

      // STEP 5. Close connection
      $access->disconnect();

      // STEP 6. Json data
      header('Content-Type: application/json');
      echo json_encode($returnArray);

      ////////////////////////////////////////////

      //
      header('Content-Type: application/json');

      //
      $ID = new_ID($domain,$process = RAND());
      $user_ID = $ID;
      $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

      //
      if(isset($_REQUEST['alias'])){$user_alias = clean($_REQUEST['alias']);}else{$user_alias=NULL;}
      if(isset($_REQUEST['password'])){$user_password = clean($_REQUEST['password']);}else{$user_password=NULL;}
      if(isset($_REQUEST['lastlogin'])){$user_lastlogin = clean($_REQUEST['lastlogin']);}else{$user_lastlogin=NULL;}
      if(isset($_REQUEST['status'])){$user_status = clean($_REQUEST['status']);}else{$user_status=NULL;}
      if(isset($_REQUEST['validation'])){$user_validation = clean($_REQUEST['validation']);}else{$user_validation=NULL;}
      if(isset($_REQUEST['salt'])){$user_salt = clean($_REQUEST['salt']);}else{$user_salt=NULL;}
      if(isset($_REQUEST['welcome'])){$user_welcome = clean($_REQUEST['welcome']);}else{$user_welcome=NULL;}
      if(isset($_REQUEST['person_ID'])){$person_ID = clean($_REQUEST['person_ID']);}else{$person_ID=NULL;}
      if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
      if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
      if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

      // BEGIN CUSTOMIZATIONS

      // END CUSTOMIZATIONS

      $query = query(

        "INSERT INTO {$domain} (

          user_ID,
          user_alias,
          user_password,
          user_lastlogin,
          user_status,
          user_validation,
          user_salt,
          user_welcome,
          person_ID,
          app_ID,
          event_ID,
          process_ID

        ) VALUES (

          '$user_ID',
          '$user_alias',
          '$user_password',
          '$user_lastlogin',
          '$user_status',
          '$user_validation',
          '$user_salt',
          '$user_welcome',
          '$person_ID',
          '$app_ID',
          '$event_ID',
          '$process_ID'

        )"

      );

      // TESTING
      //echo $query;
      //exit;

      //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
      $successful = mysqli_insert_id($db);

      if($successful) {

        //Insert notification
        /*
        if($user_to != 'none') {

          $notification = new Notification($this->con, $added_by);
          $notification->insertNotificationNote($returned_id, $user_to, "like");

        }
        */

        //Update note count for user
        /*
        $num_notes = $this->user_obj->getNumNotes();
        $num_notes++;
        $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
        */

        //
        $response = array(

          $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
          $t_api_key_status => $t_api_value_statussuccess,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        header('Content-Type: application/json');

        echo json_encode($response);

      }

      else {

        //
        $response = array(

          $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
          $t_api_key_status => $t_api_value_statusfailed,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        header('Content-Type: application/json');

        echo $response;

      }

    }

    //
    public function createProfile($user_ID, $name) {

      //
      header('Content-Type: application/json');

      //
      $ID = new_ID($domain,$process = RAND());
      $profile_ID = $ID;
      $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

      //
      if(isset($_REQUEST['images'])){$profile_images = clean($_REQUEST['images']);}else{$profile_images=NULL;}
      if(isset($_REQUEST['bio'])){$profile_bio = clean($_REQUEST['bio']);}else{$profile_bio=NULL;}
      if(isset($_REQUEST['name'])){$profile_name = clean($_REQUEST['name']);}else{$profile_name=NULL;}
      if(isset($_REQUEST['headline'])){$profile_headline = clean($_REQUEST['headline']);}else{$profile_headline=NULL;}
      if(isset($_REQUEST['access'])){$profile_access = clean($_REQUEST['access']);}else{$profile_access=NULL;}
      if(isset($_REQUEST['user_ID'])){$user_ID = clean($_REQUEST['user_ID']);}else{$user_ID=NULL;}
      if(isset($_REQUEST['person_ID'])){$person_ID = clean($_REQUEST['person_ID']);}else{$person_ID=NULL;}
      if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
      if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
      if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

      // BEGIN CUSTOMIZATIONS

      // END CUSTOMIZATIONS

      $query = query(

        "INSERT INTO {$domain} (

          profile_ID,
          profile_images,
          profile_bio,
          profile_name,
          profile_headline,
          profile_access,
          user_ID,
          person_ID,
          app_ID,
          event_ID,
          process_ID

        ) VALUES (

          '$profile_ID',
          '$profile_images',
          '$profile_bio',
          '$profile_name',
          '$profile_headline',
          '$profile_access',
          '$user_ID',
          '$person_ID',
          '$app_ID',
          '$event_ID',
          '$process_ID'

        )"

      );

      // TESTING
      //echo $query;
      //exit;

      //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
      $successful = mysqli_insert_id($db);

      if($successful) {

        //Insert notification
        /*
        if($user_to != 'none') {

          $notification = new Notification($this->con, $added_by);
          $notification->insertNotificationNote($returned_id, $user_to, "like");

        }
        */

        //Update note count for user
        /*
        $num_notes = $this->user_obj->getNumNotes();
        $num_notes++;
        $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
        */

        //
        $response = array(

          $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
          $t_api_key_status => $t_api_value_statussuccess,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        header('Content-Type: application/json');

        echo json_encode($response);

      }

      else {

        //
        $response = array(

          $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
          $t_api_key_status => $t_api_value_statusfailed,
          $t_api_key_event => $event,
          $t_api_key_process => $process

        );

        header('Content-Type: application/json');

        echo $response;

      }

    }

  }

?>
