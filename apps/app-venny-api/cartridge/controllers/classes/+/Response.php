<?php

	// Response class
	class Response {

		//
		public function __construct($results) {

			//header('Content-Type: application/json');
			//echo json_encode($results); exit;

			$this->count = count($results);

		}

		//
		public function generateResponse($type, $status, $code, $count, $results) {

			//
			$this->type = $type;

			//
			header('Content-Type: application/json');

			//
			$response = array(

				Constant::$t_api_key_status => 'successful',//$status,
				Constant::$t_api_key_code => $code,//$code,
				Constant::$t_api_key_count => $count,//$total,
				Constant::$t_api_key_results => $results,
				Constant::$t_api_key_event => RAND()//$event

			);

			echo json_encode($response);

		}

  }

?>
