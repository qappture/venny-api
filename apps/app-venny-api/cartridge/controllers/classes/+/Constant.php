<?php

	//
	class Constant {

		//
		public static $passwordsDoNoMatch = "Your passwords don't match";
		public static $passwordNotAlphanumeric = "Your password can only contain numbers and letters";
		public static $passwordCharacters = "Your password must be between 5 and 30 characters";
		public static $emailInvalid = "Email is invalid";
		public static $emailsDoNotMatch = "Your emails don't match";
		public static $emailTaken = "This email is already in use";
		public static $lastNameCharacters = "Your last name must be between 2 and 25 characters";
		public static $firstNameCharacters = "Your first name must be between 2 and 25 characters";
		public static $usernameCharacters = "Your username must be between 5 and 25 characters";
		public static $usernameTaken = "This username already exists";

		public static $loginFailed = "Your username or password was incorrect";

		// Global
		public static $t_global_app_company = "Venny";
		public static $t_global_app_name = "Venny";
		public static $t_global_app_tagline = "Time to step out of line.";
		public static $t_global_app_copyright = "&copy; 2018.";
		public static $t_global_app_allrightsreserved = "All rights reserved.";

		// Hello
		public static $t_hello_welcome_message = "Well, hello! We're Notearise.";
		public static $t_hello_tagline_message = "Prepare for it all.";
		public static $t_hello_registration_label = "Sign Up";
		public static $t_hello_login_label = "Sign In";

		// Locations
		public static $t_locations_message_ready = "Who's ready to be bribed in your area?";

		// Location
		public static $t_location_message_choose = "Choose your drinks.";

		// API
		public static $t_api_domain_needdomain = "Please include your Bribe API access domain.";
		public static $t_api_token_needtoken = "Please include your Bribe API access token.";
		public static $t_api_key_code = "code";
		public static $t_api_key_count = "count";
		public static $t_api_key_html = "html_attributions";
		public static $t_api_key_results = "results";
		public static $t_api_key_status = "status";
		public static $t_api_key_event = "event";
		public static $t_api_key_message = "message";
		public static $t_api_value_code200 = "200";
		public static $t_api_value_code403 = "403";
		public static $t_api_value_html = "[]";
		public static $t_api_value_missingdomain = "Please include a valid domain.";
		public static $t_api_value_missingtoken = "Please include a valid access token.";
		public static $t_api_value_statussuccess = "SUCCESS";
		public static $t_api_value_statusfailed = "FAILED";
		public static $t_api_value_statusfailure = "FAILURE";
		public static $t_api_value_statusdenied = "REQUEST_DENIED";
		public static $t_api_value_messagecreate = "Your entry was successfully created.";
		public static $t_api_value_messageduplicateemail = "The email used is already in our system. Please try another.";
		public static $t_api_value_messagemissingemail = "Each entry must include a valid email address. Please try again.";
		public static $t_api_value_messagemissingfirstname = "Each entry must include a first name with at least 2 characters. Please try again.";

		// Home
		public static $t_home_message_loading = "Loading locations...";

		// Sign In
		public static $t_signin_message_login = "Sign in below.";

		//
		public static $t_attributes_skills_label = "List your skill";
		public static $t_attributes_skills_level = "Skill level";
		public static $t_attributes_skills_used_last = "When was this skill last used?";
		public static $t_attributes_skills_duration = "How long have you had this skill?";
		public static $t_attributes_skills_member = "";
		public static $t_attributes_academics_certification = "Degree or certification";
		public static $t_attributes_academics_field = "Field of study";
		public static $t_attributes_academics_classification = "Classification";
		public static $t_attributes_academics_description = "Description of studies";
		public static $t_attributes_academics_from = "From Year";
		public static $t_attributes_academics_to = "To Year";
		public static $t_attributes_academics_ongoing = "Ongoing";
		public static $t_attributes_interests_label = "List an interest of yours";
		public static $t_attributes_hobbies_label = "List an hobbies of yours";
		public static $t_attributes_positions_title = "Title of this positon";
		public static $t_attributes_positions_description = "Description of the functions";
		public static $t_attributes_positions_from = "From Year";
		public static $t_attributes_positions_to = "To Year";
		public static $t_attributes_positions_ongoing = "Are you still working with this company";
		public static $t_attributes_accomplishments_title = "Name of your achievement";
		public static $t_attributes_accomplishments_description = "Description of your achievement";
		public static $t_attributes_accomplishments_issuer = "Issuing group";
		public static $t_attributes_accomplishments_from = "From year";
		public static $t_attributes_accomplishments_to = "To year";
		public static $t_attributes_activities_role = "Role in your involvement";
		public static $t_attributes_activities_description = "Description of your involvement";
		public static $t_attributes_activities_from_year = "From (year)";
		public static $t_attributes_activities_from_month = "From (month)";
		public static $t_attributes_activities_to_year = "To (year)";
		public static $t_attributes_activities_to_month = "To (month)";
		public static $t_attributes_activities_ongoing = "Is this involvement ongoing?";
		public static $t_attributes_projects_name = "Name of project";
		public static $t_attributes_projects_description = "Description of project";
		public static $t_attributes_projects_role = "Your role on this project";
		public static $t_attributes_projects_industry = "Industry of your project";
		public static $t_attributes_projects_from_year = "From (year)";
		public static $t_attributes_projects_from_month = "From (month)";
		public static $t_attributes_projects_to_year = "To (year)";
		public static $t_attributes_projects_to_month = "To (month)";
		public static $t_attributes_projects_category = "Choose a category for this project";
		public static $t_attributes_projects_access = "Who should be able to view this project?";

		public static $t_attributes_optional = "(Optional)";

		public static $t_attributes_clients = "Clients you've worked with";
		public static $t_attributes_institutions = "Choose your college or institution";
		public static $t_attributes_organizations = "Choose (or add) your company";
		public static $t_attributes_member = "";

	}

?>
