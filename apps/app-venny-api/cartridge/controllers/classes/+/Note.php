<?php

	// Note class
	class Note {

		private $user_obj;
		private $con;

		//
		public function __construct($con, $user) {

			$this->con = $con;
			$this->user_obj = new User($con, $user);

		}

		//public function submitNote($body, $user_to) {
		public function submitNote(

					$capture_topic,
					$capture_place,
					$capture_text,
					$date_opened,
					$capture_latitude,
					$capture_longitude,
					$capture_altitude,
					$capture_accuracy,
					$capture_location,
					$capture_address,
					$user_to

				)

			{

			//$note_ID = substr(md5(uniqid(microtime(true),true)),0,6);

			$capture_text = strip_tags($capture_text); //removes html tags
			$capture_text = mysqli_real_escape_string($this->con, $capture_text);
			$check_empty = preg_replace('/\s+/', '', $capture_text); // Deletes all spaces

			$capture_topic = strip_tags($capture_topic); //removes html tags
			$capture_topic = mysqli_real_escape_string($this->con, $capture_topic);

			//
			if($check_empty != "") {

				$body_array = preg_split("/\s+/", $capture_text);

				//
				foreach($body_array as $key => $value) {

					//
					if(strpos($value, "www.youtube.com/watch?v=") !== false) {

						$link = preg_split("!&!", $value);
						$value = preg_replace("!watch\?v=!", "embed/", $link[0]);
						$value = "<br><iframe width=\'420\' height=\'315\' src=\'" . $value ."\'></iframe><br>";
						$body_array[$key] = $value;

					}

				}

				$capture_text = implode(" ", $body_array);

				// Current date and time
				$date_closed = date("Y-m-d H:i:s");

				//Get username
				$added_by = $this->user_obj->getUsername();

				// If user is on own profile, user_to is 'none'
				if($user_to == $added_by) {

					$user_to = "none";

				}

				// insert note
				//$query = mysqli_query($this->con, "INSERT INTO notes (id,body,added_by,user_to,date_closed,user_closed,deleted,likes) VALUES (NULL, '$body', '$added_by', '$user_to', '$date_closed', 'no', 'no', '0')");
				$query = mysqli_query($this->con, "INSERT INTO notes (id,		body,							topic,						added_by,		user_to,		date_opened,		date_closed,		user_closed,	deleted,			likes,	latitude,							longitude,								altitude,							accuracy,							place,							location,							address,								note_ID)
																											VALUES (NULL, '$capture_text', '$capture_topic', '$added_by', '$user_to', '$date_opened', '$date_closed', 'no', 				'no', 				'0', 		'$capture_latitude',	 '$capture_longitude', 		'$capture_altitude', '$capture_accuracy', 	'$capture_place', 	'$capture_location', '$capture_address', 			'note_ID')");

				$returned_id = mysqli_insert_id($this->con);

				// Event
				//$event_ID = '1';
				//$event_type = '2';
				//$event_user = '3';
				//$event_ID = $note_ID;
				//$event_type = __FUNCTION__;
				//$event_user = $added_by;
				//$event_closed = $date_closed;
				//$event = mysqli_query($this->con, "INSERT INTO events (ID,event,type,user,closed) VALUES (NULL, '$event_ID', '$event_type', '$event_user', '$event_closed')");

				// Insert notification
				if($user_to != 'none') {

					$notification = new Notification($this->con, $added_by);
					$notification->insertNotificationNote($returned_id, $user_to, "like");

				}

				// Update note count for user
				$num_notes = $this->user_obj->getNumNotes();
				$num_notes++;
				$update_query = mysqli_query($this->con, "UPDATE users SET num_notes = '$num_notes' WHERE username = '$added_by'");

				$stopWords = "a about above across after again against all almost alone along already
				 also although always among am an and another any anybody anyone anything anywhere are
				 area areas around as ask asked asking asks at away b back backed backing backs be became
				 because become becomes been before began behind being beings best better between big
				 both but by c came can cannot case cases certain certainly clear clearly come could
				 d did differ different differently do does done down down downed downing downs during
				 e each early either end ended ending ends enough even evenly ever every everybody
				 everyone everything everywhere f face faces fact facts far felt few find finds first
				 for four from full fully further furthered furthering furthers g gave general generally
				 get gets give given gives go going good goods got great greater greatest group grouped
				 grouping groups h had has have having he her here herself high high high higher
			     highest him himself his how however i im if important in interest interested interesting
				 interests into is it its itself j just k keep keeps kind knew know known knows
				 large largely last later latest least less let lets like likely long longer
				 longest m made make making man many may me member members men might more most
				 mostly mr mrs much must my myself n necessary need needed needing needs never
				 new new newer newest next no nobody non noone not nothing now nowhere number
				 numbers o of off often old older oldest on once one only open opened opening
				 opens or order ordered ordering orders other others our out over p part parted
				 parting parts per perhaps place places point pointed pointing points possible
				 present presented presenting presents problem problems put puts q quite r
				 rather really right right room rooms s said same saw say says second seconds
				 see seem seemed seeming seems sees several shall she should show showed
				 showing shows side sides since small smaller smallest so some somebody
				 someone something somewhere state states still still such sure t take
				 taken than that the their them then there therefore these they thing
				 things think thinks this those though thought thoughts three through
		         thus to today together too took toward turn turned turning turns two
				 u under until up upon us use used uses v very w want wanted wanting
				 wants was way ways we well wells went were what when where whether
				 which while who whole whose why will with within without work
				 worked working works would x y year years yet you young younger
				 youngest your yours z lol haha omg hey ill iframe wonder else like
	             hate sleepy reason for some little yes bye choose";

	      //Convert stop words into array - split at white space
				$stopWords = preg_split("/[\s,]+/", $stopWords);

				// Remove all punctionation
				$no_punctuation = preg_replace("/[^a-zA-Z 0-9]+/", "", $capture_text);

				// Predict whether user is posting a url. If so, do not check for trending words
				if(strpos($no_punctuation, "height") === false && strpos($no_punctuation, "width") === false
					&& strpos($no_punctuation, "http") === false && strpos($no_punctuation, "youtube") === false) {

					//Convert users note (with punctuation removed) into array - split at white space
					$keywords = preg_split("/[\s,]+/", $no_punctuation);

					//
					foreach($stopWords as $value) {

						//
						foreach($keywords as $key => $value2) {

							//
							if(strtolower($value) == strtolower($value2)) {

								$keywords[$key] = "";

							}

						}

					}

					//
					foreach ($keywords as $value) {

					    $this->calculateTrend(ucfirst($value));

					}

        }

			}

		}

		//
		public function calculateTrend($term) {

			//
			if($term != '') {

				$query = mysqli_query($this->con, "SELECT * FROM trends WHERE title = '$term'");

				//
				if(mysqli_num_rows($query) == 0) {

					$insert_query = mysqli_query($this->con, "INSERT INTO trends (title,hits) VALUES ('$term','1')");

				}

				else {

					$insert_query = mysqli_query($this->con, "UPDATE trends SET hits = hits + 1 WHERE title = '$term'");

				}

			}

		}

		//
		public function loadNotesFriends($data, $limit) {

			$page = $data['page'];
			$userLoggedIn = $this->user_obj->getUsername();

			if($page == 1) {

				$start = 0;

			}

			else {

				$start = ($page - 1) * $limit;

			}

			$str = ""; //String to return
			$data_query = mysqli_query($this->con, "SELECT * FROM notes WHERE active = 1 AND deleted = 'no' AND access = 2 ORDER BY id DESC");

			//
			if(mysqli_num_rows($data_query) > 0) {

				$num_iterations = 0; //Number of results checked (not necasserily posted)
				$count = 1;

				//
				while($row = mysqli_fetch_array($data_query)) {

					$id = $row['id'];
					$note_ID = $row['note_ID'];
					$body = $row['body'];
					$topic = $row['topic'];
					$keywords = $row['keywords'];
					$added_by = $row['added_by'];
					$date_time = $row['date_closed'];

					// PLACE
					if($row['place'] == '') {
						$place = '';}
						else { $place = $row['place'];}

					//Prepare user_to string so it can be included even if not posted to a user
					if($row['user_to'] == "none") {

						$user_to = "";

					}

					else {

						$user_to_obj = new User($this->con, $row['user_to']);
						$user_to_name = $user_to_obj->getFirstAndLastName();
						$user_to = "to <a href='" . $row['user_to'] ."'>" . $user_to_name . "</a>";

					}

					//Check if user who posted, has their account closed
					$added_by_obj = new User($this->con, $added_by);

					if($added_by_obj->isClosed()) {

						continue;

					}

					$user_logged_obj = new User($this->con, $userLoggedIn);

					//
					if($user_logged_obj->isFriend($added_by)) {

						if($num_iterations++ < $start) {

							continue;

						}

						//Once 10 notes have been loaded, break
						if($count > $limit) {

							break;

						}

						else {

							$count++;

						}

						//
						if($userLoggedIn == $added_by) {

							$delete_button = "<button class='delete_button btn-danger' id='note$id'>X</button>";

						}

						else {

							$delete_button = "";

						}

						$user_details_query = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username = '$added_by'");
						$user_row = mysqli_fetch_array($user_details_query);
						$first_name = $user_row['first_name'];
						$last_name = $user_row['last_name'];
						$profile_pic = $user_row['profile_pic'];

						?>

						<script>

							function toggle<?php echo $id; ?>() {

								var target = $(event.target);

								if (!target.is("a")) {

									var element = document.getElementById("toggleComment<?php echo $id; ?>");

									if(element.style.display == "block") {

										element.style.display = "none";

									}

									else {

										element.style.display = "block";

									}

								}

							}

						</script>

						<?php

							$comments_check = mysqli_query($this->con, "SELECT * FROM comments WHERE note_id = '$id'");
							$comments_check_num = mysqli_num_rows($comments_check);

							//Timeframe
							$date_time_now = date("Y-m-d H:i:s");
							$start_date = new DateTime($date_time); //Time of note
							$end_date = new DateTime($date_time_now); //Current time
							$interval = $start_date->diff($end_date); //Difference between dates

							//
							if($interval->y >= 1) {

								if($interval == 1)
									$time_message = $interval->y . " year ago"; //1 year ago
								else
									$time_message = $interval->y . " years ago"; //1+ year ago

							}

							else if ($interval->m >= 1) {

								if($interval->d == 0) {
									$days = " ago";
								}
								else if($interval->d == 1) {
									$days = $interval->d . " day ago";
								}
								else {
									$days = $interval->d . " days ago";
								}

								if($interval->m == 1) {
									$time_message = $interval->m . " month". $days;
								}
								else {
									$time_message = $interval->m . " months". $days;
								}

							}

							else if($interval->d >= 1) {

								if($interval->d == 1) {
									$time_message = "Yesterday";
								}
								else {
									$time_message = $interval->d . " days ago";
								}

							}

							else if($interval->h >= 1) {

								if($interval->h == 1) {
									$time_message = $interval->h . " hour ago";
								}
								else {
									$time_message = $interval->h . " hours ago";
								}
							}

							else if($interval->i >= 1) {
								if($interval->i == 1) {
									$time_message = $interval->i . " minute ago";
								}
								else {
									$time_message = $interval->i . " minutes ago";
								}

							}

							else {
								if($interval->s < 30) {
									$time_message = "Just now";
								}
								else {
									$time_message = $interval->s . " seconds ago";
								}
							}

							$str .=
<<<DELIMITER

								<article class='note'>
									<h6>Note</h6>
									<ul class='row metadata optional'>
										<li class='topic-and-place'>
											<a href='template-user-topic.php?tpID={$topic}'>{$topic}</a> @
											<a href='template-user-place.php?plID={$place}'>{$place}</a>
										</li>
										<li class='time'>
											<a href='#'>{$time_message}</a>
										</li>
									</ul>
									<ul class='row content'>
										<li class='body'>
											<span class='added_by'><a onclick="button('user','profile','username={$added_by}');"><img src='{$profile_pic}'/> @{$added_by}</a></span>
											<span class='body' onclick="button('user','note','nID={$note_ID}');">{$body}</span>
										</li>
									</ul>
									<ul class='row options optional'>
										<li class='keywords'>
											<a href='#'>{$keywords}</a>
										</li>
										<li class='items'>
											<aside>
												<ul>
													<li class='comments'>
														<span class='object front'><a href='#'>{$comments_check_num}</a></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
													<li class='likes'>
														<span class='object front'><iframe src='template-user-notes-likes.php?note_id={$id}' width='50' height='25' scrolling='no'></iframe></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
													<li class='more'>
														<span class='object front'><a href='#'>More</a></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
												</ul>
											</aside>
										</li>
									</ul>
								</article>

DELIMITER;

						}

					?>

					<script>

						$(document).ready(function() {

							$('#note<?php echo $id; ?>').on('click', function() {
								bootbox.confirm("Are you sure you want to delete this note?", function(result) {

									$.note("engine/form_handlers/delete_note.php?note_id=<?php echo $id; ?>", {result:result});

									if(result) {

										location.reload();

									}

								});

							});

						});

					</script>

					<?php

				} //End while loop

				if($count > $limit) {

					$str .= "

						<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>

						<input type='hidden' class='noMoreNotes' value='false'>

					";

				}

				else {

					$str .= "

						<input type='hidden' class='noMoreNotes' value='true'>

						<p style='text-align: center;'>
							No more notes to show!
						</p>

					";

				}

			}

			echo $str;

		}

		//
		public function loadNotesofMine($data, $limit) {

			$page = $data['page'];
			$userLoggedIn = $this->user_obj->getUsername();

			if($page == 1) {

				$start = 0;

			}

			else {

				$start = ($page - 1) * $limit;

			}

			//echo $userLoggedIn;

			$str = ""; //String to return
			$data_query = mysqli_query($this->con, "SELECT * FROM notes WHERE active = 1 AND deleted = 'no' AND added_by = '$userLoggedIn' ORDER BY id DESC");

			//
			if(mysqli_num_rows($data_query) > 0) {

				$num_iterations = 0; //Number of results checked (not necasserily posted)
				$count = 1;

				//
				while($row = mysqli_fetch_array($data_query)) {

					$id = $row['id'];
					$body = $row['body'];
					$topic = $row['topic'];
					$place = $row['place'];
					$keywords = $row['keywords'];
					$added_by = $row['added_by'];
					$date_time = $row['date_closed'];
					$note_ID = $row['note_ID'];

					//Prepare user_to string so it can be included even if not posted to a user
					if($row['user_to'] == "none") {

						$user_to = "";

					}

					else {

						$user_to_obj = new User($this->con, $row['user_to']);
						$user_to_name = $user_to_obj->getFirstAndLastName();
						$user_to = "to <a href='" . $row['user_to'] ."'>" . $user_to_name . "</a>";

					}

					//Check if user who posted, has their account closed
					$added_by_obj = new User($this->con, $added_by);

					if($added_by_obj->isClosed()) {

						continue;

					}

					$user_logged_obj = new User($this->con, $userLoggedIn);

					//
					if($user_logged_obj->isFriend($added_by)) {

						if($num_iterations++ < $start) {

							continue;

						}

						//Once 10 notes have been loaded, break
						if($count > $limit) {

							break;

						}

						else {

							$count++;

						}

						//
						if($userLoggedIn == $added_by) {

							$delete_button = "<button class='delete_button btn-danger' id='note$id'>X</button>";

						}

						else {

							$delete_button = "";

						}

						$user_details_query = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username = '$added_by'");
						$user_row = mysqli_fetch_array($user_details_query);
						$first_name = $user_row['first_name'];
						$last_name = $user_row['last_name'];
						$profile_pic = $user_row['profile_pic'];

						?>

						<script>

							function toggle<?php echo $id; ?>() {

								var target = $(event.target);

								if (!target.is("a")) {

									var element = document.getElementById("toggleComment<?php echo $id; ?>");

									if(element.style.display == "block") {

										element.style.display = "none";

									}

									else {

										element.style.display = "block";

									}

								}

							}

						</script>

						<?php

							$comments_check = mysqli_query($this->con, "SELECT * FROM comments WHERE note_id = '$id'");
							$comments_check_num = mysqli_num_rows($comments_check);

							//Timeframe
							$date_time_now = date("Y-m-d H:i:s");
							$start_date = new DateTime($date_time); //Time of note
							$end_date = new DateTime($date_time_now); //Current time
							$interval = $start_date->diff($end_date); //Difference between dates

							//
							if($interval->y >= 1) {

								if($interval == 1)
									$time_message = $interval->y . " year ago"; //1 year ago
								else
									$time_message = $interval->y . " years ago"; //1+ year ago

							}

							else if ($interval->m >= 1) {

								if($interval->d == 0) {
									$days = " ago";
								}
								else if($interval->d == 1) {
									$days = $interval->d . " day ago";
								}
								else {
									$days = $interval->d . " days ago";
								}

								if($interval->m == 1) {
									$time_message = $interval->m . " month". $days;
								}
								else {
									$time_message = $interval->m . " months". $days;
								}

							}

							else if($interval->d >= 1) {

								if($interval->d == 1) {
									$time_message = "Yesterday";
								}
								else {
									$time_message = $interval->d . " days ago";
								}

							}

							else if($interval->h >= 1) {

								if($interval->h == 1) {
									$time_message = $interval->h . " hour ago";
								}
								else {
									$time_message = $interval->h . " hours ago";
								}
							}

							else if($interval->i >= 1) {
								if($interval->i == 1) {
									$time_message = $interval->i . " minute ago";
								}
								else {
									$time_message = $interval->i . " minutes ago";
								}

							}

							else {
								if($interval->s < 30) {
									$time_message = "Just now";
								}
								else {
									$time_message = $interval->s . " seconds ago";
								}
							}

							$str .= "

								<article class='note'>
									<h6>Note</h6>
									<ul class='row metadata optional'>
										<li class='topic-and-place'>
											<a href='template-user-topic.php?tpID={$topic}'>$topic</a> @ <a href='#'>$place</a>
										</li>
										<li class='time'>
											<a href='#'>$time_message</a>
										</li>
									</ul>
									<ul class='row content'>
										<li class='body'>
										<a href='template-user-note.php?nID={$note_ID}'>{$body}</a>
										</li>
									</ul>
									<ul class='row options optional'>
										<li class='keywords'>
											<a href='#'>$keywords</a>
										</li>
										<li class='items'>
											<aside>
												<ul>
													<li class='comments'>
														<span class='object front'><a href='#'>$comments_check_num</a></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
													<li class='likes'>
														<span class='object front'><iframe src='template-user-notes-likes.php?note_id=$id' width='50' height='25' scrolling='no'></iframe></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
													<li class='more'>
														<span class='object front'><a href='#'>More</a></span>
														<span class='object back'><img src='static/images/icons/spacer.gif'></span>
													</li>
												</ul>
											</aside>
										</li>
									</ul>
								</article>

							";

						}

					?>

					<script>

						$(document).ready(function() {

							$('#note<?php echo $id; ?>').on('click', function() {
								bootbox.confirm("Are you sure you want to delete this note?", function(result) {

									$.note("engine/form_handlers/delete_note.php?note_id=<?php echo $id; ?>", {result:result});

									if(result) {

										location.reload();

									}

								});

							});

						});

					</script>

					<?php

				} //End while loop

				if($count > $limit) {

					$str .= "

						<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>

						<input type='hidden' class='noMoreNotes' value='false'>

					";

				}

				else {

					$str .= "

						<input type='hidden' class='noMoreNotes' value='true'>

						<p style='text-align: center;'>
							No more notes to show!
						</p>

					";

				}

			}

			echo $str;

		}

		//
		public function loadProfileNotes($data, $limit) {

			$page = $data['page'];
			$profileUser = $data['profileUsername'];
			$userLoggedIn = $this->user_obj->getUsername();

			if($page == 1) {

				$start = 0;

			}

			else {

				$start = ($page - 1) * $limit;

			}

			$str = ""; //String to return
			$data_query = mysqli_query($this->con, "SELECT * FROM notes WHERE active = 1 AND deleted = 'no' AND access = 2 AND ((added_by = '$profileUser' AND user_to = 'none') OR user_to = '$profileUser')  ORDER BY id DESC");

			//
			if(mysqli_num_rows($data_query) > 0) {

				$num_iterations = 0; //Number of results checked (not necasserily posted)
				$count = 1;

				//
				while($row = mysqli_fetch_array($data_query)) {

					$id = $row['id'];
					$body = $row['body'];
					$topic = $row['topic'];
					$place = $row['place'];
					$keywords = $row['keywords'];
					$added_by = $row['added_by'];
					$date_time = $row['date_closed'];

					//
					if($num_iterations++ < $start) {

							continue;

					}

					//Once 10 notes have been loaded, break
					if($count > $limit) {

						break;

					}

					else {

						$count++;

					}

					if($userLoggedIn == $added_by) {

						$delete_button = "<button class='delete_button btn-danger' id='note$id'>X</button>";

					}

					else {

						$delete_button = "";

					}

					$user_details_query = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username='$added_by'");
					$user_row = mysqli_fetch_array($user_details_query);
					$first_name = $user_row['first_name'];
					$last_name = $user_row['last_name'];
					$profile_pic = $user_row['profile_pic'];

				?>

						<script>

							function toggle<?php echo $id; ?>() {

								var target = $(event.target);
								if (!target.is("a")) {
									var element = document.getElementById("toggleComment<?php echo $id; ?>");

									if(element.style.display == "block")
										element.style.display = "none";
									else
										element.style.display = "block";
								}
							}

						</script>
						<?php

						$comments_check = mysqli_query($this->con, "SELECT * FROM comments WHERE note_id = '$id'");
						$comments_check_num = mysqli_num_rows($comments_check);

						//Timeframe
						$date_time_now = date("Y-m-d H:i:s");
						$start_date = new DateTime($date_time); //Time of note
						$end_date = new DateTime($date_time_now); //Current time
						$interval = $start_date->diff($end_date); //Difference between dates
						if($interval->y >= 1) {
							if($interval == 1)
								$time_message = $interval->y . " year ago"; //1 year ago
							else
								$time_message = $interval->y . " years ago"; //1+ year ago
						}
						else if ($interval->m >= 1) {
							if($interval->d == 0) {
								$days = " ago";
							}
							else if($interval->d == 1) {
								$days = $interval->d . " day ago";
							}
							else {
								$days = $interval->d . " days ago";
							}

							if($interval->m == 1) {
								$time_message = $interval->m . " month". $days;
							}
							else {
								$time_message = $interval->m . " months". $days;
							}

						}
						else if($interval->d >= 1) {
							if($interval->d == 1) {
								$time_message = "Yesterday";
							}
							else {
								$time_message = $interval->d . " days ago";
							}
						}
						else if($interval->h >= 1) {
							if($interval->h == 1) {
								$time_message = $interval->h . " hour ago";
							}
							else {
								$time_message = $interval->h . " hours ago";
							}
						}
						else if($interval->i >= 1) {
							if($interval->i == 1) {
								$time_message = $interval->i . " minute ago";
							}
							else {
								$time_message = $interval->i . " minutes ago";
							}
						}
						else {
							if($interval->s < 30) {
								$time_message = "Just now";
							}
							else {
								$time_message = $interval->s . " seconds ago";
							}
						}

						$str .= "

							<article class='note'>
								<h6>Note</h6>
								<ul class='row metadata optional'>
									<li class='topic-and-place'>
										<a href='template-user-topic.php?tpID={$topic}'>$topic</a>
										@
										<a href='template-user-place.php?tpID={$place}'>$place</a>
									</li>
									<li class='time'>
										<a href='#'>$time_message</a>
									</li>
								</ul>
								<ul class='row content'>
									<li class='body'>
										$body
									</li>
								</ul>
								<ul class='row options optional'>
									<li class='keywords'>
										<a href='#'>$keywords</a>
									</li>
									<li class='items'>
										<aside>
											<ul>
												<li class='comments'>
													<span class='object front'><a href='#'>$comments_check_num</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
												</li>
												<li class='likes'>
													<span class='object front'><iframe src='template-user-notes-likes.php?note_id=$id' width='50' height='25' scrolling='no'></iframe></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
												</li>
												<li class='more'>
													<span class='object front'><a href='#'>More</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
												</li>
											</ul>
										</aside>
									</li>
								</ul>
							</article>

						";

					?>
					<script>

						$(document).ready(function() {

							$('#note<?php echo $id; ?>').on('click', function() {

								bootbox.confirm("Are you sure you want to delete this note?", function(result) {

									$.note("engine/form_handlers/delete_note.php?note_id=<?php echo $id; ?>", {result:result});

									if(result) {

										location.reload();

									}

								});

							});

						});

					</script>

					<?php

				} //End while loop

				if($count > $limit) {

					$str .= "

						<input type='hidden' class='nextPage' value='" . ($page + 1) . "'>

						<input type='hidden' class='noMoreNotes' value='false'>

					";

				}

				else {

					$str .= "

						<input type='hidden' class='noMoreNotes' value='true'>

						<p style='text-align: centre;'> No more notes to show! </p>

					";

				}

			}

			echo $str;

		}

		//
		public function getSingleNote($note_id) {

			$userLoggedIn = $this->user_obj->getUsername();

			//$opened_query = mysqli_query($this->con, "UPDATE notifications SET opened = 'yes' WHERE user_to = '$userLoggedIn' AND link LIKE '%=$note_id'");

			$str = ""; //String to return
			$data_query = mysqli_query($this->con, "SELECT * FROM notes WHERE active = 1 AND deleted = 'no' AND id = '$note_id' LIMIT 1");

			//
			if(mysqli_num_rows($data_query) > 0) {

				$row = mysqli_fetch_array($data_query);

					$id = $row['id'];
					$note_ID = $row['note_ID'];
					$body = $row['body'];
					$topic = $row['topic'];
					$place = $row['place'];
					$keywords = $row['keywords'];
					$added_by = $row['added_by'];
					$date_time = $row['date_closed'];

					//Prepare user_to string so it can be included even if not posted to a user
					if($row['user_to'] == "none") {

						$user_to = "";

					}

					else {

						$user_to_obj = new User($this->con, $row['user_to']);
						$user_to_name = $user_to_obj->getFirstAndLastName();
						$user_to = "to <a href='" . $row['user_to'] ."'>" . $user_to_name . "</a>";

					}

					//Check if user who posted, has their account closed
					$added_by_obj = new User($this->con, $added_by);

					//
					if($added_by_obj->isClosed()) {

						return;

					}

					$user_logged_obj = new User($this->con, $userLoggedIn);

					//
					if($user_logged_obj->isFriend($added_by)) {

						//
						if($userLoggedIn == $added_by) {

							$delete_button = "<button class='delete_button btn-danger' id='note$id'>X</button>";

						}

						else {

							$delete_button = "";

						}

						$user_details_query = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username='$added_by'");
						$user_row = mysqli_fetch_array($user_details_query);
						$first_name = $user_row['first_name'];
						$last_name = $user_row['last_name'];
						$profile_pic = $user_row['profile_pic'];

						?>

						<script>

							function toggle<?php echo $id; ?>() {

								var target = $(event.target);
								if (!target.is("a")) {
									var element = document.getElementById("toggleComment<?php echo $id; ?>");

									if(element.style.display == "block")
										element.style.display = "none";
									else
										element.style.display = "block";
								}
							}

						</script>

						<?php

						$comments_check = mysqli_query($this->con, "SELECT * FROM comments WHERE note_id = '$id'");
						$comments_check_num = mysqli_num_rows($comments_check);

						//Timeframe
						$date_time_now = date("Y-m-d H:i:s");
						$start_date = new DateTime($date_time); //Time of note
						$end_date = new DateTime($date_time_now); //Current time
						$interval = $start_date->diff($end_date); //Difference between dates
						if($interval->y >= 1) {
							if($interval == 1)
								$time_message = $interval->y . " year ago"; //1 year ago
							else
								$time_message = $interval->y . " years ago"; //1+ year ago
						}
						else if ($interval->m >= 1) {
							if($interval->d == 0) {
								$days = " ago";
							}
							else if($interval->d == 1) {
								$days = $interval->d . " day ago";
							}
							else {
								$days = $interval->d . " days ago";
							}

							if($interval->m == 1) {
								$time_message = $interval->m . " month". $days;
							}
							else {
								$time_message = $interval->m . " months". $days;
							}

						}
						else if($interval->d >= 1) {
							if($interval->d == 1) {
								$time_message = "Yesterday";
							}
							else {
								$time_message = $interval->d . " days ago";
							}
						}
						else if($interval->h >= 1) {
							if($interval->h == 1) {
								$time_message = $interval->h . " hour ago";
							}
							else {
								$time_message = $interval->h . " hours ago";
							}
						}
						else if($interval->i >= 1) {
							if($interval->i == 1) {
								$time_message = $interval->i . " minute ago";
							}
							else {
								$time_message = $interval->i . " minutes ago";
							}
						}
						else {
							if($interval->s < 30) {
								$time_message = "Just now";
							}
							else {
								$time_message = $interval->s . " seconds ago";
							}
						}

						$str .= "

							<article class='note'>
							  <h6>Note</h6>
							  <ul class='row metadata optional'>
							    <li class='topic-and-place'>
							      <a href='template-user-topic.php?tpID={$topic}'>$topic</a> @ <a href='#'>$place</a>
							    </li>
							    <li class='time'>
							      <a href='#'>$time_message</a>
							    </li>
							  </ul>
							  <ul class='row content'>
							    <li class='body'>
							      $body
							    </li>
							  </ul>
							  <ul class='row options optional'>
							    <li class='keywords'>
							      <a href='#'>$keywords</a>
							    </li>
							    <li class='items'>
							      <aside>
							        <ul>
							          <li class='comments'>
							            <span class='object front'><a href='#'>$comments_check_num</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
							          </li>
							          <li class='likes'>
							            <span class='object front'><iframe src='template-user-notes-likes.php?note_id=$id' width='50' height='25' scrolling='no'></iframe></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
												</li>
							          <li class='more'>
							            <span class='object front'><a href='#'>More</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
							          </li>
							        </ul>
							      </aside>
							    </li>
							  </ul>
							</article>

							<iframe src='template-user-notes-comments.php?note_id=$id' id='comment_iframe' frameborder='0' height='300' width='100%'></iframe>

						";

					?>
					<script>

						$(document).ready(function() {

							$('#note<?php echo $id; ?>').on('click', function() {
								bootbox.confirm("Are you sure you want to delete this note?", function(result) {

									$.note("engine/form_handlers/delete_note.php?note_id=<?php echo $id; ?>", {result:result});

									if(result)
										location.reload();

								});
							});

						});

					</script>
					<?php
					}

					else {

						echo "<p>You cannot see this note because you are not friends with this user.</p>";

						return;

					}

			}

			else {

				echo "<p>No note found. If you clicked a link, it may be broken.</p>";

				return;

			}

			echo $str;

		}

		//
		public function getNote($note_ID) {

			$userLoggedIn = $this->user_obj->getUsername();

			//$opened_query = mysqli_query($this->con, "UPDATE notifications SET opened = 'yes' WHERE user_to = '$userLoggedIn' AND link LIKE '%=$note_id'");

			$str = ""; //String to return
			$data_query = mysqli_query($this->con, "SELECT * FROM notes WHERE active = 1 AND deleted = 'no' AND note_ID = '$note_ID' LIMIT 1");

			//
			if(mysqli_num_rows($data_query) > 0) {

				$row = mysqli_fetch_array($data_query);

					$id = $row['id'];
					$note_ID = $row['note_ID'];
					$body = $row['body']; //$body = convert_links($body);
					$topic = $row['topic'];
					$place = $row['place'];
					$keywords = $row['keywords'];
					$added_by = $row['added_by'];
					$date_time = $row['date_closed'];

					//Prepare user_to string so it can be included even if not posted to a user
					if($row['user_to'] == "none") {

						$user_to = "";

					}

					else {

						$user_to_obj = new User($this->con, $row['user_to']);
						$user_to_name = $user_to_obj->getFirstAndLastName();
						$user_to = "to <a href='" . $row['user_to'] ."'>" . $user_to_name . "</a>";

					}

					//Check if user who posted, has their account closed
					$added_by_obj = new User($this->con, $added_by);

					//
					if($added_by_obj->isClosed()) {

						return;

					}

					$user_logged_obj = new User($this->con, $userLoggedIn);

					//
					if($user_logged_obj->isFriend($added_by)) {

						//
						if($userLoggedIn == $added_by) {

							$delete_button = "<button class='delete_button btn-danger' id='note$id'>X</button>";

						}

						else {

							$delete_button = "";

						}

						$user_details_query = mysqli_query($this->con, "SELECT first_name, last_name, profile_pic FROM users WHERE username = '$added_by'");
						$user_row = mysqli_fetch_array($user_details_query);
						$first_name = $user_row['first_name'];
						$last_name = $user_row['last_name'];
						$profile_pic = $user_row['profile_pic'];

						?>

						<script>

							function toggle<?php echo $id; ?>() {

								var target = $(event.target);
								if (!target.is("a")) {
									var element = document.getElementById("toggleComment<?php echo $id; ?>");

									if(element.style.display == "block")
										element.style.display = "none";
									else
										element.style.display = "block";
								}
							}

						</script>

						<?php

						$comments_check = mysqli_query($this->con, "SELECT * FROM comments WHERE note_id = '$id'");
						$comments_check_num = mysqli_num_rows($comments_check);

						//Timeframe
						$date_time_now = date("Y-m-d H:i:s");
						$start_date = new DateTime($date_time); //Time of note
						$end_date = new DateTime($date_time_now); //Current time
						$interval = $start_date->diff($end_date); //Difference between dates
						if($interval->y >= 1) {
							if($interval == 1)
								$time_message = $interval->y . " year ago"; //1 year ago
							else
								$time_message = $interval->y . " years ago"; //1+ year ago
						}
						else if ($interval->m >= 1) {
							if($interval->d == 0) {
								$days = " ago";
							}
							else if($interval->d == 1) {
								$days = $interval->d . " day ago";
							}
							else {
								$days = $interval->d . " days ago";
							}

							if($interval->m == 1) {
								$time_message = $interval->m . " month". $days;
							}
							else {
								$time_message = $interval->m . " months". $days;
							}

						}
						else if($interval->d >= 1) {
							if($interval->d == 1) {
								$time_message = "Yesterday";
							}
							else {
								$time_message = $interval->d . " days ago";
							}
						}
						else if($interval->h >= 1) {
							if($interval->h == 1) {
								$time_message = $interval->h . " hour ago";
							}
							else {
								$time_message = $interval->h . " hours ago";
							}
						}
						else if($interval->i >= 1) {
							if($interval->i == 1) {
								$time_message = $interval->i . " minute ago";
							}
							else {
								$time_message = $interval->i . " minutes ago";
							}
						}
						else {
							if($interval->s < 30) {
								$time_message = "Just now";
							}
							else {
								$time_message = $interval->s . " seconds ago";
							}
						}

						$str .= "

							<article class='note'>
							  <h6>Note</h6>
							  <ul class='row metadata optional'>
							    <li class='topic-and-place'>
							      <a href='template-user-topic.php?tpID={$topic}'>$topic</a>
										@
										<a href='template-user-place.php?plID={$place}'>$place</a>
							    </li>
							    <li class='time'>
							      <a href='#'>$time_message</a>
							    </li>
							  </ul>
							  <ul class='row content'>
							    <li class='body'>
							      $body
							    </li>
							  </ul>
							  <ul class='row options optional'>
							    <li class='keywords'>
							      <a href='#'>$keywords</a>
							    </li>
							    <li class='items'>
							      <aside>
							        <ul>
							          <li class='comments'>
							            <span class='object front'><a href='#'>$comments_check_num</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
							          </li>
							          <li class='likes'>
							            <span class='object front'><iframe src='template-user-notes-likes.php?note_id=$id' width='50' height='25' scrolling='no'></iframe></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
												</li>
							          <li class='more'>
							            <span class='object front'><a href='#'>More</a></span>
													<span class='object back'><img src='static/images/icons/spacer.gif'></span>
							          </li>
							        </ul>
							      </aside>
							    </li>
							  </ul>
							</article>

							<iframe src='template-user-notes-comments.php?note_id=$id' id='comment_iframe' frameborder='0' height='300' width='100%'></iframe>

						";

					?>
					<script>

						$(document).ready(function() {

							$('#note<?php echo $id; ?>').on('click', function() {
								bootbox.confirm("Are you sure you want to delete this note?", function(result) {

									$.note("engine/form_handlers/delete_note.php?note_id=<?php echo $id; ?>", {result:result});

									if(result)
										location.reload();

								});
							});

						});

					</script>
					<?php
					}

					else {

						echo "<p>You cannot see this note because you are not friends with this user.</p>";

						return;

					}

			}

			else {

				echo "<p>No note found. If you clicked a link, it may be broken.</p>";

				return;

			}

			$str = converttoSocial($str);

			echo $str;

		}

		//
		public function capture(

			$capture_topic,
			$capture_place,
			$capture_body,
			$date_opened,
			$capture_latitude,
			$capture_longitude,
			$capture_altitude,
			$capture_accuracy,
			$capture_location,
			$capture_address,
			$capture_access,
			$user_to

			) {

			// Body...
			$capture_body = strip_tags($capture_body); //removes html tags
			$capture_body = mysqli_real_escape_string($this->con, $capture_body);
			$check_empty = preg_replace('/\s+/', '', $capture_body); //Deletes all spaces

			// Tags
			$capture_tags = gethashtags($capture_body);
			set_message($capture_tags);

			// Keywords
			$capture_keywords = $capture_tags;

			// Topic...
			$capture_topic = strip_tags($capture_topic); //removes html tags
			$capture_topic = mysqli_real_escape_string($this->con, $capture_topic);

			// Place...
			$capture_place = strip_tags($capture_place); //removes html tags
			$capture_place = mysqli_real_escape_string($this->con, $capture_place);

			// Address...
			$capture_address = strip_tags($capture_address); //removes html tags
			$capture_address = mysqli_real_escape_string($this->con, $capture_address);

			// Location...
			$capture_location = strip_tags($capture_location); //removes html tags
			$capture_location = mysqli_real_escape_string($this->con, $capture_location);

			// Latitude...
			if(empty($capture_latitude)) {

				$capture_latitude = 'NULL';

			}

			// Longitude...
			if(empty($capture_longitude)) {

				$capture_longitude = 'NULL';

			}

			// Altitude...
			if(empty($capture_altitude)) {

				$capture_altitude = 'NULL';

			}

			// Accuracy...
			if(empty($capture_accuracy)) {

				$capture_accuracy = 'NULL';

			}

			$_SESSION['ghost_topic'] = $capture_topic; //Stores topic into session variable
			$_SESSION['ghost_place'] = $capture_place; //Stores place into session variable
			$_SESSION['ghost_location'] = $capture_location; //Stores location into session variable
			$_SESSION['ghost_address'] = $capture_address; //Stores address into session variable
			$_SESSION['ghost_latitude'] = $capture_latitude; //Stores latitude into session variable
			$_SESSION['ghost_longitude'] = $capture_longitude; //Stores longitude into session variable
			$_SESSION['ghost_altitude'] = $capture_altitude; //Stores altitude into session variable
			$_SESSION['ghost_accuracy'] = $capture_accuracy; //Stores accuracy into session variable

			//
			if($check_empty != "") {

				$body_array = preg_split("/\s+/", $capture_body);

				foreach($body_array as $key => $value) {

					if(strpos($value, "www.youtube.com/watch?v=") !== false) {

						$link = preg_split("!&!", $value);
						$value = preg_replace("!watch\?v=!", "embed/", $link[0]);
						$value = "<br><iframe width=\'420\' height=\'315\' src=\'" . $value ."\'></iframe><br>";
						$body_array[$key] = $value;

					}

				}

				$capture_body = implode(" ", $body_array);

				//Current date and time
				$date_added = date("Y-m-d H:i:s");
				//Get username
				$added_by = $this->user_obj->getUsername();

				//If user is on own profile, user_to is 'none'
				if($user_to == $added_by) {

					$user_to = "none";

				}

				$note_ID = substr(md5(uniqid(microtime(true),true)),0,8);
				$stage_ID = substr(md5(uniqid(microtime(true),true)),0,8);

				//insert note
				$query = mysqli_query(

					$this->con, "INSERT INTO notes (

						id,

						body,
						added_by,
						user_to,
						date_opened,
						date_closed,
						user_closed,
						deleted,
						likes,
						topic,
						place,
						address,
						location,
						latitude,
						longitude,
						altitude,
						accuracy,
						keywords,
						tags,
						note_ID,
						stage_ID,
						access

					) VALUES (

						NULL, /* id, */

						'$capture_body', /* body, */
						'$added_by', /* added_by, */
						'$user_to', /* user_to, */
						'$date_opened', /* date_opened, */
						'$date_added', /* date_closed, */
						'no', /* user_closed, */
						'no', /* deleted, */
						0, /* likes */
						'$capture_topic', /* topic */
						'$capture_place', /* place */
						'$capture_address', /* address */
						'$capture_location', /* location */
						$capture_latitude, /* latitude */
						$capture_longitude, /* longitude */
						$capture_altitude, /* altitude */
						$capture_accuracy, /* accuracy */
						'$capture_keywords', /* keywords */
						'$capture_tags', /* tags */
						'$note_ID', /* note_ID */
						'$stage_ID', /* stage_ID */
						$capture_access /* access */

					)"

				);
				//$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
				$returned_id = mysqli_insert_id($this->con);

				//Insert notification
				if($user_to != 'none') {

					$notification = new Notification($this->con, $added_by);
					$notification->insertNotificationNote($returned_id, $user_to, "like");

				}

				//Update note count for user
				$num_notes = $this->user_obj->getNumNotes();
				$num_notes++;
				$update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");

				$stopWords = "a about above across after again against all almost alone along already
				 also although always among am an and another any anybody anyone anything anywhere are
				 area areas around as ask asked asking asks at away b back backed backing backs be became
				 because become becomes been before began behind being beings best better between big
				 both but by c came can cannot case cases certain certainly clear clearly come could
				 d did differ different differently do does done down down downed downing downs during
				 e each early either end ended ending ends enough even evenly ever every everybody
				 everyone everything everywhere f face faces fact facts far felt few find finds first
				 for four from full fully further furthered furthering furthers g gave general generally
				 get gets give given gives go going good goods got great greater greatest group grouped
				 grouping groups h had has have having he her here herself high high high higher
					 highest him himself his how however i im if important in interest interested interesting
				 interests into is it its itself j just k keep keeps kind knew know known knows
				 large largely last later latest least less let lets like likely long longer
				 longest m made make making man many may me member members men might more most
				 mostly mr mrs much must my myself n necessary need needed needing needs never
				 new new newer newest next no nobody non noone not nothing now nowhere number
				 numbers o of off often old older oldest on once one only open opened opening
				 opens or order ordered ordering orders other others our out over p part parted
				 parting parts per perhaps place places point pointed pointing points possible
				 present presented presenting presents problem problems put puts q quite r
				 rather really right right room rooms s said same saw say says second seconds
				 see seem seemed seeming seems sees several shall she should show showed
				 showing shows side sides since small smaller smallest so some somebody
				 someone something somewhere state states still still such sure t take
				 taken than that the their them then there therefore these they thing
				 things think thinks this those though thought thoughts three through
						 thus to today together too took toward turn turned turning turns two
				 u under until up upon us use used uses v very w want wanted wanting
				 wants was way ways we well wells went were what when where whether
				 which while who whole whose why will with within without work
				 worked working works would x y year years yet you young younger
				 youngest your yours z lol haha omg hey ill iframe wonder else like
							 hate sleepy reason for some little yes bye choose";

							 //Convert stop words into array - split at white space
				$stopWords = preg_split("/[\s,]+/", $stopWords);

				//Remove all punctionation
				$no_punctuation = preg_replace("/[^a-zA-Z 0-9]+/", "", $capture_body);

				//Predict whether user is posting a url. If so, do not check for trending words
				if(strpos($no_punctuation, "height") === false && strpos($no_punctuation, "width") === false
					&& strpos($no_punctuation, "http") === false && strpos($no_punctuation, "youtube") === false){
					//Convert users note (with punctuation removed) into array - split at white space
					$keywords = preg_split("/[\s,]+/", $no_punctuation);

					foreach($stopWords as $value) {
						foreach($keywords as $key => $value2){
							if(strtolower($value) == strtolower($value2))
								$keywords[$key] = "";
						}
					}

					foreach ($keywords as $value) {
							$this->calculateTrend(ucfirst($value));
					}

							 }

			}

		}

	}

?>
