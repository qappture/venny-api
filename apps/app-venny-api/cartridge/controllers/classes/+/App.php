<?php

	//
	class App {

		//
		public function __construct($db, $app_ID = NULL) {

			//
			$this->db = $db;
			$this->app_ID = $app_ID;

		}

		//
		public function getApp($app_ID = NULL) {

			//
			$statement =

				"SELECT

					app_ID,
					app_name,
					app_website,
					app_industry,
					app_email,
					app_description,
					app_type,
					partner_ID,
					event_ID,
					process_ID

				"
			;
			$statement .= " FROM apps ";
			$statement .= " WHERE active = 1 ";

			//
			if($app_ID !== NULL) {

				//
				$conditions = " AND app_ID = " . $_GET['id'];
				$limit = " LIMIT 1 ";

				//
				$statement .= $conditions;
				$statement .= $limit;

			} else {

				//
				$conditions = "";
				if(isset($_GET['partner_ID'])){$partner_ID=$_GET['partner_ID'];$conditions.="AND partner_ID = ".$partner_ID;}else{$conditions.="";}

				//
				$statement .= $conditions;
				$statement .= " ORDER BY app_name ASC ";
				$statement .= $limit;

			}

			//
			$query = $this->db->prepare($statement);

			//
			if($app_ID !== NULL) {

				$query->bindParam(":app_ID", $app_ID);

			}

			$query->execute();

			//
			while($this->result = $query->fetch(PDO::FETCH_ASSOC)) {

				//
				$results[] = array(

					'app_ID' => $this->result['app_ID'],
					'name' => $this->result['app_name'],
					'website' => $this->result['app_website'],
					'industry' => $this->result['app_industry'],
					'email' => $this->result['app_email'],
					'description' => $this->result['app_description'],
					'type' => $this->result['app_type'],
					'partner_ID' => $this->result['partner_ID'],
					'event_ID' => $this->result['event_ID'],
					'process_ID' => $this->result['process_ID']

				);

			}

			//
			$response = new Response($results);
			$response->generateResponse('external', 'successful', 200, $response->count, $results);

		}

	}

?>
