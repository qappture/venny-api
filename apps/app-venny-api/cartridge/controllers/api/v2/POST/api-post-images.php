<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $image_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['type'])){$image_type = clean($_REQUEST['type']);}else{$image_type=NULL;}
  if(isset($_REQUEST['status'])){$image_status = clean($_REQUEST['status']);}else{$image_status=NULL;}
  if(isset($_REQUEST['primary'])){$image_primary = clean($_REQUEST['primary']);}else{$image_primary=NULL;}
  if(isset($_REQUEST['object'])){$image_object = clean($_REQUEST['object']);}else{$image_object=NULL;}
  if(isset($_REQUEST['caption'])){$image_caption = clean($_REQUEST['caption']);}else{$image_caption=NULL;}
  if(isset($_REQUEST['filename'])){$image_filename = clean($_REQUEST['filename']);}else{$image_filename=NULL;}
  if(isset($_REQUEST['metadata'])){$image_metadata = clean($_REQUEST['metadata']);}else{$image_metadata=NULL;}
  if(isset($_REQUEST['profile_ID'])){$profile_ID = clean($_REQUEST['profile_ID']);}else{$profile_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      image_ID,
      image_type,
      image_status,
      image_primary,
      image_object,
      image_caption,
      image_filename,
      image_metadata,
      profile_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$image_ID',
      '$image_type',
      '$image_status',
      '$image_primary',
      '$image_object',
      '$image_caption',
      '$image_filename',
      '$image_metadata',
      '$profile_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
