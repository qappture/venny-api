<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $idea_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['text'])){$idea_text = clean($_REQUEST['text']);}else{$idea_text=NULL;}
  if(isset($_REQUEST['x'])){$idea_x = clean($_REQUEST['x']);}else{$idea_x=NULL;}
  if(isset($_REQUEST['y'])){$idea_y = clean($_REQUEST['y']);}else{$idea_y=NULL;}
  if(isset($_REQUEST['z'])){$idea_z = clean($_REQUEST['z']);}else{$idea_z=NULL;}
  if(isset($_REQUEST['width'])){$idea_width = clean($_REQUEST['width']);}else{$idea_width=NULL;}
  if(isset($_REQUEST['height'])){$idea_height = clean($_REQUEST['height']);}else{$idea_height=NULL;}
  if(isset($_REQUEST['stage_ID'])){$stage_ID = clean($_REQUEST['stage_ID']);}else{$stage_ID=NULL;}
  if(isset($_REQUEST['post_ID'])){$post_ID = clean($_REQUEST['post_ID']);}else{$post_ID=NULL;}
  if(isset($_REQUEST['excerpt_ID'])){$excerpt_ID = clean($_REQUEST['excerpt_ID']);}else{$excerpt_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      idea_ID,
      idea_text,
      idea_x,
      idea_y,
      idea_z,
      idea_width,
      idea_height,
      stage_ID,
      post_ID,
      excerpt_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$idea_ID',
      '$idea_text',
      '$idea_x',
      '$idea_y',
      '$idea_z',
      '$idea_width',
      '$idea_height',
      '$stage_ID',
      '$post_ID',
      '$excerpt_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
