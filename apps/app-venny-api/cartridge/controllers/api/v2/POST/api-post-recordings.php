<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $recording_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['type'])){$recording_type = clean($_REQUEST['type']);}else{$recording_type=NULL;}
  if(isset($_REQUEST['source'])){$recording_source = clean($_REQUEST['source']);}else{$recording_source=NULL;}
  if(isset($_REQUEST['length'])){$recording_length = clean($_REQUEST['length']);}else{$recording_length=NULL;}
  if(isset($_REQUEST['cues'])){$recording_cues = clean($_REQUEST['cues']);}else{$recording_cues=NULL;}
  if(isset($_REQUEST['start_time'])){$recording_start_time = clean($_REQUEST['start_time']);}else{$recording_start_time=NULL;}
  if(isset($_REQUEST['end_time'])){$recording_end_time = clean($_REQUEST['end_time']);}else{$recording_end_time=NULL;}
  if(isset($_REQUEST['stage_ID'])){$stage_ID = clean($_REQUEST['stage_ID']);}else{$stage_ID=NULL;}
  if(isset($_REQUEST['attachment_ID'])){$attachment_ID = clean($_REQUEST['attachment_ID']);}else{$attachment_ID=NULL;}
  if(isset($_REQUEST['post_ID'])){$post_ID = clean($_REQUEST['post_ID']);}else{$post_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      recording_ID,
      recording_type,
      recording_source,
      recording_length,
      recording_cues,
      recording_start_time,
      recording_end_time,
      stage_ID,
      attachment_ID,
      post_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$recording_ID',
      '$recording_type',
      '$recording_source',
      '$recording_length',
      '$recording_cues',
      '$recording_start_time',
      '$recording_end_time',
      '$stage_ID',
      '$attachment_ID',
      '$post_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
