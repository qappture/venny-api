<?php

  //
  require 'controllers/classes/Onboarding.php';

  $host='';$user='';$pass='';$name='';
  $first_name='';$last_name='';$email='';$phone='';

  $onboarding = new Onboarding($host, $user, $pass, $name);
  //$access->connect();
  //$access->disconnect();

  // STEP 3. Insert user information
  $result = $onboarding->createPerson($first_name, $last_name = NULL, $email, $phone = NULL);
  // successfully registered

  exit;
  if ($result) {

    // get current registered user information and store in $user
    $user = $access->selectUser($username);

    // declare information to feedback to user of App as json
    $returnArray["status"] = "200";
    $returnArray["message"] = "Successfully registered";
    $returnArray["id"] = $user["id"];
    $returnArray["username"] = $user["username"];
    $returnArray["email"] = $user["email"];
    $returnArray["fullname"] = $user["fullname"];
    $returnArray["ava"] = $user["ava"];

    // STEP 4. Emailing
    // include email.php
    require ("email.php");

    // store all class in $email var
    $email = new notification();

    // store generated token in $token var
    $token = $email->generateToken(20);

    // save inf in 'emailTokens' table
    $access->saveToken("emailTokens", $user["id"], $token);

    // refer emailing information
    $details = array();
    $details["subject"] = "Email confirmation on Venny";
    $details["to"] = $user["email"];
    $details["fromName"] = "sonofadolphus";
    $details["fromEmail"] = "sonofadolphus@gmail.com";

    // access template file
    $template = $email->confirmationTemplate();

    // replace {token} from confirmationTemplate.html by $token and store all content in $template var
    $template = str_replace("{token}", $token, $template);

    $details["body"] = $template;

    $email->sendEmail($details);

  }

  else {

      $returnArray["status"] = "400";
      $returnArray["message"] = "Could not register with provided information";

  }

  // STEP 5. Close connection
  $access->disconnect();

  // STEP 6. Json data
  header('Content-Type: application/json');
  echo json_encode($returnArray);

?>
