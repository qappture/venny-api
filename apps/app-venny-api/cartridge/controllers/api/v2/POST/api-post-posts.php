<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = RAND());
  $post_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['topic'])){$post_topic = clean($_REQUEST['topic']);}else{$post_topic=NULL;}
  if(isset($_REQUEST['images'])){$post_images = clean($_REQUEST['images']);}else{$post_images=NULL;}
  if(isset($_REQUEST['detail'])){$post_detail = clean($_REQUEST['detail']);}else{$post_detail=NULL;}
  if(isset($_REQUEST['author_closed'])){$post_author_closed = clean($_REQUEST['author_closed']);}else{$post_author_closed=NULL;}
  if(isset($_REQUEST['deleted'])){$post_deleted = clean($_REQUEST['deleted']);}else{$post_deleted=NULL;}
  if(isset($_REQUEST['stage'])){$post_stage = clean($_REQUEST['stage']);}else{$post_stage=NULL;}
  if(isset($_REQUEST['host_ID'])){$host_ID = clean($_REQUEST['host_ID']);}else{$host_ID=NULL;}
  if(isset($_REQUEST['author_ID'])){$author_ID = clean($_REQUEST['author_ID']);}else{$author_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}


  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      post_ID,
      post_topic,
      post_images,
      post_detail,
      post_author_closed,
      post_deleted,
      post_stage,
      host_ID,
      author_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$post_ID',
      '$post_topic',
      '$post_images',
      '$post_detail',
      '$post_author_closed',
      '$post_deleted',
      '$post_stage',
      '$host_ID',
      '$author_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
