<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $notification_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['message'])){$notification_message = clean($_REQUEST['message']);}else{$notification_message=NULL;}
  if(isset($_REQUEST['type'])){$notification_type = clean($_REQUEST['type']);}else{$notification_type=NULL;}
  if(isset($_REQUEST['opened'])){$notification_opened = clean($_REQUEST['opened']);}else{$notification_opened=NULL;}
  if(isset($_REQUEST['viewed'])){$notification_viewed = clean($_REQUEST['viewed']);}else{$notification_viewed=NULL;}
  if(isset($_REQUEST['recipient'])){$notification_recipient = clean($_REQUEST['recipient']);}else{$notification_recipient=NULL;}
  if(isset($_REQUEST['sender'])){$notification_sender = clean($_REQUEST['sender']);}else{$notification_sender=NULL;}
  if(isset($_REQUEST['link'])){$notification_link = clean($_REQUEST['link']);}else{$notification_link=NULL;}
  if(isset($_REQUEST['object_ID'])){$object_ID = clean($_REQUEST['object_ID']);}else{$object_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      notification_ID,
      notification_message,
      notification_type,
      notification_opened,
      notification_viewed,
      notification_recipient,
      notification_sender,
      notification_link,
      object_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$notification_ID',
      '$notification_message',
      '$notification_type',
      '$notification_opened',
      '$notification_viewed',
      '$notification_recipient',
      '$notification_sender',
      '$notification_link',
      '$object_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
