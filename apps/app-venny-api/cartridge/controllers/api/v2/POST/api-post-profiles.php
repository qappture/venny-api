<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = RAND());
  $profile_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['images'])){$profile_images = clean($_REQUEST['images']);}else{$profile_images=NULL;}
  if(isset($_REQUEST['bio'])){$profile_bio = clean($_REQUEST['bio']);}else{$profile_bio=NULL;}
  if(isset($_REQUEST['name'])){$profile_name = clean($_REQUEST['name']);}else{$profile_name=NULL;}
  if(isset($_REQUEST['headline'])){$profile_headline = clean($_REQUEST['headline']);}else{$profile_headline=NULL;}
  if(isset($_REQUEST['access'])){$profile_access = clean($_REQUEST['access']);}else{$profile_access=NULL;}
  if(isset($_REQUEST['user_ID'])){$user_ID = clean($_REQUEST['user_ID']);}else{$user_ID=NULL;}
  if(isset($_REQUEST['person_ID'])){$person_ID = clean($_REQUEST['person_ID']);}else{$person_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      profile_ID,
      profile_images,
      profile_bio,
      profile_name,
      profile_headline,
      profile_access,
      user_ID,
      person_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$profile_ID',
      '$profile_images',
      '$profile_bio',
      '$profile_name',
      '$profile_headline',
      '$profile_access',
      '$user_ID',
      '$person_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
