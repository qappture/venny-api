<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = RAND());
  $user_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['alias'])){$user_alias = clean($_REQUEST['alias']);}else{$user_alias=NULL;}
  if(isset($_REQUEST['password'])){$user_password = clean($_REQUEST['password']);}else{$user_password=NULL;}
  if(isset($_REQUEST['lastlogin'])){$user_lastlogin = clean($_REQUEST['lastlogin']);}else{$user_lastlogin=NULL;}
  if(isset($_REQUEST['status'])){$user_status = clean($_REQUEST['status']);}else{$user_status=NULL;}
  if(isset($_REQUEST['validation'])){$user_validation = clean($_REQUEST['validation']);}else{$user_validation=NULL;}
  if(isset($_REQUEST['salt'])){$user_salt = clean($_REQUEST['salt']);}else{$user_salt=NULL;}
  if(isset($_REQUEST['welcome'])){$user_welcome = clean($_REQUEST['welcome']);}else{$user_welcome=NULL;}
  if(isset($_REQUEST['person_ID'])){$person_ID = clean($_REQUEST['person_ID']);}else{$person_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      user_ID,
      user_alias,
      user_password,
      user_lastlogin,
      user_status,
      user_validation,
      user_salt,
      user_welcome,
      person_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$user_ID',
      '$user_alias',
      '$user_password',
      '$user_lastlogin',
      '$user_status',
      '$user_validation',
      '$user_salt',
      '$user_welcome',
      '$person_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
