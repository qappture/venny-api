<?php

  // Do nothing without a token...
  if(isset($_REQUEST['token']) && good_api_token($_REQUEST['token'])) {

    $token = $_REQUEST['token'];

    // Do nothing without a token...
    if(isset($_REQUEST['domain']) && good_api_domain($_REQUEST['domain'])) {

      $domain = $_REQUEST['domain'];

      // Send request to respective API endpoint...
      switch ($domain) {

        //
        case 'apps': require 'controllers/api/POST/api-post-apps.php'; break;
        case 'attachments': require 'controllers/api/POST/api-post-attachments.php'; break;
        case 'comments': require 'controllers/api/POST/api-post-comments.php'; break;
        case 'crowds': require 'controllers/api/POST/api-post-crowds.php'; break;
        case 'events': require 'controllers/api/POST/api-post-events.php'; break;
        case 'excerpts': require 'controllers/api/POST/api-post-excerpts.php'; break;
        case 'followships': require 'controllers/api/POST/api-post-followships.php'; break;
        case 'ideas': require 'controllers/api/POST/api-post-ideas.php'; break;
        case 'images': require 'controllers/api/POST/api-post-images.php'; break;
        case 'likes': require 'controllers/api/POST/api-post-likes.php'; break;
        case 'messages': require 'controllers/api/POST/api-post-messages.php'; break;
        case 'notifications': require 'controllers/api/POST/api-post-notifications.php'; break;
        case 'partners': require 'controllers/api/POST/api-post-partners.php'; break;
        case 'persons': require 'controllers/api/POST/api-post-persons.php'; break;
        case 'posts': require 'controllers/api/POST/api-post-posts.php'; break;
        case 'processes': require 'controllers/api/POST/api-post-processes.php'; break;
        case 'profiles': require 'controllers/api/POST/api-post-profiles.php'; break;
        case 'recordings': require 'controllers/api/POST/api-post-recordings.php'; break;
        case 'searches': require 'controllers/api/POST/api-post-searches.php'; break;
        case 'stages': require 'controllers/api/POST/api-post-stages.php'; break;
        case 'tags': require 'controllers/api/POST/api-post-tags.php'; break;
        case 'threads': require 'controllers/api/POST/api-post-threads.php'; break;
        case 'tokens': require 'controllers/api/POST/api-post-tokens.php'; break;
        case 'topics': require 'controllers/api/POST/api-post-topics.php'; break;
        case 'trends': require 'controllers/api/POST/api-post-trends.php'; break;
        case 'uniques': require 'controllers/api/POST/api-post-uniques.php'; break;
        case 'users': require 'controllers/api/POST/api-post-users.php'; break;
        case 'views': require 'controllers/api/POST/api-post-views.php'; break;

        case 'register': require 'controllers/api/POST/api-post-register.php'; break;
        case 'subscribe': require 'controllers/api/POST/api-post-subscribe.php'; break;

        //
        default:

          header("Location: documentation.php?TRK=POST");

      }

    }

    else {

      $response = array(

        $t_api_key_message => $t_api_value_missingdomain,
        $t_api_key_status => $t_api_value_statusfailed

      );

      header('Content-Type: application/json');

      echo json_encode($response);

    }

  }

  else {

    $response = array(

      $t_api_key_message => $t_api_value_missingtoken,
      $t_api_key_status => $t_api_value_statusdenied

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

?>
