<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $message_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['body'])){$message_body = clean($_REQUEST['body']);}else{$message_body=NULL;}
  if(isset($_REQUEST['images'])){$message_images = clean($_REQUEST['images']);}else{$message_images=NULL;}
  if(isset($_REQUEST['opened'])){$message_opened = clean($_REQUEST['opened']);}else{$message_opened=NULL;}
  if(isset($_REQUEST['viewed'])){$message_viewed = clean($_REQUEST['viewed']);}else{$message_viewed=NULL;}
  if(isset($_REQUEST['deleted'])){$message_deleted = clean($_REQUEST['deleted']);}else{$message_deleted=NULL;}
  if(isset($_REQUEST['thread_ID'])){$thread_ID = clean($_REQUEST['thread_ID']);}else{$thread_ID=NULL;}
  if(isset($_REQUEST['profile_ID'])){$profile_ID = clean($_REQUEST['profile_ID']);}else{$profile_ID=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}



  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      message_ID,
      message_body,
      message_images,
      message_opened,
      message_viewed,
      message_deleted,
      thread_ID,
      profile_ID,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$message_ID',
      '$message_body',
      '$message_images',
      '$message_opened',
      '$message_viewed',
      '$message_deleted',
      '$thread_ID',
      '$profile_ID',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
