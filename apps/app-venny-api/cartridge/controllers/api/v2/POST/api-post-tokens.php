<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = create_api_process());
  $token_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['key'])){$token_key = clean($_REQUEST['key']);}else{$token_key=NULL;}
  if(isset($_REQUEST['secret'])){$token_secret = clean($_REQUEST['secret']);}else{$token_secret=NULL;}
  if(isset($_REQUEST['expires'])){$token_expires = clean($_REQUEST['expires']);}else{$token_expires=NULL;}
  if(isset($_REQUEST['limit'])){$token_limit = clean($_REQUEST['limit']);}else{$token_limit=NULL;}
  if(isset($_REQUEST['balance'])){$token_balance = clean($_REQUEST['balance']);}else{$token_balance=NULL;}
  if(isset($_REQUEST['status'])){$token_status = clean($_REQUEST['status']);}else{$token_status=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      token_ID,
      token_key,
      token_secret,
      token_expires,
      token_limit,
      token_balance,
      token_status,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$token_ID',
      '$token_key',
      '$token_secret',
      '$token_expires',
      '$token_limit',
      '$token_balance',
      '$token_status',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
