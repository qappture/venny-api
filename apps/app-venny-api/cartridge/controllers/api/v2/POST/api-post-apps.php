<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = RAND());
  $app_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['name'])){$app_name = clean($_REQUEST['name']);}else{$app_name=NULL;}
  if(isset($_REQUEST['website'])){$app_website = clean($_REQUEST['website']);}else{$app_website=NULL;}
  if(isset($_REQUEST['industry'])){$app_industry = clean($_REQUEST['industry']);}else{$app_industry=NULL;}
  if(isset($_REQUEST['email'])){$app_email = clean($_REQUEST['email']);}else{$app_email=NULL;}
  if(isset($_REQUEST['description'])){$app_description = clean($_REQUEST['description']);}else{$app_description=NULL;}
  if(isset($_REQUEST['type'])){$app_type = clean($_REQUEST['type']);}else{$app_type=NULL;}
  if(isset($_REQUEST['partner_ID'])){$partner_ID = clean($_REQUEST['partner_ID']);}else{$partner_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      app_ID,
      app_name,
      app_website,
      app_industry,
      app_email,
      app_description,
      app_type,
      partner_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$app_ID',
      '$app_name',
      '$app_website',
      '$app_industry',
      '$app_email',
      '$app_description',
      '$app_type',
      '$partner_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
