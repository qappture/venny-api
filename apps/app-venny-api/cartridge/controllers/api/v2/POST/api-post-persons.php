<?php

  //
  header('Content-Type: application/json');

  //
  $ID = new_ID($domain,$process = create_api_process();
  $person_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['first_name'])){$person_first_name = clean($_REQUEST['first_name']);}else{$person_first_name=NULL;}
  if(isset($_REQUEST['last_name'])){$person_last_name = clean($_REQUEST['last_name']);}else{$person_last_name=NULL;}
  if(isset($_REQUEST['email'])){$person_email = clean($_REQUEST['email']);}else{$person_email=NULL;}
  if(isset($_REQUEST['phone'])){$person_phone = clean($_REQUEST['phone']);}else{$person_phone=NULL;}
  if(isset($_REQUEST['entitlements'])){$person_entitlements = clean($_REQUEST['entitlements']);}else{$person_entitlements=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      person_ID,
      person_first_name,
      person_last_name,
      person_email,
      person_phone,
      person_entitlements,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$person_ID',
      '$person_first_name',
      '$person_last_name',
      '$person_email',
      '$person_phone',
      '$person_entitlements',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
