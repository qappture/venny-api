<?php

  //
  header('Content-Type: application/json');

  //
  $type = 'POST_' . $domain;
  $process = create_api_process(NULL,$type,$token);
  $ID = new_ID($domain,$process);
  $crowd_ID = $ID;
  $event = create_api_event($ID,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  //
  if(isset($_REQUEST['title'])){$crowd_title = clean($_REQUEST['title']);}else{$crowd_title=NULL;}
  if(isset($_REQUEST['headline'])){$crowd_headline = clean($_REQUEST['headline']);}else{$crowd_headline=NULL;}
  if(isset($_REQUEST['access'])){$crowd_access = clean($_REQUEST['access']);}else{$crowd_access=NULL;}
  if(isset($_REQUEST['administrators'])){$crowd_administrators = clean($_REQUEST['administrators']);}else{$crowd_administrators=NULL;}
  if(isset($_REQUEST['contributors'])){$crowd_contributors = clean($_REQUEST['contributors']);}else{$crowd_contributors=NULL;}
  if(isset($_REQUEST['images'])){$crowd_images = clean($_REQUEST['images']);}else{$crowd_images=NULL;}
  if(isset($_REQUEST['author'])){$crowd_author = clean($_REQUEST['author']);}else{$crowd_author=NULL;}
  if(isset($_REQUEST['app_ID'])){$app_ID = clean($_REQUEST['app_ID']);}else{$app_ID=NULL;}
  if(isset($_REQUEST['event_ID'])){$event_ID = clean($_REQUEST['event_ID']);}else{$event_ID=NULL;}
  if(isset($_REQUEST['process_ID'])){$process_ID = clean($_REQUEST['process_ID']);}else{$process_ID=NULL;}

  // BEGIN CUSTOMIZATIONS

  // END CUSTOMIZATIONS

  $query = query(

    "INSERT INTO {$domain} (

      crowd_ID,
      crowd_title,
      crowd_headline,
      crowd_access,
      crowd_administrators,
      crowd_contributors,
      crowd_images,
      crowd_author,
      app_ID,
      event_ID,
      process_ID

    ) VALUES (

      '$crowd_ID',
      '$crowd_title',
      '$crowd_headline',
      '$crowd_access',
      '$crowd_administrators',
      '$crowd_contributors',
      '$crowd_images',
      '$crowd_author',
      '$app_ID',
      '$event_ID',
      '$process_ID'

    )"

  );

  // TESTING
  //echo $query;
  //exit;

  //$query = mysqli_query($this->con, "INSERT INTO notes (id,body) VALUES (NULL,'$body')");
  $successful = mysqli_insert_id($db);

  if($successful) {

    //Insert notification
    /*
    if($user_to != 'none') {

      $notification = new Notification($this->con, $added_by);
      $notification->insertNotificationNote($returned_id, $user_to, "like");

    }
    */

    //Update note count for user
    /*
    $num_notes = $this->user_obj->getNumNotes();
    $num_notes++;
    $update_query = mysqli_query($this->con, "UPDATE users SET num_notes='$num_notes' WHERE username='$added_by'");
    */

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . " was created successfully.",
      $t_api_key_status => $t_api_value_statussuccess,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

  else {

    //
    $response = array(

      $t_api_key_message => "The " . $domain . " entry " . $ID /* $successful is formerly $id */ . "was created",
      $t_api_key_status => $t_api_value_statusfailed,
      $t_api_key_event => $event,
      $t_api_key_process => $process

    );

    header('Content-Type: application/json');

    echo $response;

  }

?>
