<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['topic'])){$topic=clean($_GET['topic']);$conditions.="AND ".substr($domain,0,-1)."_topic LIKE '%".$topic."%' ";}else{$conditions.="";}
    if(isset($_GET['images'])){$images=clean($_GET['images']);$conditions.="AND ".substr($domain,0,-1)."_images LIKE '%".$images."%' ";}else{$conditions.="";}
    if(isset($_GET['detail'])){$detail=clean($_GET['detail']);$conditions.="AND ".substr($domain,0,-1)."_detail LIKE '%".$detail."%' ";}else{$conditions.="";}
    if(isset($_GET['author_closed'])){$author_closed=clean($_GET['author_closed']);$conditions.="AND ".substr($domain,0,-1)."_author_closed LIKE '%".$author_closed."%' ";}else{$conditions.="";}
    if(isset($_GET['deleted'])){$deleted=clean($_GET['deleted']);$conditions.="AND ".substr($domain,0,-1)."_deleted LIKE '%".$deleted."%' ";}else{$conditions.="";}
    if(isset($_GET['stage'])){$stage=clean($_GET['stage']);$conditions.="AND ".substr($domain,0,-1)."_stage LIKE '%".$stage."%' ";}else{$conditions.="";}
    if(isset($_GET['host_ID'])){$host_ID=clean($_GET['host_ID']);$conditions.="AND ".substr($domain,0,-1)."_host_ID LIKE '%".$host_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['author_ID'])){$author_ID=clean($_GET['author_ID']);$conditions.="AND ".substr($domain,0,-1)."_author_ID LIKE '%".$author_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

    }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['post_ID'],
      'topic' => $row['post_topic'],
      'images' => $row['post_images'],
      'detail' => $row['post_detail'],
      'author_closed' => $row['post_author_closed'],
      'deleted' => $row['post_deleted'],
      'stage' => $row['post_stage'],
      'host_ID' => $row['host_ID'],
      'author_ID' => $row['author_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
