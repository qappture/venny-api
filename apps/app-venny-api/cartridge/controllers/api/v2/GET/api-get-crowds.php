<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['title'])){$title=clean($_GET['title']);$conditions.="AND ".substr($domain,0,-1)."_title LIKE '%".$title."%' ";}else{$conditions.="";}
    if(isset($_GET['headline'])){$headline=clean($_GET['headline']);$conditions.="AND ".substr($domain,0,-1)."_headline LIKE '%".$headline."%' ";}else{$conditions.="";}
    if(isset($_GET['access'])){$access=clean($_GET['access']);$conditions.="AND ".substr($domain,0,-1)."_access = '".$access."' ";}else{$conditions.="";}
    if(isset($_GET['administrators'])){$administrators=clean($_GET['administrators']);$conditions.="AND ".substr($domain,0,-1)."_administrators LIKE '%".$administrators."%' ";}else{$conditions.="";}
    if(isset($_GET['contributors'])){$contributors=clean($_GET['contributors']);$conditions.="AND ".substr($domain,0,-1)."_contributors LIKE '%".$contributors."%' ";}else{$conditions.="";}
    if(isset($_GET['images'])){$images=clean($_GET['images']);$conditions.="AND ".substr($domain,0,-1)."_images LIKE '%".$images."%' ";}else{$conditions.="";}
    if(isset($_GET['author'])){$author=clean($_GET['author']);$conditions.="AND ".substr($domain,0,-1)."_author LIKE '%".$author."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

    }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['crowd_ID'],
      'title' => $row['crowd_title'],
      'headline' => $row['crowd_headline'],
      'access' => $row['crowd_access'],
      'administrators' => $row['crowd_administrators'],
      'contributors' => $row['crowd_contributors'],
      'images' => $row['crowd_images'],
      'author' => $row['crowd_author'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
