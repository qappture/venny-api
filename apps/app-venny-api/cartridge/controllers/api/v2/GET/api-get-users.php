<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['alias'])){$alias=clean($_GET['alias']);$conditions.="AND ".substr($domain,0,-1)."_alias = '".$alias."' ";}else{$conditions.="";}
    if(isset($_GET['password'])){$password=clean($_GET['password']);$conditions.="AND ".substr($domain,0,-1)."_password = '".$password."' ";}else{$conditions.="";}
    if(isset($_GET['lastlogin'])){$lastlogin=clean($_GET['lastlogin']);$conditions.="AND ".substr($domain,0,-1)."_lastlogin = '".$lastlogin."' ";}else{$conditions.="";}
    if(isset($_GET['status'])){$status=clean($_GET['status']);$conditions.="AND ".substr($domain,0,-1)."_status = '".$status."' ";}else{$conditions.="";}
    if(isset($_GET['validation'])){$validation=clean($_GET['validation']);$conditions.="AND ".substr($domain,0,-1)."_validation = '".$validation."' ";}else{$conditions.="";}
    if(isset($_GET['salt'])){$salt=clean($_GET['salt']);$conditions.="AND ".substr($domain,0,-1)."_salt = '".$salt."' ";}else{$conditions.="";}
    if(isset($_GET['welcome'])){$welcome=clean($_GET['welcome']);$conditions.="AND ".substr($domain,0,-1)."_welcome LIKE '%".$welcome."%' ";}else{$conditions.="";}
    if(isset($_GET['person_ID'])){$person_ID=clean($_GET['person_ID']);$conditions.="AND ".substr($domain,0,-1)."_person_ID LIKE '%".$person_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

  }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['user_ID'],
      'alias' => $row['user_alias'],
      'password' => $row['user_password'],
      'lastlogin' => $row['user_lastlogin'],
      'status' => $row['user_status'],
      'validation' => $row['user_validation'],
      'salt' => $row['user_salt'],
      'welcome' => $row['user_welcome'],
      'person_ID' => $row['person_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID'],


    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
