<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['type'])){$type=clean($_GET['type']);$conditions.="AND ".substr($domain,0,-1)."_type = '".$type."' ";}else{$conditions.="";}
    if(isset($_GET['status'])){$status=clean($_GET['status']);$conditions.="AND ".substr($domain,0,-1)."_status = '".$status."' ";}else{$conditions.="";}
    if(isset($_GET['primary'])){$primary=clean($_GET['primary']);$conditions.="AND ".substr($domain,0,-1)."_primary = '".$primary."' ";}else{$conditions.="";}
    if(isset($_GET['object'])){$object=clean($_GET['object']);$conditions.="AND ".substr($domain,0,-1)."_object = '".$object."' ";}else{$conditions.="";}
    if(isset($_GET['caption'])){$caption=clean($_GET['caption']);$conditions.="AND ".substr($domain,0,-1)."_caption LIKE '%".$caption."%' ";}else{$conditions.="";}
    if(isset($_GET['filename'])){$filename=clean($_GET['filename']);$conditions.="AND ".substr($domain,0,-1)."_filename = '".$filename."' ";}else{$conditions.="";}
    if(isset($_GET['metadata'])){$metadata=clean($_GET['metadata']);$conditions.="AND ".substr($domain,0,-1)."_metadata LIKE '%".$metadata."%' ";}else{$conditions.="";}
    if(isset($_GET['profile_ID'])){$profile_ID=clean($_GET['profile_ID']);$conditions.="AND ".substr($domain,0,-1)."_profile_ID = '".$profile_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

  }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['image_ID'],
      'type' => $row['image_type'],
      'status' => $row['image_status'],
      'primary' => $row['image_primary'],
      'object' => $row['image_object'],
      'caption' => $row['image_caption'],
      'filename' => $row['image_filename'],
      'metadata' => $row['image_metadata'],
      'profile_ID' => $row['profile_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
