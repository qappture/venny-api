<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['name'])){$name=clean($_GET['name']);$conditions.="AND ".substr($domain,0,-1)."_name LIKE '%".$name."%' ";}else{$conditions.="";}
    if(isset($_GET['website'])){$website=clean($_GET['website']);$conditions.="AND ".substr($domain,0,-1)."_website LIKE '%".$website."%' ";}else{$conditions.="";}
    if(isset($_GET['industry'])){$industry=clean($_GET['industry']);$conditions.="AND ".substr($domain,0,-1)."_industry = '".$industry."' ";}else{$conditions.="";}
    if(isset($_GET['email'])){$email=clean($_GET['email']);$conditions.="AND ".substr($domain,0,-1)."_email = '".$email."' ";}else{$conditions.="";}
    if(isset($_GET['description'])){$description=clean($_GET['description']);$conditions.="AND ".substr($domain,0,-1)."_description LIKE '%".$description."%' ";}else{$conditions.="";}
    if(isset($_GET['type'])){$type=clean($_GET['type']);$conditions.="AND ".substr($domain,0,-1)."_type = '".$type."%' ";}else{$conditions.="";}
    if(isset($_GET['partner_ID'])){$partner_ID=clean($_GET['partner_ID']);$conditions.="AND ".substr($domain,0,-1)."_partner_ID = '".$partner_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID LIKE '%".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID LIKE '%".$process_ID."%' ";}else{$conditions.="";}

  }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['app_ID'],
      'name' => $row['app_name'],
      'website' => $row['app_website'],
      'industry' => $row['app_industry'],
      'email' => $row['app_email'],
      'description' => $row['app_description'],
      'type' => $row['app_type'],
      'partner_ID' => $row['partner_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
