<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['first_name'])){$first_name=clean($_GET['first_name']);$conditions.="AND ".substr($domain,0,-1)."_first_name LIKE '%".$first_name."%' ";}else{$conditions.="";}
    if(isset($_GET['last_name'])){$last_name=clean($_GET['last_name']);$conditions.="AND ".substr($domain,0,-1)."_last_name LIKE '%".$last_name."%' ";}else{$conditions.="";}
    if(isset($_GET['email'])){$email=clean($_GET['email']);$conditions.="AND ".substr($domain,0,-1)."_email = '".$email."' ";}else{$conditions.="";}
    if(isset($_GET['phone'])){$phone=clean($_GET['phone']);$conditions.="AND ".substr($domain,0,-1)."_phone = '".$phone."' ";}else{$conditions.="";}
    if(isset($_GET['entitlements'])){$entitlements=clean($_GET['entitlements']);$conditions.="AND ".substr($domain,0,-1)."_entitlements LIKE '%".$entitlements."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

  }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['person_ID'],
      'first_name' => $row['person_first_name'],
      'last_name' => $row['person_last_name'],
      'email' => $row['person_email'],
      'phone' => $row['person_phone'],
      'entitlements' => $row['person_entitlements'],
      'app_ID' => $row['app_ID'],		
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
