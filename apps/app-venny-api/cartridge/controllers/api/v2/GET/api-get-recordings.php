<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['type'])){$type=clean($_GET['type']);$conditions.="AND ".substr($domain,0,-1)."_type = '".$type."%' ";}else{$conditions.="";}
    if(isset($_GET['source'])){$source=clean($_GET['source']);$conditions.="AND ".substr($domain,0,-1)."_source = '".$source."%' ";}else{$conditions.="";}
    if(isset($_GET['length'])){$length=clean($_GET['length']);$conditions.="AND ".substr($domain,0,-1)."_length = '".$length."%' ";}else{$conditions.="";}
    if(isset($_GET['cues'])){$cues=clean($_GET['cues']);$conditions.="AND ".substr($domain,0,-1)."_cues LIKE '%".$cues."%' ";}else{$conditions.="";}
    if(isset($_GET['x'])){$x=clean($_GET['x']);$conditions.="AND ".substr($domain,0,-1)."_x = '".$x."%' ";}else{$conditions.="";}
    if(isset($_GET['y'])){$y=clean($_GET['y']);$conditions.="AND ".substr($domain,0,-1)."_y = '".$y."%' ";}else{$conditions.="";}
    if(isset($_GET['z'])){$z=clean($_GET['z']);$conditions.="AND ".substr($domain,0,-1)."_z = '".$z."%' ";}else{$conditions.="";}
    if(isset($_GET['width'])){$width=clean($_GET['width']);$conditions.="AND ".substr($domain,0,-1)."_width = '".$width."%' ";}else{$conditions.="";}
    if(isset($_GET['height'])){$height=clean($_GET['height']);$conditions.="AND ".substr($domain,0,-1)."_height = '".$height."%' ";}else{$conditions.="";}
    if(isset($_GET['stage_ID'])){$stage_ID=clean($_GET['stage_ID']);$conditions.="AND ".substr($domain,0,-1)."_stage_ID = '".$stage_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['attachment_ID'])){$attachment_ID=clean($_GET['attachment_ID']);$conditions.="AND ".substr($domain,0,-1)."_attachment_ID = '".$attachment_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['post_ID'])){$post_ID=clean($_GET['post_ID']);$conditions.="AND ".substr($domain,0,-1)."_post_ID = '".$post_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

    }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['recording_ID'],
      'type' => $row['recording_type'],
      'source' => $row['recording_source'],
      'length' => $row['recording_length'],
      'cues' => $row['recording_cues'],
      'x' => $row['recording_x'],
      'y' => $row['recording_y'],
      'z' => $row['recording_z'],
      'width' => $row['recording_width'],
      'height' => $row['recording_height'],
      'stage_ID' => $row['stage_ID'],
      'attachment_ID' => $row['attachment_ID'],
      'post_ID' => $row['post_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
