<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['key'])){$key=clean($_GET['key']);$conditions.="AND ".substr($domain,0,-1)."_key = '".$key."' ";}else{$conditions.="";}
    if(isset($_GET['secret'])){$secret=clean($_GET['secret']);$conditions.="AND ".substr($domain,0,-1)."_secret = '".$secret."' ";}else{$conditions.="";}
    if(isset($_GET['expires'])){$expires=clean($_GET['expires']);$conditions.="AND ".substr($domain,0,-1)."_expires = '".$expires."' ";}else{$conditions.="";}
    if(isset($_GET['limit'])){$limit=clean($_GET['limit']);$conditions.="AND ".substr($domain,0,-1)."_limit = '".$limit."' ";}else{$conditions.="";}
    if(isset($_GET['balance'])){$balance=clean($_GET['balance']);$conditions.="AND ".substr($domain,0,-1)."_balance = '".$balance."' ";}else{$conditions.="";}
    if(isset($_GET['status'])){$status=clean($_GET['status']);$conditions.="AND ".substr($domain,0,-1)."_status = '".$status."' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

  }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['token_ID'],
      'key' => $row['token_key'],
      'secret' => $row['token_secret'],
      'expires' => $row['token_expires'],
      'limit' => $row['token_limit'],
      'balance' => $row['token_balance'],
      'status' => $row['token_status'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
