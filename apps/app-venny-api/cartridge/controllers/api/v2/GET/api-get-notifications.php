<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['message'])){$message=clean($_GET['message']);$conditions.="AND ".substr($domain,0,-1)."_message LIKE '%".$message."%' ";}else{$conditions.="";}
    if(isset($_GET['type'])){$type=clean($_GET['type']);$conditions.="AND ".substr($domain,0,-1)."_type = '".$type."%' ";}else{$conditions.="";}
    if(isset($_GET['opened'])){$opened=clean($_GET['opened']);$conditions.="AND ".substr($domain,0,-1)."_opened = '".$opened."%' ";}else{$conditions.="";}
    if(isset($_GET['viewed'])){$viewed=clean($_GET['viewed']);$conditions.="AND ".substr($domain,0,-1)."_viewed = '".$viewed."%' ";}else{$conditions.="";}
    if(isset($_GET['recipient'])){$recipient=clean($_GET['recipient']);$conditions.="AND ".substr($domain,0,-1)."_recipient = '".$recipient."%' ";}else{$conditions.="";}
    if(isset($_GET['sender'])){$sender=clean($_GET['sender']);$conditions.="AND ".substr($domain,0,-1)."_sender = '".$sender."%' ";}else{$conditions.="";}
    if(isset($_GET['link'])){$link=clean($_GET['link']);$conditions.="AND ".substr($domain,0,-1)."_link LIKE '%".$link."%' ";}else{$conditions.="";}
    if(isset($_GET['object_ID'])){$object_ID=clean($_GET['object_ID']);$conditions.="AND ".substr($domain,0,-1)."_object_ID = '".$object_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND ".substr($domain,0,-1)."_app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND ".substr($domain,0,-1)."_event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND ".substr($domain,0,-1)."_process_ID = '".$process_ID."%' ";}else{$conditions.="";}

    }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['notification_ID'],
      'message' => $row['notification_message'],
      'type' => $row['notification_type'],
      'opened' => $row['notification_opened'],
      'viewed' => $row['notification_viewed'],
      'recipient' => $row['notification_recipient'],
      'sender' => $row['notification_sender'],
      'link' => $row['notification_link'],
      'object_ID' => $row['object_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID']

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
