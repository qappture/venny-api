<?php

  // Do nothing without a token...
  if(isset($_GET['token']) && good_api_token($_GET['token'])) {

    //
    $token = $_GET['token'];

    //
    if(isset($_GET['domain']) && good_api_domain($_GET['domain'])) {

      //
      $domain = $_GET['domain'];

      // Send request to respective API endpoint...
      switch ($domain) {

        //
        case 'apps': require 'controllers/api/GET/api-get-apps.php'; break;
        case 'attachments': require 'controllers/api/GET/api-get-attachments.php'; break;
        case 'comments': require 'controllers/api/GET/api-get-comments.php'; break;
        case 'crowds': require 'controllers/api/GET/api-get-crowds.php'; break;
        case 'events': require 'controllers/api/GET/api-get-events.php'; break;
        case 'excerpts': require 'controllers/api/GET/api-get-excerpts.php'; break;
        case 'followships': require 'controllers/api/GET/api-get-followships.php'; break;
        case 'ideas': require 'controllers/api/GET/api-get-ideas.php'; break;
        case 'images': require 'controllers/api/GET/api-get-images.php'; break;
        case 'likes': require 'controllers/api/GET/api-get-likes.php'; break;
        case 'messages': require 'controllers/api/GET/api-get-messages.php'; break;
        case 'notifications': require 'controllers/api/GET/api-get-notifications.php'; break;
        case 'partners': require 'controllers/api/GET/api-get-partners.php'; break;
        case 'persons': require 'controllers/api/GET/api-get-persons.php'; break;
        case 'posts': require 'controllers/api/GET/api-get-posts.php'; break;
        case 'processes': require 'controllers/api/GET/api-get-processes.php'; break;
        case 'profiles': require 'controllers/api/GET/api-get-profiles.php'; break;
        case 'recordings': require 'controllers/api/GET/api-get-recordings.php'; break;
        case 'searches': require 'controllers/api/GET/api-get-searches.php'; break;
        case 'stages': require 'controllers/api/GET/api-get-stages.php'; break;
        case 'tags': require 'controllers/api/GET/api-get-tags.php'; break;
        case 'threads': require 'controllers/api/GET/api-get-threads.php'; break;
        case 'tokens': require 'controllers/api/GET/api-get-tokens.php'; break;
        case 'topics': require 'controllers/api/GET/api-get-topics.php'; break;
        case 'trends': require 'controllers/api/GET/api-get-trends.php'; break;
        case 'uniques': require 'controllers/api/GET/api-get-uniques.php'; break;
        case 'users': require 'controllers/api/GET/api-get-users.php'; break;
        case 'views': require 'controllers/api/GET/api-get-views.php'; break;

        //
        default:

          header("Location: documentation.php?TRK=GET");

      }

    }

    else {

      $response = array(

        $t_api_key_message => $t_api_value_missingdomain,
        $t_api_key_status => $t_api_value_statusfailed,
        $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
        $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

      );

      header('Content-Type: application/json');

      echo json_encode($response);

    }

  }

  else {

    $response = array(

      $t_api_key_message => $t_api_value_missingtoken,
      $t_api_key_status => $t_api_value_statusdenied,
      $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
      $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

?>
