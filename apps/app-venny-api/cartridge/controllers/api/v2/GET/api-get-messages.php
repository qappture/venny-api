<?php

  //
  header('Content-Type: application/json');

  //
  if(isset($_GET['id'])) {

    $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $_GET['id'] . "' ";
    $limit = " LIMIT 1";

  }

  else {

    $conditions = "";
    $limit = "";

    if(isset($_GET['body'])){$body=clean($_GET['body']);$conditions.="AND ".substr($domain,0,-1)."_body LIKE '%".$body."%' ";}else{$conditions.="";}
    if(isset($_GET['images'])){$images=clean($_GET['images']);$conditions.="AND ".substr($domain,0,-1)."_images LIKE '%".$images."%' ";}else{$conditions.="";}
    if(isset($_GET['opened'])){$opened=clean($_GET['opened']);$conditions.="AND ".substr($domain,0,-1)."_opened = '".$opened."%' ";}else{$conditions.="";}
    if(isset($_GET['viewed'])){$viewed=clean($_GET['viewed']);$conditions.="AND ".substr($domain,0,-1)."_viewed = '".$viewed."%' ";}else{$conditions.="";}
    if(isset($_GET['deleted'])){$deleted=clean($_GET['deleted']);$conditions.="AND ".substr($domain,0,-1)."_deleted = '".$deleted."%' ";}else{$conditions.="";}
    if(isset($_GET['profile_ID'])){$profile_ID=clean($_GET['profile_ID']);$conditions.="AND "."profile_ID = '".$profile_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['thread_ID'])){$thread_ID=clean($_GET['thread_ID']);$conditions.="AND "."thread_ID = '".$thread_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['app_ID'])){$app_ID=clean($_GET['app_ID']);$conditions.="AND "."app_ID = '".$app_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['event_ID'])){$event_ID=clean($_GET['event_ID']);$conditions.="AND "."event_ID = '".$event_ID."%' ";}else{$conditions.="";}
    if(isset($_GET['process_ID'])){$process_ID=clean($_GET['process_ID']);$conditions.="AND "."process_ID = '".$process_ID."%' ";}else{$conditions.="";}

    }

  // SQL...
  $sql  = "SELECT * FROM {$domain} ";
  $sql .= "WHERE active = 1 ";
  $sql .= $conditions;
  $sql .= "ORDER BY time_finished DESC";
  $sql .= $limit;

  //TESTING
  //echo $sql;
  //exit;

  $query = query($sql); // create query

  //TESTING
  //echo $query;
  //exit;

  $results = array(); // instantiate an array to store query results
  $total = mysqli_num_rows($query); // derive count of records after query run
  $html = "[]"; // create HTML attribute for later use
  //$event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

  // for every record returned create an array and store values against these keys... users of the API will see these keys
  while ($row = mysqli_fetch_array($query)) {

    $results[] = array (

      'id' => $row['message_ID'],
      'body' => $row['message_body'],
      'images' => $row['message_images'],
      'opened' => $row['message_opened'],
      'viewed' => $row['message_viewed'],
      'deleted' => $row['message_deleted'],
      'thread_ID' => $row['thread_ID'],
      'profile_ID' => $row['profile_ID'],
      'app_ID' => $row['app_ID'],
      'event_ID' => $row['event_ID'],
      'process_ID' => $row['process_ID'],

    );

  }

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token),
    $t_api_key_process => create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
