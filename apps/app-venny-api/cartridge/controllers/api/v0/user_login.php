<?php

// Importing DBConfig.php file.
include 'dbconfig.php';

header('Content-Type: application/json');

// Creating connection.
 $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

 // Getting the received JSON into $json variable.
 $json = $_REQUEST;

 // decoding the received JSON and store into $obj variable.
 $obj = $json;

// Populate User email from JSON $obj array and store into $email.
$email = $obj['email'];

// Populate Password from JSON $obj array and store into $password.
$password = $obj['password'];

//Applying User Login query with email and password match.
$Sql_Query = "select * from user_details where email = '$email' and password = '$password' ";

// Executing SQL Query.
$check = mysqli_fetch_array(mysqli_query($con,$Sql_Query));


if(isset($check)) {

   $SuccessLoginMsg = 'Data Matched';

   // Converting the message into JSON format.
  $SuccessLoginJson = $SuccessLoginMsg;

  // Echo the message.
   echo json_encode(array(
     "status" => 200,
     "message" => $SuccessLoginJson,
   ));

 }

 else{

 // If the record inserted successfully then show the message.
$InvalidMSG = 'Invalid Username or Password Please Try Again' ;

// Converting the message into JSON format.
$InvalidMSGJSon = $InvalidMSG;

// Echo the message.
 echo json_encode(array(
   "status" => 401,
   "message"=>$InvalidMSGJSon
 ));

 }

 mysqli_close($con);
?>
