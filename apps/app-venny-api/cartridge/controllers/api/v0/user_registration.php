<?php

  // Importing DBConfig.php file.
  include 'dbconfig.php';
  //echo $HostName.$HostUser.$HostPass.$DatabaseName;exit;

  //
  header('Content-Type: application/json');

  // Creating connection.
  $con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);
  //echo json_encode($con); exit;

  // Getting the received JSON into $json variable.
  $json = $_REQUEST;
  //echo json_encode($json); exit;

  // decoding the received JSON and store into $obj variable.
  //$obj = json_decode($json,true);
  $obj = $json;
  //echo json_encode($obj); exit;

   // Populate User name from JSON $obj array and store into $name.
  $name = $obj['name'];
  //echo $obj['name'];

  // Populate User email from JSON $obj array and store into $email.
  $email = $obj['email'];
  //echo $obj['email'];

  // Populate Password from JSON $obj array and store into $password.
  $password = $obj['password'];
  //echo $obj['password'];
  //exit;

  //Checking Email is already exist or not using SQL query.
  $CheckSQL = "SELECT * FROM user_details WHERE email='$email'";

  // Executing SQL Query.
  $check = mysqli_fetch_array(mysqli_query($con,$CheckSQL));

  //
  if(isset($check)) {

    $EmailExistMSG = array(

      'status' => 'Email Already Exist, Please Try Again!'

    );

    // Converting the message into JSON format.
    $EmailExistJson = json_encode($EmailExistMSG);

    // Echo the message.
    header('Content-Type: application/json');
    echo $EmailExistJson;

  }

  else {

    // Creating SQL query and insert the record into MySQL database table.
    $Sql_Query = "INSERT INTO `user_details` (name,email,password) values ('{$name}','{$email}','{$password}')";

    //
    if(mysqli_query($con,$Sql_Query)) {

      // If the record inserted successfully then show the message.
      $MSG =  array(

        'status' => 'User Registered Successfully'

      );

      // Converting the message into JSON format.
      $json = json_encode($MSG);

        // Echo the message.
      header('Content-Type: application/json');
      echo $json;

    }

    else {

      header('Content-Type: application/json');
      echo array(

        'status' => 'Try Again',
        'status' => 'Try Again'

      );

    }

  }

  mysqli_close($con);

?>
