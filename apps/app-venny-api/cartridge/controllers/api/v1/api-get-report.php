<?php

  //
  header('Content-Type: application/json');

    // OTHER CONDITIONS
    //$raw_results = array();
    //$raw_results = getReportofProjects();

    //print_r($raw_results);exit;

    ///////////////////////////////

    //print_r(getResultsbySkills($q,$l)); exit;
    //print_r(getResultsbyPositions($q,$l)); exit;
    //print_r(getResultsbyProjects($q,$l)); exit;

    $total = NULL; // derive count of records after query run
    $html = "[]"; // create HTML attribute for later use
    $event = create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token); // creates event for each call

    //print_r($raw_results);exit;

    // for every record returned create an array and store values against these keys... users of the API will see these keys
    $results[] = array (

      'report' => array (

        'projects' => $projects = getReportofProjects(),
        'skills' => $skills = getReportofSkills(),
        'positions' => $positions = getReportofPositions(),
        'profiles' => $positions = getReportofProfiles()

      )

    );

  // Return JSON array...
  $response = array(

    $t_api_key_total => $total,
    $t_api_key_html => $html,
    $t_api_key_results => $results,
    $t_api_key_status => $t_api_value_statussuccess,
    $t_api_key_event => $event

  );

  header('Content-Type: application/json');

  echo json_encode($response);

?>
