<?php

  //
  function getMemberIDbyAlias($member_alias, $amplify = false) {

    ////////////
    $condition_member = "AND member_alias = '{$member_alias}' ";

    $sql_member  = "SELECT member_ID FROM members ";
    $sql_member .= "WHERE active = 1 ";
    $sql_member .= $condition_member;
    $sql_member .= "LIMIT 1";

    $query_member = query($sql_member);

    $member = fetch_array($query_member);
    $member_ID = $member['member_ID'];
    ////////////

    return $member_ID;

  }

  //
  function getAliasbyMemberID($member_ID, $amplify = false) {

    ////////////
    $condition_alias = "AND member_ID = '{$member_ID}' ";

    $sql_alias  = "SELECT member_alias FROM members ";
    $sql_alias .= "WHERE active = 1 ";
    $sql_alias .= $condition_alias;
    $sql_alias .= "LIMIT 1";

    $query_alias = query($sql_alias);

    $alias = fetch_array($query_alias);
    $member_alias = $alias['member_alias'];
    ////////////

    return $member_alias;

  }

  //
  function getMemberbyMemberID($member_ID, $amplify = false) {

    ////////////
    $condition_member = "AND member_ID = '{$member_ID}' ";

    $sql_member  = "SELECT member_ID,member_alias,member_lastlogin,member_status,person_ID FROM members ";
    $sql_member .= "WHERE active = 1 ";
    $sql_member .= $condition_member;
    $sql_member .= "LIMIT 1";

    $query_member = query($sql_member);

    $member = fetch_array($query_member);
    $member_ID = $member['member_ID'];
    $member_alias = $member['member_alias'];
    $member_lastlogin = $member['member_lastlogin'];
    $member_status = $member['member_status'];
    $person_ID = $member['person_ID'];
    ////////////

    $member = array(

      'member_ID' => $member['member_ID'],
      'alias' => $member['member_alias'],
      'last_login' => $member['member_lastlogin'],
      'status' => $member['member_status']

    );

    return $member;

  }

  //
  function getSendEmailbyMemberID($member_ID, $amplify = false) {

    //////////// GET MEMBER INFORMATION FROM PERSON ID //////////////////
    $condition_person = "AND person_type_member = '{$member_ID}' ";

    $sql_person  = "SELECT person_ID,person_email,person_first_name,person_last_name,person_phone,person_type_guest,person_type_member,person_type_profile,person_type_user,person_type_partner FROM persons ";
    $sql_person .= "WHERE active = 1 ";
    $sql_person .= $condition_person;
    $sql_person .= "LIMIT 1 ";

    $query_person = query($sql_person);

    $persons = fetch_array($query_person);
    //////////// GET PERSON INFORMATION FROM PERSON ID //////////////////

    $person = array(

      'person_ID' => $persons['person_ID'],
      'email' => $persons['person_email'],
      'first_name' => $persons['person_first_name'],
      'last_name' => $persons['person_last_name'],
      'phone' => $persons['person_phone'],

      'guest' => $persons['person_type_guest'],
      'member' => $persons['person_type_member'],
      'profile' => $persons['person_type_profile'],
      'user' => $persons['person_type_user'],
      'partner' => $persons['person_type_partner']

    );

    return $person;

  }

  //
  function getEntitlementsbyID($ID, $type, $amplify = false) {

    //
    switch ($type) {

      case "user":
        $condition_entitlements = "AND person_type_user = '{$ID}' ";
        break;

      case "person":
        $condition_entitlements = "AND person_ID = '{$ID}' ";
        break;

      case "member":
        $condition_entitlements = "AND person_type_member = '{$ID}' ";
        break;

      default:
        # code...
        break;

    }

    //////////// GET MEMBER INFORMATION FROM PERSON ID //////////////////

    $sql_entitlements  = "SELECT person_ID,person_type_guest,person_type_member,person_type_profile,person_type_user,person_type_partner FROM persons ";
    $sql_entitlements .= "WHERE active = 1 ";
    $sql_entitlements .= $condition_entitlements;
    $sql_entitlements .= "LIMIT 1 ";

    $query_entitlements = query($sql_entitlements);

    $entitlements = fetch_array($query_entitlements);
    //////////// GET PERSON INFORMATION FROM PERSON ID //////////////////

    $entitlements = array(

      'person' => $entitlements['person_ID'],
      'guest' => $entitlements['person_type_guest'],
      'member' => $entitlements['person_type_member'],
      'profile' => $entitlements['person_type_profile'],
      'user' => $entitlements['person_type_user'],
      'partner' => $entitlements['person_type_partner']

    );

    return $entitlements;

  }

  //
  function getPersonbyMemberID($member_ID, $amplify = false) {

    //////////// GET PERSON INFORMATION FROM PERSON ID //////////////////
    $condition_person = "AND person_type_member = '{$member_ID}' ";

    $sql_person  = "SELECT person_ID,person_first_name,person_last_name,person_email,person_phone FROM persons ";
    $sql_person .= "WHERE active = 1 ";
    $sql_person .= $condition_person;
    $sql_person .= "LIMIT 1 ";

    $query_person = query($sql_person);

    $person = fetch_array($query_person);
    $person_ID = $person['person_ID'];
    $person_first_name = $person['person_first_name'];
    $person_last_name = $person['person_last_name'];
    $person_email = $person['person_email'];
    $person_phone = $person['person_phone'];
    //////////// GET PERSON INFORMATION FROM PERSON ID //////////////////

    $person = array(

      'person_ID' => $person['person_ID'],
      'first_name' => $person['person_first_name'],
      'last_name' => $person['person_last_name'],
      'email' => $person['person_email'],
      'phone' => $person['person_phone']

    );

    return $person;

  }

  //
  function getProfilebyMemberID($member_ID, $amplify = false) {

    //////////// GET PROFILE INFORMATION FROM MEMBER ID //////////////////
    $condition_profile = "AND member_ID = '{$member_ID}' ";

    $sql_profile  = "SELECT profile_ID,profile_image_profile,profile_headline,profile_bio FROM profiles ";
    $sql_profile .= "WHERE active = 1 ";
    $sql_profile .= $condition_profile;
    $sql_profile .= "LIMIT 1 ";

    $query_profile = query($sql_profile);

    $profile = fetch_array($query_profile);
    $profile_ID = $profile['profile_ID'];
    $profile_image_profile = $profile['profile_image_profile'];
    $profile_headline = $profile['profile_headline'];
    $profile_bio = $profile['profile_bio'];
    //////////// GET PROFILE INFORMATION FROM MEMBER ID //////////////////

    $profile = array(

      'profile_ID' => $profile['profile_ID'],
      'image_profile' => getImagebyImageID($profile['profile_image_profile']),
      'headline' => $profile['profile_headline'],
      'bio' => $profile['profile_bio']

    );

    return $profile;

  }

  //
  function getSkillsbyMemberID($member_ID, $limit, $amplify = false) {

    //
    $conditions_skill = "AND member_ID = '" . $member_ID . "' ";

    $sql_skill  = "SELECT skill_label FROM skills ";
    $sql_skill .= "WHERE active = 1 ";
    $sql_skill .= $conditions_skill;
    $sql_skill .= "ORDER BY id DESC";

    if($limit){$sql_skill .= " LIMIT " . $limit;}

    $result_skill = query($sql_skill);

    //////////////////
    $skills = NULL;
    $count = row_count($result_skill);
    if(!$count > 0) {
      return $skills;
    }
    //////////////////

    //$skill = fetch_array($result_skill);
    //$skill_label = $skill['skill_label'];

    //
    while($skill = fetch_array($result_skill)) {

      //$skills[$row['skill_label']]; // or smth like $row["video_title"] for title
      $skills[] = $skill['skill_label'];

    }

    return $skills;

  }

  //
  function getBrandsbyMemberID($member_ID, $amplify = false) {

    //
    $brands = [];

    ///// QUERY 1 [ ////////////////////
    $conditions_clients = "AND member_ID = '" . $member_ID . "' ";

    $sql_clients  = "SELECT position_clients FROM positions ";
    $sql_clients .= "WHERE active = 1 ";
    $sql_clients .= $conditions_clients;

    //
    $result_clients = query($sql_clients);

    //
    while($client = fetch_array($result_clients)) {

      // https://www.codeproject.com/Questions/1180937/How-to-merge-array-in-one-array-under-foreach-loop
      $brands = array_unique(array_merge($brands,arraySplitter($client['position_clients'])));

    }
    ///// ] QUERY 1 ////////////////////

    ///// QUERY 2 [ ////////////////////
    $conditions_moreclients = "AND member_ID = '" . $member_ID . "' ";

    $sql_moreclients  = "SELECT organization_ID_client FROM projects ";
    $sql_moreclients .= "WHERE active = 1 ";
    $sql_moreclients .= $conditions_moreclients;

    $result_moreclients = query($sql_moreclients);

    //
    while($client = fetch_array($result_moreclients)) {

      //$skills[$row['skill_label']]; // or smth like $row["video_title"] for title
      $brands[] = $client['organization_ID_client'];
      //$brands = array_merge($brands);
      //https://www.codeproject.com/Questions/1180937/How-to-merge-array-in-one-array-under-foreach-loop

    }
    ///// ] QUERY 2 ////////////////////

    //print_r($brands);exit;
    return getOrganizationsbyOrganizationIDs(array_unique($brands), false);
    //return $brands;

  }

  //
  function getCategoriesbyMemberID($member_ID, $amplify = false) {

    //
    $categories = [];

    ///// QUERY 1 [ ////////////////////
    $conditions_categories = "AND member_ID = '" . $member_ID . "' ";

    $sql_categories  = "SELECT project_category FROM projects ";
    $sql_categories .= "WHERE active = 1 ";
    $sql_categories .= $conditions_categories;

    //
    $result_categories = query($sql_categories);

    //
    while($category = fetch_array($result_categories)) {

      // https://www.codeproject.com/Questions/1180937/How-to-merge-array-in-one-array-under-foreach-loop
      $categories = array_unique(arraySplitter(getCategorybyCategoryID($category['project_category'])));

    }
    ///// ] QUERY 1 ////////////////////

    //print_r($categories);exit;
    return array_unique($categories);
    //return $brands;

  }

  //
  function getInterestsbyMemberID($member_ID, $limit, $amplify = false) {

    ////////////////
    $conditions_interests = "AND member_ID = '" . $member_ID . "' ";

    $sql_interests  = "SELECT interest_label FROM interests ";
    $sql_interests .= "WHERE active = 1 ";
    $sql_interests .= $conditions_interests;

    if($limit){$sql_interests .= " LIMIT " . $limit;}

    $result_interests = query($sql_interests);

    //////////////////
    $interests = NULL;
    $count = row_count($result_interests);
    if(!$count > 0) {
      return $interests;
    }
    //////////////////

    //$interest = fetch_array($result_interest);
    //$interest_label = $interest['interest_label'];

    //
    while($interest = fetch_array($result_interests)) {

      //$interests[$row['interest_label']]; // or smth like $row["video_title"] for title
      $interests[] = $interest['interest_label'];

    }

    return $interests;

  }

  //
  function getProjectsbyMemberID($member_ID, $amplify = false) {

    ////////////////
    $conditions_projects = "AND member_ID = '" . $member_ID . "' ";

    $sql_projects  = "SELECT project_ID,project_name,project_access,project_description,project_role,project_industry,project_images,project_finish_year,project_category,organization_ID_sponsor,organization_ID_client FROM projects ";
    $sql_projects .= "WHERE active = 1 ";
    $sql_projects .= $conditions_projects;
    //$sql_interest .= "LIMIT 1";

    $result_projects = query($sql_projects);

    //////////////////
    $projects = NULL;
    $count = row_count($result_projects);
    if(!$count > 0) {
      return $projects;
    }
    //////////////////

    //$interest = fetch_array($result_interest);
    //$interest_label = $interest['interest_label'];

    //
    while($project = fetch_array($result_projects)) {

      $projects[] = array(

        'project_ID' => $project['project_ID'],
        'name' => $project['project_name'],
        'description' => $project['project_description'],
        'role' => $project['project_role'],
        'industry' => getMediabyMediaID($project['project_industry']),
        'images' => getImagesbyImageIDs($project['project_images']),
        'finish_year' => $project['project_finish_year'],
        'access' => $project['project_access'],
        'category' => arraySplitter($project['project_category']),
        'sponsor' => getOrganizationbyOrganizationID($project['organization_ID_sponsor']),
        'client' => getOrganizationbyOrganizationID($project['organization_ID_client'])

      );

    }

    return $projects;

  }

  //
  function getPositionsbyMemberID($member_ID, $amplify = false) {

    ////////////////
    $conditions_positions = "AND member_ID = '" . $member_ID . "' ";

    $sql_positions  = "SELECT position_ID,position_title,position_description,position_start_year,position_start_month,position_finish_year,position_finish_month,position_ongoing,position_clients,organization_ID FROM positions ";
    $sql_positions .= "WHERE active = 1 ";
    $sql_positions .= $conditions_positions;

    $result_positions = query($sql_positions);

    //////////////////
    $positions = NULL;
    $count = row_count($result_positions);
    if(!$count > 0) {
      return $positions;
    }
    //////////////////

    //
    while($position = fetch_array($result_positions)) {

      $positions[] = array(

        'position_ID' => $position['position_ID'],
        'title' => $position['position_title'],
        'description' => $position['position_description'],
        'start_year' => $position['position_start_year'],
        'start_month' => $position['position_start_month'],
        'finish_year' => $position['position_finish_year'],
        'finish_month' => $position['position_finish_month'],
        'ongoing' => $position['position_ongoing'],
        'clients' => getOrganizationsbyOrganizationIDs($position['position_clients']),
        'organization' => getOrganizationbyOrganizationID($position['organization_ID'])

      );

    }

    return $positions;

  }

  //
  function getAcademicsbyMemberID($member_ID, $amplify = false) {

    ////////////////
    $conditions_academics = "AND member_ID = '" . $member_ID . "' ";

    $sql_academics  = "SELECT academic_ID,academic_institution,academic_certification,academic_field,academic_description,academic_start_year,academic_start_month,academic_finish_year,academic_finish_month,academic_ongoing FROM academics ";
    $sql_academics .= "WHERE active = 1 ";
    $sql_academics .= $conditions_academics;
    //$sql_interest .= "LIMIT 1";

    $result_academics = query($sql_academics);

    //////////////////
    $academics = NULL;
    $count = row_count($result_academics);
    if(!$count > 0) {
      return $academics;
    }
    //////////////////

    //$interest = fetch_array($result_interest);
    //$interest_label = $interest['interest_label'];

    //
    while($academic = fetch_array($result_academics)) {

      $academics[] = array(

        'academic_ID' => $academic['academic_ID'],
        'institution' => getInstitutionbyInstitutionID($academic['academic_institution']),
        'certification' => $academic['academic_certification'],
        'field' => $academic['academic_field'],
        'description' => $academic['academic_description'],
        'start_year' => $academic['academic_start_year'],
        'start_month' => $academic['academic_start_month'],
        'finish_year' => $academic['academic_finish_year'],
        'finish_month' => $academic['academic_finish_month'],
        'ongoing' => $academic['academic_ongoing']

      );

    }

    return $academics;

  }

  //
  function getActivitiesbyMemberID($member_ID, $amplify = false) {

    ////////////////
    $conditions_activities = "AND member_ID = '" . $member_ID . "' ";

    $sql_activities  = "SELECT activity_ID,activity_role,activity_description,activity_start_year,activity_start_month,activity_finish_year,activity_finish_month,activity_ongoing,organization_ID FROM activities ";
    $sql_activities .= "WHERE active = 1 ";
    $sql_activities .= $conditions_activities;
    //$sql_interest .= "LIMIT 1";

    $result_activities = query($sql_activities);

    //////////////////
    $activities = NULL;
    $count = row_count($result_activities);
    if(!$count > 0) {
      return $activities;
    }
    //////////////////

    //$interest = fetch_array($result_interest);
    //$interest_label = $interest['interest_label'];

    //
    while($activity = fetch_array($result_activities)) {

      $activities[] = array(

        'activity_ID' => $activity['activity_ID'],
        'role' => $activity['activity_role'],
        'description' => $activity['activity_description'],
        'start_year' => $activity['activity_start_year'],
        'start_month' => $activity['activity_start_month'],
        'finish_year' => $activity['activity_finish_year'],
        'finish_month' => $activity['activity_finish_month'],
        'ongoing' => $activity['activity_ongoing'],
        'organization' => getOrganizationbyOrganizationID($activity['organization_ID']),
        'member_ID' => $member_ID

      );

    }

    return $activities;

  }

  //
  function getAccomplishmentsbyMemberID($member_ID, $amplify = false) {

    ////////////////
    $conditions_accomplishments = "AND member_ID = '" . $member_ID . "' ";

    $sql_accomplishments  = "SELECT accomplishment_ID,accomplishment_title,accomplishment_description,accomplishment_issuer,accomplishment_issue_year,accomplishment_issue_month,organization_ID,member_ID FROM accomplishments ";
    $sql_accomplishments .= "WHERE active = 1 ";
    $sql_accomplishments .= $conditions_accomplishments;

    $result_accomplishments = query($sql_accomplishments);

    //////////////////
    $accomplishments = NULL;
    $count = row_count($result_accomplishments);
    if(!$count > 0) {
      return $accomplishments;
    }
    //////////////////

    //
    while($accomplishment = fetch_array($result_accomplishments)) {

      $accomplishments[] = array(

        'accomplishment_ID' => $accomplishment['accomplishment_ID'],
        'title' => $accomplishment['accomplishment_title'],
        'description' => $accomplishment['accomplishment_description'],
        'issuer' => $accomplishment['accomplishment_issuer'],
        'issue_year' => $accomplishment['accomplishment_issue_year'],
        'issue_month' => $accomplishment['accomplishment_issue_month'],
        'organization_ID' => getOrganizationbyOrganizationID($accomplishment['organization_ID'])

      );

    }

    return $accomplishments;

  }

  //
  function arraySplitter($array) { //CSA comma separated array

    //
    $array = trim($array,','); // remove outward commas
    $items = explode(',',$array); // you have array now
    //krsort($items); // forces friends to return with the latest friend first

    //print_r($items);exit;
    return $items;

  }

  //
  function getImagesbyImageIDs($array, $amplify = false) { //CSA comma separated array

    //
    //print($array);exit;
    $array = trim($array,','); // remove outward commas
    $items = explode(',',$array); // you have array now
    //$items = $array;//explode(',',$array); // you have array now

    $details = array();

    //
    foreach($items as $item) {

      $details[] = getImagebyImageID($item);

    }

    return $details;

  }

  //
  function getSmartImagesbyPostID($post_ID) {

    if(!$post_ID == NULL || !$post_ID == '') {

      //
      $conditions_post = "AND post_ID = '{$post_ID}' ";

      $sql_post  = "SELECT images FROM posts ";
      $sql_post .= "WHERE user_closed = 'no' ";
      $sql_post .= "AND deleted = 'no' ";
      $sql_post .= $conditions_post;
      $sql_post .= "LIMIT 1";

      $result_post = query($sql_post);

      $detail = fetch_array($result_post);

      $arrayofimages = getImagesbyImageIDs($detail[0]);

      foreach ($arrayofimages as $single) {

        if($single['primary']==1) {
          $primary = getImagebyImageID($single['image_ID']);
        }
        elseif($single['primary']==0) {
          $secondary[] = getImagebyImageID($single['image_ID']);
        }
        elseif($single['primary']==NULL){
            $secondary[] = 1;
        }

      }

    }

    else {

      $detail = NULL;

    }

    $images = array(
      'primary' => $primary,
      'secondary' => $secondary
    );

    return $images;

  }

  //
  function getPostbyPostID($post_ID, $amplify = false) {

    //
    if(!$post_ID == NULL || !$post_ID == '') {

      //
      $conditions_post = "AND post_ID = '{$post_ID}' ";

      $sql_post  = "SELECT * FROM posts ";
      $sql_post .= "WHERE user_closed = 'no' ";
      $sql_post .= "AND deleted = 'no' ";
      $sql_post .= $conditions_post;
      $sql_post .= "LIMIT 1";

      $result_post = query($sql_post);

      $detail = fetch_array($result_post);

      $details = array(

        'id' => $detail['post_ID'],
        'type' => $detail['post_type'],
        'author' => $detail['added_by'],
        'title' => $detail['title'],
        'slug' => $detail['slug'],
        'body' => $detail['body'],
        'tags' => $detail['tags'],
        'likes' => $detail['likes'],
        'verifications' => $detail['post_verifications'],
        'images' => getImagesbyImageIDs($detail['images']),

        'member' => array(

          'id' => $detail['member_ID']

        )

      );

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getImagebyImageID($image_ID, $node = false, $amplify = false) {

    if(!$image_ID == NULL || !$image_ID == '') {

      //
      $conditions_image = "AND image_ID = '{$image_ID}' ";
      $sql_image  = "SELECT image_type,image_status,image_object,image_caption,image_primary,image_filename FROM images ";
      $sql_image .= "WHERE active = 1 ";
      $sql_image .= $conditions_image;
      $sql_image .= "LIMIT 1";

      $result_image = query($sql_image);

      $detail = fetch_array($result_image);

      if(!empty($detail['image_caption'])) { $image_caption = $detail['image_caption']; } else { $image_caption = NULL; }
      $image_url = APP_ST_CDN . 'images/' . $detail['image_type'] . '/' . $detail['image_filename'];
      $image_primary = $detail['image_primary'];

      //
      switch ($node) {

        case 'url':
          $details = $image_url;
          break;

        default:
          $details = array(

            'image_ID' => $image_ID,
            'url' => $image_url,
            'caption' => $image_caption,
            'primary' => $image_primary

          );

          break;

      }

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getInstitutionbyInstitutionID($institution_ID, $amplify = false) {

    if(!$institution_ID == NULL || !$institution_ID == '') {

      //
      $conditions_institution = "AND institution_ID = '{$institution_ID}' ";
      $sql_institution  = "SELECT institution_name FROM institutions ";
      $sql_institution .= "WHERE active = 1 ";
      $sql_institution .= $conditions_institution;
      $sql_institution .= "LIMIT 1";

      $result_institution = query($sql_institution);

      $detail = fetch_array($result_institution);

      $institution_name = $detail['institution_name'];

      $details = $institution_name;

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getMediabyMediaID($media_ID, $amplify = false) {

    if(!$media_ID == NULL || !$media_ID == '') {

      //
      $conditions_media = "AND media_ID = '{$media_ID}' ";
      $sql_media  = "SELECT media_label FROM media ";
      $sql_media .= "WHERE active = 1 ";
      $sql_media .= $conditions_media;
      $sql_media .= "LIMIT 1";

      $result_media = query($sql_media);

      $detail = fetch_array($result_media);

      $media_name = $detail['media_label'];

      $details = $media_name;

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getCategorybyCategoryID($category_ID, $amplify = false) {

    if(!$category_ID == NULL || !$category_ID == '') {

      //
      $conditions_category = "AND category_ID = '{$category_ID}' ";
      $sql_category  = "SELECT category_name FROM categories ";
      $sql_category .= "WHERE active = 1 ";
      $sql_category .= $conditions_category;
      $sql_category .= "LIMIT 1";

      $result_category = query($sql_category);

      $detail = fetch_array($result_category);

      $category_name = $detail['category_name'];

      $details = $category_name;

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getCategoriesbyCategoryIDs($array, $amplify = false) { //CSA comma separated array

    //
    //print_r($array);exit;

    //if(substr($array, 0, 1) === ',') {
    if(is_array($array)) {

      //$array = trim($array,','); // remove outward commas
      //$items = explode(',',$array); // you have array now
      $items = $array;//explode(',',$array); // you have array now

      $details = array();

      //
      foreach($items as $item) {

        //

        $details[] = getCategorybyCategoryID($item,$amplify);

      }

      return $details;

    }

  }

  //
  function getOrganizationsbyOrganizationIDs($list, $amplify = false) { //CSA comma separated array

    //
    //print_r($array);exit;

    //if(substr($array, 0, 1) === ',') {
    if(is_array($list)) {

      //$array = trim($array,','); // remove outward commas
      //$items = explode(',',$array); // you have array now
      $items = $list;//explode(',',$array); // you have array now

      $details = array();

      //
      foreach($items as $item) {

        //

        $details[] = getOrganizationbyOrganizationID($item,$amplify);

      }

      return $details;

    }

    else { // This either means it's comma separated or by itself

      if (substr($list,0,1) === ',') {

        $items = arraySplitter($list);
        $item = $list;//explode(',',$array); // you have array now

        $details = array();

        //
        foreach($items as $item) {

          //

          $details[] = getOrganizationbyOrganizationID($item,$amplify);

        }

        return $details;

      }

      else {
        $item = $list;
        $details = array();
        $details[] = getOrganizationbyOrganizationID($list,$amplify);
        return $details;

      }

    }

  }

  //
  function getOrganizationbyOrganizationID($organization_ID, $amplify = false) {

    ////////////////
    $conditions_organization = "AND organization_ID = '" . $organization_ID . "' ";

    $sql_organization  = "SELECT organization_name,organization_description,organization_image_logo,organization_image_cover FROM organizations ";
    $sql_organization .= "WHERE active = 1 ";
    $sql_organization .= $conditions_organization;
    $sql_organization .= "LIMIT 1";

    $result_organization = query($sql_organization);

    $organization = fetch_array($result_organization);

    //
    if($amplify == true) {

      $details = array(

        'organization_ID' => $organization_ID,
        'name' => $organization['organization_name'],
        'description' => $organization['organization_description'],
        'logo' => getImagebyImageID($organization['organization_image_logo']),
        'cover' => getImagebyImageID($organization['organization_image_cover'])

      );

    }

    else {

      $details = array(

        'name' => $organization['organization_name'],
        'logo' => getImagebyImageID($organization['organization_image_logo'])

      );
    }

    return $details;

  }

  //
  function getOpportunitybyOpportunityID($opportunity_ID, $amplify = false) {

    ////////////
    $condition_opportunity = "AND opportunity_ID = '{$opportunity_ID}' ";

    $sql_opportunity  = "SELECT
      opportunity_ID,
      opportunity_role,
      opportunity_description,
      opportunity_summary,
      opportunity_city,
      opportunity_state,
      opportunity_country,
      opportunity_postal_code,
      opportunity_salary_rate,
      opportunity_salary_terms,
      opportunity_type,
      opportunity_tags,
      opportunity_requirements,
      opportunity_responsibilities,
      /*opportunity_academics,*/
      organization_ID,
      partner_ID
      FROM opportunities "
    ;
    $sql_opportunity .= "WHERE active = 1 ";
    $sql_opportunity .= $condition_opportunity;
    $sql_opportunity .= "LIMIT 1";

    $query_opportunity = query($sql_opportunity);

    $opportunity = fetch_array($query_opportunity);

    ////////////

    $opportunity = array(

      'opportunity_ID' => $opportunity['opportunity_ID'],
      'role' => $opportunity['opportunity_role'],
      'description' => $opportunity['opportunity_description'],
      'summary' => $opportunity['opportunity_summary'],
      'city' => $opportunity['opportunity_city'],
      'state' => $opportunity['opportunity_state'],
      'country' => $opportunity['opportunity_country'],
      'postal_code' => $opportunity['opportunity_postal_code'],
      'salary_rate' => $opportunity['opportunity_salary_rate'],
      'salary_terms' => $opportunity['opportunity_salary_terms'],
      'type' => $opportunity['opportunity_type'],
      'tags' => getTagsbyTagIDs($opportunity['opportunity_tags']),
      'requirements' => getRequirementsbyRequirementIDs($opportunity['opportunity_requirements']),
      'responsibilities' => getResponsibilitiesbyResponsibilityIDs($opportunity['opportunity_responsibilities']),
      'organization_ID' => $opportunity['organization_ID'],
      'partner_ID' => $opportunity['partner_ID']

    );

    return $opportunity;

  }

  //
  function getRequirementsbyRequirementIDs($array, $amplify = false) { //CSA comma separated array

    //
    //print($array);exit;
    $array = trim($array,','); // remove outward commas
    $items = explode(',',$array); // you have array now
    $items = array_unique($items); // ensures no duplicates

    $details = array();

    //
    foreach($items as $item) {

      $details[] = getRequirementbyRequirementID($item);

    }

    return $details;

  }

  //
  function getRequirementbyRequirementID($requirement_ID, $amplify = false) {

    if(!$requirement_ID == NULL || !$requirement_ID == '') {

      //
      $conditions_requirement = "AND requirement_ID = '{$requirement_ID}' ";
      $sql_requirement  = "SELECT * FROM requirements ";
      $sql_requirement .= "WHERE active = 1 ";
      $sql_requirement .= $conditions_requirement;
      $sql_requirement .= "LIMIT 1";

      $result_requirement = query($sql_requirement);

      $detail = fetch_array($result_requirement);

      $details = array(

        'requirement_ID' => $detail['requirement_ID'],
        'description' => $detail['requirement_description'],
        'type' => $detail['requirement_type'],
        'priority' => $detail['requirement_priority'],
        'opportunity_ID' => $detail['opportunity_ID'],
        'partner_ID' => $detail['partner_ID']

      );

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getResponsibilitiesbyResponsibilityIDs($array, $amplify = false) { //CSA comma separated array

    //
    //print($array);exit;
    $array = trim($array,','); // remove outward commas
    $items = explode(',',$array); // you have array now
    $items = array_unique($items); // ensures no duplicates

    $details = array();

    //
    foreach($items as $item) {

      $details[] = getResponsibilitybyResponsibilityID($item);

    }

    return $details;

  }

  //
  function getResponsibilitybyResponsibilityID($responsibility_ID, $amplify = false) {

    if(!$responsibility_ID == NULL || !$responsibility_ID == '') {

      //
      $conditions_responsibility = "AND responsibility_ID = '{$responsibility_ID}' ";
      $sql_responsibility  = "SELECT * FROM responsibilities ";
      $sql_responsibility .= "WHERE active = 1 ";
      $sql_responsibility .= $conditions_responsibility;
      $sql_responsibility .= "LIMIT 1";

      $result_responsibility = query($sql_responsibility);

      $detail = fetch_array($result_responsibility);

      $details = array(

        'responsibility_ID' => $detail['responsibility_ID'],
        'description' => $detail['responsibility_description'],
        'priority' => $detail['responsibility_priority'],
        'opportunity_ID' => $detail['opportunity_ID'],
        'partner_ID' => $detail['partner_ID']

      );

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getTagsbyTagIDs($array, $amplify = false) { //CSA comma separated array

    //
    //print($array);exit;
    $array = trim($array,','); // remove outward commas
    $items = explode(',',$array); // you have array now
    $items = array_unique($items); // ensures no duplicates

    $details = array();

    //
    foreach($items as $item) {

      $details[] = getTagbyTagID($item);

    }

    return $details;

  }

  //
  function getTagbyTagID($tag_ID, $amplify = false) {

    if(!$tag_ID == NULL || !$tag_ID == '') {

      //
      $conditions_tag = "AND tag_ID = '{$tag_ID}' ";
      $sql_tag  = "SELECT * FROM tags ";
      $sql_tag .= "WHERE active = 1 ";
      $sql_tag .= $conditions_tag;
      $sql_tag .= "LIMIT 1";

      $result_tag = query($sql_tag);

      $detail = fetch_array($result_tag);

      $details = array(

        'tag_ID' => $detail['tag_ID'],
        'description' => $detail['tag_description'],
        'priority' => $detail['tag_priority'],
        'opportunity_ID' => $detail['opportunity_ID'],
        'partner_ID' => $detail['partner_ID']

      );

    }

    else {

      $details = NULL;

    }

    return $details;

  }

  //
  function getResultsbySkills($query,$limit) {

    //
    $conditions = "";

    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="AND skill_label LIKE '%".$q."%' ";}else{$conditions.="";}

    if(isset($_REQUEST['duration'])){$search_duration=clean($_REQUEST['duration']);$conditions.="AND skill_duration = '".$search_duration."' ";}else{$conditions.="";}
    if(isset($_REQUEST['level'])){$search_level=clean($_REQUEST['level']);$conditions.="AND skill_level = '".$search_level."' ";}else{$conditions.="";}
    if(isset($_REQUEST['used_last'])){$search_used_last=clean($_REQUEST['used_last']);$conditions.="AND skill_used_last = '".$search_used_last."' ";}else{$conditions.="";}
    if(isset($_REQUEST['category'])){$search_category=clean($_REQUEST['category']);$conditions.="AND skill_category LIKE '%".$search_category."%' ";}else{$conditions.="";}
    if(isset($_REQUEST['member'])){$member_ID=clean($_REQUEST['member']);$conditions.="AND member_ID = '".$member_ID."' ";}else{$conditions.="";}

    // ORDER BY
    //$order_by = " ORDER BY skill_duration DESC, skill_level DESC, skill_verifications DESC, time_started ASC ";
    $order_by = " ORDER BY member_ID ASC ";

    //////////////////////////////
    $sql_skills_search  = "SELECT DISTINCT member_ID FROM skills ";
    $sql_skills_search .= "WHERE active = 1 ";
    $sql_skills_search .= $conditions;
    $sql_skills_search .= $order_by;
    $sql_skills_search .= $limit;
    $sql_skills_search .= ";";

    //echo $sql_skills_search; exit;

    $query_skills_search = query($sql_skills_search);

    //print_r($query_skills_search);exit;

    //
    if(row_count($query_skills_search)>0) {

      while ($result = fetch_array($query_skills_search)) {

        $score = rand(7,77);

        $results[] = array (

          'member' => $result['member_ID'],
          'score' => $score,
          'domain' => 'skills'

        );

      }

    } else { $results[] = NULL; }

    //print_r($results); exit;

    return $results;

  }

  //
  function getResultsbyInterests($query,$limit) {

    //
    $conditions = "";

    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="AND interest_label LIKE '%".$q."%' ";}else{$conditions.="";}

    // ORDER BY
    //$order_by = " ORDER BY skill_duration DESC, skill_level DESC, skill_verifications DESC, time_started ASC ";
    $order_by = " ORDER BY member_ID ASC ";

    //////////////////////////////
    $sql_interests_search  = "SELECT DISTINCT member_ID FROM interests ";
    $sql_interests_search .= "WHERE active = 1 ";
    $sql_interests_search .= $conditions;
    $sql_interests_search .= $order_by;
    $sql_interests_search .= $limit;
    $sql_interests_search .= ";";

    //echo $sql_interests_search; exit;

    $query_interests_search = query($sql_interests_search);

    //print_r($query_interests_search);exit;

    //
    if(row_count($query_interests_search)>0) {

      while ($result = fetch_array($query_interests_search)) {

        $score = rand(7,77);

        $results[] = array (

          'member' => $result['member_ID'],
          'score' => $score,
          'domain' => 'interests'

        );

      }

    } else { $results[] = NULL; }

    //print_r($results); exit;

    return $results;

  }

  //
  function getResultsbyPositions($query,$limit) {

    //
    $conditions = "";

    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="AND position_title LIKE '%".$q."%' ";}else{$conditions.="";}
    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="OR position_description LIKE '%".$q."%' ";}else{$conditions.="";}

    // ORDER BY
    //$order_by = " ORDER BY skill_duration DESC, skill_level DESC, skill_verifications DESC, time_started ASC ";
    $order_by = " ORDER BY member_ID ASC ";

    //////////////////////////////
    $sql_positions_search  = "SELECT DISTINCT member_ID FROM positions ";
    $sql_positions_search .= "WHERE active = 1 ";
    $sql_positions_search .= $conditions;
    $sql_positions_search .= $order_by;
    $sql_positions_search .= $limit;
    $sql_positions_search .= ";";

    //print_r($sql_positions_search);
    //exit;

    $query_positions_search = query($sql_positions_search);

    //print_r($query_positions_search);
    //exit;

    //
    if(row_count($query_positions_search)>0) {

      while ($result = fetch_array($query_positions_search)) {

        $score = rand(7,77);

        $results[] = array (

          'member' => $result['member_ID'],
          'score' => $score,
          'domain' => 'positions'

        );

      }

    } else { $results[] = NULL; }

    //print_r($results); exit;

    return $results;

  }

  //
  function getResultsbyProjects($query,$limit) {

    //
    $conditions = "";

    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="AND project_name LIKE '%".$q."%' ";}else{$conditions.="";}
    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="OR project_description LIKE '%".$q."%' ";}else{$conditions.="";}
    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="OR project_role LIKE '%".$q."%' ";}else{$conditions.="";}

    // ORDER BY
    //$order_by = " ORDER BY skill_duration DESC, skill_level DESC, skill_verifications DESC, time_started ASC ";
    $order_by = " ORDER BY member_ID ASC ";

    //////////////////////////////
    $sql_projects_search  = "SELECT DISTINCT member_ID FROM projects ";
    $sql_projects_search .= "WHERE active = 1 ";
    $sql_projects_search .= $conditions;
    $sql_projects_search .= $order_by;
    $sql_projects_search .= $limit;
    $sql_projects_search .= ";";

    //print_r($sql_positions_search);
    //exit;

    $query_projects_search = query($sql_projects_search);

    //print_r($query_projects_search);
    //exit;

    //
    if(row_count($query_projects_search)>0) {

      while ($result = fetch_array($query_projects_search)) {

        $score = rand(7,77);

        $results[] = array (

          'member' => $result['member_ID'],
          'score' => $score,
          'domain' => 'projects'

        );

      }

    } else { $results[] = NULL; }

    //print_r($results); exit;

    return $results;

  }

  //
  function getResultsbyPersons($query,$limit) {

    //
    $conditions = "";

    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="AND person_first_name LIKE '%".$q."%' ";}else{$conditions.="";}
    if(isset($_REQUEST['q'])){$q=clean($_REQUEST['q']);$conditions.="OR person_last_name LIKE '%".$q."%' ";}else{$conditions.="";}

    // ORDER BY
    //$order_by = " ORDER BY skill_duration DESC, skill_level DESC, skill_verifications DESC, time_started ASC ";
    $order_by = " ORDER BY person_type_member ASC ";

    //////////////////////////////
    $sql_persons_search  = "SELECT DISTINCT person_type_member FROM persons ";
    $sql_persons_search .= "WHERE active = 1 ";
    $sql_persons_search .= $conditions;
    $sql_persons_search .= $order_by;
    $sql_persons_search .= $limit;
    $sql_persons_search .= ";";

    //print_r($sql_persons_search);
    //exit;

    $query_persons_search = query($sql_persons_search);

    //print_r($query_persons_search);
    //exit;

    //echo row_count($query_persons_search); exit;

    //
    if(row_count($query_persons_search)>0) {

      while ($result = fetch_array($query_persons_search)) {

        $score = rand(7,77);

        $results[] = array (

          'member' => $result['person_type_member'],
          'score' => $score,
          'domain' => 'persons'

        );

      }

    } else { $results[] = NULL; }

    //print_r($results); exit;

    return $results;

  }

?>
