<?php

  //
  require 'core.php';

  //
  if (empty($_REQUEST['first_name']) || empty($_REQUEST['username']) || empty($_REQUEST['email']) || empty($_REQUEST['password'])) {

    $response['status'] = 400;
    $response['message'] = "Missing required information";

    header('Content-Type: application/json');
    echo json_encode($response);

    return;

  }

  // Securing information and storing variables
  $user_ID = $access->generate_ID('user');

  //
  $request = array();

  //
  $request['first_name'] = htmlentities($_REQUEST['first_name']);
  if (!empty($_REQUEST['last_name'])){$request['last_name'] = htmlentities($_REQUEST['last_name']);}else{$request['last_name']=NULL;}
  $request['username'] = htmlentities($_REQUEST['username']);
  $request['email'] = htmlentities($_REQUEST['email']);
  if (!empty($_REQUEST['phone'])){$request['phone'] = htmlentities($_REQUEST['phone']);}else{$request['phone']=NULL;}
  $password = htmlentities($_REQUEST['password']);
  $request['app'] = htmlentities($_REQUEST['app']);

  // secure password
  $request['salt'] = openssl_random_pseudo_bytes(30);
  $request['secured_password'] = sha1($password . $request['salt']);

  //echo print_r($request);exit;

  //
  $person = $access->getPerson('email',$request['email'],true);

  //echo print_r($person);exit;

  // Is this email attached to an existing PERSON?
  if($person) {

    // Person exists so we must retrieve currnt data about the customer if they are verified.
    // This won't be the case in the beginning.
    //echo "not blank";
    //echo print_r($emailExists); exit;
    $request['person'] = $person['id'];

  }

  else {

    // Person must be created
    $request['person'] = $access->generate_ID('person');

    // Transforms $person variable to full person DATA
    $person = $access->createPerson($request);

  }

  //echo print_r($request);exit;
  $request['user'] = $access->generate_ID('user');

  // STEP 3. Insert user information
  $user = $access->createUser($request);

  //echo print_r($user);exit;

  // successfully registered
  if($user) {

    //echo print_r($user);exit;

    $request['profile'] = $access->generate_ID('profile');

    //echo print_r($request);exit;

    /* $profile_ID, $first_name, $user_ID, $app */
    $access->createProfile($request);

    // get current registered user information and store in $user
    $user = $access->getUser('ID', $request['user']);
    $person = $access->getPerson('profile', $request['profile']);
    $profile = $access->getProfile('ID', $request['profile']);

    //echo print_r($user);
    //echo print_r($profile);
    //exit;

    // declare information to feedback to user of App as json
    $response['status'] = 200;
    $response['message'] = "Successfully signed up!";
    $response['id'] = $user['id'];
    $response['lastlogin'] = $user['lastlogin'];
    $response['validation'] = $user['validation'];
    $response['welcome'] = $user['welcome'];
    $response['images'] = $profile['images'];
    $response['bio'] = $profile['bio'];
    $response['profile'] = $profile['id'];
    $response['headline'] = $profile['headline'];
    $response['access'] = $profile['access'];


    //print_r($response);
    //exit;

    // STEP 4. Emailing
    // include email.php
    require 'notification.php';

    // store all class in $email var
    $notification = new notification();

    // store generated token in $token var
    $token = $notification->generateToken(20);

    // save inf in 'emailTokens' table
    $access->saveToken("password", $request['person'], $token, $app);

    //echo print_r($request['email']);exit;

    // refer emailing information
    $details = array();
    $details["subject"] = "Email confirmation on Venny";
    $details["to"] = $request['email'];
    $details["organization"] = "Notearise";
    $details["sender"] = "hello@notearise.com";

    // access template file
    $template = $notification->emailTemplate('createUser');

    // replace {token} from confirmationTemplate.html by $token and store all content in $template var
    $template = str_replace("{token}", $token, $template);
    $template = str_replace("{organization}", APP_NAME, $template);
    $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

    $details["body"] = $template;

    $notification->send_email($details);

  }

  else {

    $response["status"] = 400;
    $response["message"] = "Could not sign up with the provided information";

  }

  // STEP 5. Close connection
  $access->disconnect();

  // STEP 6. JSON data
  header('Content-Type: application/json');
  echo json_encode($response);

?>
