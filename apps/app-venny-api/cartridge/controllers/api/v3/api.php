<?php

  //
  header('Content-Type: application/json');

  //
  require 'controllers/components/core/core-user.php';
  require 'controllers/components/functions/function-core.php';

  // Catch request method...
  $method = $_SERVER['REQUEST_METHOD'];

  // Catch parameters of request...
  $request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

  // Send request to respective API endpoint...
  switch ($method) {

    //
    case 'GET': require 'controllers/api/GET/api-get.php'; break;
    case 'POST': require 'controllers/api/POST/api-post.php'; break;

    //
    default: header("Location: index.php");

  }

?>
