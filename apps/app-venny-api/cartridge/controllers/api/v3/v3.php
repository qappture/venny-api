<?php

  //
  require 'properties.php';
  require 'functions.php';
  require 'environment.php';

  $version = 'v3';

  /*
  echo $_REQUEST['token'] . ' | ';
  echo $_REQUEST['domain'] . ' | ';
  echo $_REQUEST['app'];
  exit;
  */

  // Do nothing without a token...
  if(!empty($_REQUEST['token'])) {

    //
    $token = $_REQUEST['token'];

    //
    if(!empty($_REQUEST['app'])) {

      //
      $app = $_REQUEST['app'];

      //
      if(!empty($_REQUEST['domain'])) {

        $domain = $_REQUEST['domain'];

        // Send request to respective API endpoint...
        switch ($domain) {

          //
          case 'posts': require 'posts.php'; break;
          case 'signin': require 'signin.php'; break;
          case 'signup': require 'signup.php'; break;
          case 'subscribe': require 'subscribe.php'; break;
          case 'comments': require 'comments.php'; break;
          case 'categories': require 'categories.php'; break;
          case 'resetPassword': require 'resetPassword.php'; break;
          case 'follow': require 'follow.php'; break;
          case 'followers': require 'followers.php'; break;
          case 'followings': require 'followings.php'; break;
          case 'profile': require 'profile.php'; break;
          case 'search': require 'search.php'; break;
          case 'crowds': require 'crowds.php'; break;
          case 'threads': require 'threads.php'; break;
          case 'messages': require 'messages.php'; break;
          case 'likes': require 'likes.php'; break;
          case 'notifications': require 'notifications.php'; break;

          //
          default: header('Location: documentation.php?TRK=GET');

        }

      }

      else {

        $response = array(

          $t_api_key_version => $version,
          $t_api_key_code => 401,
          $t_api_key_status => $t_api_value_statusfailed,
          $t_api_key_message => $t_api_value_missingdomain,
          $t_api_key_status => $t_api_value_statusfailed,
          $t_api_key_event => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
          $t_api_key_process => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
          "request" => json_encode($_REQUEST)

        );

        header('Content-Type: application/json');

        echo json_encode($response);

      }

    }

    else {

      $response = array(

        $t_api_key_version => $version,
        $t_api_key_code => 401,
        $t_api_key_status => $t_api_value_statusdenied,
        $t_api_key_message => $t_api_value_missingapp,
        $t_api_key_event => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
        $t_api_key_process => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
        "request" => json_encode($_REQUEST)

      );

      header('Content-Type: application/json');

      echo json_encode($response);

    }

  }

  else {

    $response = array(

      $t_api_key_version => $version,
      $t_api_key_code => 401,
      $t_api_key_status => $t_api_value_statusdenied,
      $t_api_key_message => $t_api_value_missingtoken,
      $t_api_key_event => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
      $t_api_key_process => RAND()/*create_api_event(NULL,pathinfo(__FILE__, PATHINFO_FILENAME),$token)*/,
      "request" => json_encode($_REQUEST)

    );

    header('Content-Type: application/json');

    echo json_encode($response);

  }

?>
