<?php

  // Determine scope...
  if($_SERVER['HTTP_HOST'] == 'localhost') {

    define ('APP_NAME',     'Venny'); // app
    define ('APP_ENV',      'DEVELOPMENT'); // app environment
		define ('APP_ENV_ROOT', '/Library/WebServer/Documents/'); // app environment
    define ('APP_ENV_SRVR', 'http://localhost/'); // Site path
    define ('APP_ST_NAME',  'www.venny.io/');
    define ('APP_ST_ALIS',  APP_ST_NAME);
		define ('APP_ST_URL',   APP_ENV_SRVR . APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/app-venny-api/'); // include path
		define ('APP_ST_UPLD',  'views/assets/images/'); // directory for all image uploads
    define ('APP_ST_INI',   APP_ENV_ROOT . APP_ST_NAME . 'apps/'.'app-venny-api.ini'); // INI location
    $constantly = parse_ini_file(APP_ST_INI);
    define ('APP_ST_CDN',   'http://venny-web.imgix.net/'); // directory for all image uploads
		define ('APP_DB_HOST',  trim($constantly["development_db_host"])); // db hostname
    define ('APP_DB_PORT',  '3306'); // db port
    define ('APP_DB_NAME',  trim($constantly["development_db_name"])); // db name
    define ('APP_DB_USER',  trim($constantly["development_db_user"])); // db username
    define ('APP_DB_PASS',  trim($constantly["development_db_pass"])); // db password
    define ('APP_API_KEY',  trim($constantly["development_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@venny.io'); // app
    define ('APP_EM_HOST',  'email.venny.io'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["development_em_key"])); // Private API Key
    define ('APP_GG_TAG',   trim($constantly["development_google_analytics"])); // Google Tag Manager ID

  }

  elseif($_SERVER['HTTP_HOST'] == 'test.api.venny.io') {

    define ('APP_NAME',     'Venny'); // app environment
    define ('APP_ENV',      'STAGING'); // app environment
    define ('APP_ENV_SRVR', 'http://'); // Site path
    define ('APP_ENV_ROOT', '/app/'); // app environment
    define ('APP_ST_NAME',  'test.api.venny.io/');
    define ('APP_ST_ALIS',  '');
    define ('APP_ST_URL',   APP_ENV_SRVR.APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/app-venny-api/'); // include path
		define ('APP_ST_UPLD',  'views/assets/images/'); // directory for all image uploads
    define ('APP_ST_INI',   APP_ENV_ROOT . 'apps/' . 'app-venny-api.ini'); // INI location
    $constantly = parse_ini_file(APP_ST_INI);
    define ('APP_ST_CDN',   'http://venny-cdn.imgix.net/'); // directory for all image uploads
    define ('APP_DB_HOST',  trim($constantly["test_db_host"])); // Clear MySQL db hostname
    define ('APP_DB_PORT',  '3307'); // Clear MySQL db port
    define ('APP_DB_NAME',  trim($constantly["test_db_name"])); // Clear MySQL db name
    define ('APP_DB_USER',  trim($constantly["test_db_user"])); // Clear MySQL db username
    define ('APP_DB_PASS',  trim($constantly["test_db_pass"])); // Clear MySQL db password
    define ('APP_API_KEY',  trim($constantly["test_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@venny.io'); // app
    define ('APP_EM_HOST',  'email.venny.io'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["test_em_key"])); // Private API Key
    define ('APP_GG_TAG',   'UA-116723436-1'); // Google Tag Manager ID

  }

  else {

    define ('APP_NAME',     'Venny'); // app environment
    define ('APP_ENV',      'PRODUCTION'); // app environment
    define ('APP_ENV_SRVR', 'https://'); // Site path
    define ('APP_ENV_ROOT', '/app/'); // app environment
    define ('APP_ST_NAME',  'api.venny.io/');
    define ('APP_ST_ALIS',  '');
    define ('APP_ST_URL',   APP_ENV_SRVR . APP_ST_NAME); // host
    define ('APP_ST_INCL',  'apps/app-venny-api/'); // include path
    define ('APP_ST_UPLD',  'views/assets/images/'); // directory for all image uploads
    define ('APP_ST_INI',   APP_ENV_ROOT . 'apps/' . 'app-venny-api.ini'); // INI location
    $constantly = parse_ini_file(APP_ST_INI);
    define ('APP_ST_CDN',   'https://venny-web.imgix.net/'); // directory for all image uploads
    define ('APP_DB_HOST',  trim($constantly["prod_db_host"])); // Clear MySQL db hostname
    define ('APP_DB_PORT',  '3306'); // Clear MySQL db port
    define ('APP_DB_NAME',  trim($constantly["prod_db_name"])); // Clear MySQL db name
    define ('APP_DB_USER',  trim($constantly["prod_db_user"])); // Clear MySQL db username
    define ('APP_DB_PASS',  trim($constantly["prod_db_pass"])); // Clear MySQL db password
    define ('APP_API_KEY',  trim($constantly["prod_app_token"])); // API key used for all intraapp API calls
    define ('APP_EM_FROM',  'hello@venny.io'); // app
    define ('APP_EM_HOST',  'email.venny.io'); // Mailgun hostname
    define ('APP_EM_KEY',   trim($constantly["prod_em_key"])); // Private API Key
    define ('APP_GG_TAG',   'UA-116723436-2'); // Google Tag Manager ID

  }

?>
