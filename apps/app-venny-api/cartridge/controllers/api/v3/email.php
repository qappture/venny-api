<?php

  require 'autoload.php';
  use Mailgun\Mailgun;

  //
  class email {

    // Generate unique token for user when he got confirmation email message
    function generateToken($length) {

      // some characters
      $characters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";

      // get length of characters string
      $charactersLength= strlen($characters);

      $token = '';

      // generate random char from $characters every time until it is less than $charactersLength
      for ($i = 0; $i < $length; $i++) {
          $token .= $characters[rand(0, $charactersLength-1)];
      }

      return $token;

    }

    // Open confirmation template user gonna receive
    function emailTemplate($type = NULL) {

      //
      switch($type) {

        //
        case 'createUser':

          // open file
          $file = fopen("email/confirmationTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/confirmationTemplate.html"));

          // PUSH
          $notification = NULL;
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'createPerson':

          // open file
          $file = fopen("email/confirmationTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/confirmationTemplate.html"));

          // PUSH
          $notification = NULL;
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'resetPassword':

          // open file
          $file = fopen("email/resetPasswordTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/resetPasswordTemplate.html"));

          // PUSH
          $notification = "Your password was recently reset.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'newFollower':

          // open file
          $file = fopen("email/newFollowerTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/newFollowerTemplate.html"));

          // PUSH
          $notification = "{sender} is now following you.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'followResponse':

          // open file
          $file = fopen("email/newFollowerTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/newFollowerTemplate.html"));

          // PUSH
          $notification = NULL;
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'newThread':

          // open file
          $file = fopen("email/newThreadTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/newThreadTemplate.html"));

          $notification = "You have created the new thread {THREADNAME}.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'updateThread':

          // open file
          $file = fopen("email/updateThreadTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/updateThreadTemplate.html"));

          // PUSH
          $notification = NULL;
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'newMessage':

          // open file
          $file = fopen("email/newMessageTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/newMessageTemplate.html"));

          // PUSH
          $notification = "{author} just sent a new message.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'updateLike':

          // open file
          $file = fopen("email/updateLikeTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/updateLikeTemplate.html"));

          // PUSH
          $notification = NULL;
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'createLike':

          // open file
          $file = fopen("email/createLikeTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/createLikeTemplate.html"));

          // PUSH
          $notification = "{author} just liked your note.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'newCrowd':

          // open file
          $file = fopen("email/newCrowdTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/newCrowdTemplate.html"));

          // PUSH
          $notification = "The new crowd {title} has been created.";
          $object = NULL;
          $target = NULL;

          break;

        //
        case 'updateCrowd':

          // open file
          $file = fopen("email/updateCrowdTemplate.html", "r") or die("Unable to open file");

          // store content of file in $template var
          $template = fread($file, filesize("email/updateCrowdTemplate.html"));

          $notification = "The crowd {title} has been updated.";
          $object = NULL;
          $target = NULL;

          break;

        //
        default: header("Location: index.php");

      }

      fclose($file);

      $insert_query = mysqli_query($this->con, "INSERT INTO notifications (id,user_to,user_from,message,link,datetime,opened,viewed) VALUES (NULL, '$user_to', '$userLoggedIn', '$message', '$link', '$date_time', 'no', 'no')");

      return $template;

    }

    //
    public function getUnreadNotifications() {

      $userLoggedIn = $this->user_obj->getUsername();
      $query = mysqli_query($access->conn, "SELECT notification_ID AS id, notification_message AS message, notification_type AS type, notification_viewed AS viewed, notification_recipient AS recipient, notification_sender AS sender, object_ID AS object FROM notifications WHERE notification_viewed = 0 AND active = 1 AND app_ID = '" . $request['app'] . "' ");
      return mysqli_num_rows($query);

    }

    //
    public function getNotifications($data, $limit) {

      $page = $data['page'];
      $userLoggedIn = $this->user_obj->getUsername();
      $return_string = "";

      //
      if($page == 1) {

        $start = 0;

      }

      else {

        $start = ($page - 1) * $limit;

      }

      $set_viewed_query = mysqli_query($this->con, "UPDATE notifications SET notification_viewed = 1 WHERE notification_recipient = '" . $me . "' ");

      $query = mysqli_query($this->con, "SELECT notification_ID AS id, notification_message AS message, notification_type AS type, notification_viewed AS viewed, notification_recipient AS recipient, notification_sender AS sender, object_ID AS object FROM notifications WHERE notification_recipient = '" . $me . "' ORDER BY id DESC");

      //
      if(mysqli_num_rows($query) == 0) {

        $notification = "You have no notifications!";
        return;

      }

      $num_iterations = 0; //Number of messages checked
      $count = 1; //Number of messages posted

      //
      while($row = mysqli_fetch_array($query)) {

        //
        if($num_iterations++ < $start) {

          continue;

        }

        //
        if($count > $limit) {

          break;

        }

        else {

          $count++;

        }

        $user_from = $row['user_from'];

        $user_data_query = mysqli_query($this->con, "SELECT * FROM users WHERE username = '$user_from'");
        $user_data = mysqli_fetch_array($user_data_query);

        //Timeframe
        $date_time_now = date("Y-m-d H:i:s");
        $start_date = new DateTime($row['datetime']); //Time of post
        $end_date = new DateTime($date_time_now); //Current time
        $interval = $start_date->diff($end_date); //Difference between dates
        if($interval->y >= 1) {
          if($interval == 1)
            $time_message = $interval->y . " year ago"; //1 year ago
          else
            $time_message = $interval->y . " years ago"; //1+ year ago
        }
        else if ($interval->m >= 1) {
          if($interval->d == 0) {
            $days = " ago";
          }
          else if($interval->d == 1) {
            $days = $interval->d . " day ago";
          }
          else {
            $days = $interval->d . " days ago";
          }

          if($interval->m == 1) {
            $time_message = $interval->m . " month". $days;
          }
          else {
            $time_message = $interval->m . " months". $days;
          }

        }
        else if($interval->d >= 1) {
          if($interval->d == 1) {
            $time_message = "Yesterday";
          }
          else {
            $time_message = $interval->d . " days ago";
          }
        }
        else if($interval->h >= 1) {
          if($interval->h == 1) {
            $time_message = $interval->h . " hour ago";
          }
          else {
            $time_message = $interval->h . " hours ago";
          }
        }
        else if($interval->i >= 1) {
          if($interval->i == 1) {
            $time_message = $interval->i . " minute ago";
          }
          else {
            $time_message = $interval->i . " minutes ago";
          }
        }
        else {
          if($interval->s < 30) {
            $time_message = "Just now";
          }
          else {
            $time_message = $interval->s . " seconds ago";
          }
        }

        $opened = $row['opened'];
        $style = ($opened == 'no') ? "background-color: #DDEDFF;" : "";

        $return_string .= "

          <article class='discussion' style='" . $style . "'>

            <h6>Discussion</h6>

            <a href='" . $row['link'] . "'>
              <ul class='notification'>
                <li class='image'>
                  <img class='' src='" . $user_data['profile_pic'] . "' alt='@" . $user_data['username'] . "' />
                </li>
                <li class='message'>
                  " . $row['message'] . " " . $time_message . "
                </li>
              </ul>
            </a>

          </article>

        ";
      }

      //If posts were loaded
      if($count > $limit) {

        $return_string .= "

          <input type='hidden' class='nextPageDropdownData' value='" . ($page + 1) . "'>

          <input type='hidden' class='noMoreDropdownData' value='false'>

        ";

      }

      else {

        $return_string .= "

          <input type='hidden' class='noMoreDropdownData' value='true'>

          <p style='text-align: center;'>No more notifications to load!</p>

        ";

      }

      return $return_string;

    }

    // Send email with Mailgun
    function send_email($details) {

      /*
      $user_email,
      $user_first_name,
      $user_username,
      $user_validation_code,
      $email_subject,
      $email_message
      */

      // information of email
      $bcc = "notearise@gmail.com";
      $subject = $details["subject"];
      $to = $details["to"];
      $fromName = $details["organization"];
      $fromEmail = $details["sender"];
      $from = "The " . $fromName . " Team <". $fromEmail .">";
      $text = $details["subject"];
      $body = $details["body"];

      //echo print_r($details); exit;

      //$user_email
      //$user_first_name
      //$user_validation_code
      //$email_subject
      //$email_message

  		// Get time
  	  $user_current_time = date("Y-m-d h:i:sa");

      $domain = 'email.notearise.com';
      $params = array(
        'from' => $from,
        'to' => $to,
        'bcc' => $bcc,
        'subject' => $subject . $user_current_time,
        'text' => $text,
        'html' => $body,
      );

      //echo json_encode($params); exit;

      # First, instantiate the SDK with your API credentials
      $mg = Mailgun::create('key-47c1b17d3a8ad4bdddc7afc3c70b458f');

      # Now, compose and send your message.
      # $mg->messages()->send($domain, $params);
      $mg->messages()->send($domain,$params);

  		return true;

    }

  }

?>
