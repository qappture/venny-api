<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['author']) || empty($_REQUEST['thread'])) {

        $response['status'] = 400;
        $response['message'] = "You can not start a message at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // ID
      $request['id'] = $access->generate_ID('message');

      // Securing information and storing variables
      if(!empty($_REQUEST['author'])){$request['author']=clean($_REQUEST['author']);}else{$request['author']=NULL;}
      if(!empty($_REQUEST['body'])){$request['body']=clean($_REQUEST['body']);}else{$request['body']=NULL;}
      if(!empty($_REQUEST['images'])){$request['images']=clean($_REQUEST['images']);}else{$request['images']=NULL;} // No preview needed to begin a thread... only after the first message is sent
      if(!empty($_REQUEST['thread'])){$request['thread']=clean($_REQUEST['thread']);}else{$request['thread']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request); exit;

      // STEP 3. Insert user information
      $result = $access->createMessage($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        // get current registered user information and store in $user
        $results = $access->getMessages($request);

        //echo print_r($results);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully requested";
        $response['results'] = $results['messages'];
        $response['count'] = $results['count'];
        $response['html'] = $results['html'];

        //echo print_r($response);exit;
        //echo json_encode($response);exit;

        //echo $request['author'];exit;
        //echo print_r($request);exit;

        $thread['id']=$request['thread'];
        $thread['app']=$request['app'];

        $participants = $access->getThreads($thread);
        //echo print_r($participants);exit;
        //echo print_r($participants['threads'][0]['participants']);exit;
        $contributor_array = json_decode($participants['threads'][0]['participants']);
        $contributors = $contributor_array->contributors;
        //echo json_encode($contributors);exit;
        $notifications['sender'] = $request['author'];

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $notification = new notification();

        // Send notification for each individual contributor
        foreach($contributors as $item) {

          $notifications['recipient'] = $item;

          $getEmail = $access->getPerson('profile',$item);

          //echo print_r($getEmail);exit;
          //echo print_r($getEmail['email']);exit;
          $gotEmail = $getEmail['email'];
          //echo $gotEmail; exit;

          // refer emailing information
          $details = array();
          $details['subject'] = "New message from {$request['author']} on Notearise";
          $details['to'] = $gotEmail;
          $details['organization'] = "Notearise";
          $details['sender'] = "hello@notearise.com";

          //echo print_r($details);exit;

          $notifications['type'] = "newMessage";

          // access template file
          $email_template = $notification->emailTemplate($notifications['type']);
          //echo print_r($email_template);exit;

          // replace {token} from confirmationTemplate.html by $token and store all content in $email_template var
          $email_template = str_replace("{author}", $request['author'], $email_template);
          $email_template = str_replace("{organization}", APP_NAME, $email_template);
          $email_template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $email_template);

          //echo print_r($email_template);exit;

          // access template file
          $push_template = $notification->pushTemplate($notifications['type']);
          //echo print_r($email_template);exit;

          // replace {token} from confirmationTemplate.html by $token and store all content in $email_template var
          $push_template = str_replace("{author}", $request['author'], $push_template);
          //echo print_r($email_template);exit;

          $details['body'] = $email_template;
          $notifications['body'] = $push_template;
          $notifications['app'] = $request['app'];
          $notifications['subject'] = $details["subject"];

          //echo print_r($details);exit;
          //echo print_r($notifications);exit;

          $notification->send_email($details);
          //$notification->send_push($details);
          $access->createNotification($notifications);

        }

      }

      else {

        $response["status"] = 400;
        $response["message"] = "Could not create message at this time.";

      }

      break;

    // GET
    case 'GET':

      //
      if (empty($_REQUEST["thread"])) {

        $response["status"] = 400;
        $response["message"] = "Your message(s) can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id'] = htmlentities($_REQUEST["id"]);}else{$request['id']=NULL;}
      if(!empty($_REQUEST['author'])){$request['author'] = htmlentities($_REQUEST["author"]);}else{$request['author']=NULL;}
      if(!empty($_REQUEST['thread'])){$request['thread'] = htmlentities($_REQUEST["thread"]);}else{$request['thread']=NULL;}
      if(!empty($_REQUEST['body'])){$request['body'] = htmlentities($_REQUEST["body"]);}else{$request['body']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app'] = htmlentities($_REQUEST["app"]);}else{$request['app']=NULL;}

      //echo print_r($_GET); exit;

      //echo print_r($request['app']); exit;
      //echo print_r($request); exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getMessages($request);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response["status"] = 200;
        $response["message"] = "SUCCESS";
        $response["results"] = $result;
        $response["event"] = new_ID('event');
        $response["process"] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
