<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['sender']) || empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "You can not create a notification at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // ID
      if(empty($_REQUEST['id'])){
        $request['id'] = $access->generate_ID('notification');
      }

      // Securing information and storing variables
      if(!empty($_REQUEST['subject'])){$request['subject']=clean($_REQUEST['subject']);}else{$request['subject']=NULL;}
      if(!empty($_REQUEST['body'])){$request['body']=clean($_REQUEST['body']);}else{$request['body']=NULL;}
      if(!empty($_REQUEST['type'])){$request['type']=clean($_REQUEST['type']);}else{$request['type']=NULL;}
      if(!empty($_REQUEST['opened'])){$request['opened']=$_REQUEST['opened'];}else{$request['opened']=0;}
      if(!empty($_REQUEST['viewed'])){$request['viewed']=$_REQUEST['viewed'];}else{$request['viewed']=0;}
      if(!empty($_REQUEST['recipient'])){$request['recipient']=clean($_REQUEST['recipient']);}else{$request['recipient']=NULL;}
      if(!empty($_REQUEST['sender'])){$request['sender']=clean($_REQUEST['sender']);}else{$request['sender']=NULL;}
      if(!empty($_REQUEST['object'])){$request['object']=clean($_REQUEST['object']);}else{$request['object']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request); exit;

      // STEP 3. Insert user information
      $result = $access->createNotification($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        // get current registered user information and store in $user
        $results = $access->getNotifications($request);

        //echo print_r($results);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully requested";
        $response['results'] = $results['notifications'];
        $response['count'] = $results['count'];
        $response['html'] = $results['html'];

        //echo print_r($response);exit;
        //echo json_encode($response);exit;

        //echo $request["author"];exit;

        //
        $getEmail = $access->getPerson('profile',$request['recipient']);

        //echo print_r($getEmail);exit;

        $gotEmail = $getEmail['email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "You have created a new notification on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('newThread');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{author}", $request['author'], $template);
        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not create new notification at this time.";

      }

      break;

    // GET
    case 'GET':

      //
      if (empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your notification(s) can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id'] = htmlentities($_REQUEST['id']);}
      if(!empty($_REQUEST['author'])){$request['author'] = htmlentities($_REQUEST['author']);}
      if(!empty($_REQUEST['app'])){$request['app'] = htmlentities($_REQUEST['app']);}

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getNotifications($request);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        $response['results'] = $result;
        $response['event'] = new_ID('event');
        $response['process'] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      if (empty($_REQUEST['id']) || empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your notification can not be updated at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // Securing information and storing variables
      if(!empty($_REQUEST['opened'])){$request['opened']=clean($_REQUEST['opened']);}else{$request['opened']=NULL;}
      if(!empty($_REQUEST['viewed'])){$request['viewed']=clean($_REQUEST['viewed']);}else{$request['viewed']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request);
      //echo print_r($request['id']);exit;

      // STEP 3. Insert user information
      $result = $access->updateNotification($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        //echo $request['id']; exit;

        // get current registered user information and store in $user
        $result = $access->getNotifications($request);

        //header('Content-Type: application/json');
        //echo json_encode($result);exit;
        //echo print_r($result);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successful";
        $response['results'] = $result;

        //echo $result['threads'][0]['author'];exit;

        //
        $getEmail = $access->getPerson('profile',$result['notifications'][0]['author']);

        //echo var_dump($getEmail);exit;
        $gotEmail = $getEmail['person_email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "Your notification request was updated on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('updateThread');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{participant}", $result[0]["participant"], $template);
        $template = str_replace("{organization}", APP_NAME, $template);
        $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not update notification at this time.";

      }

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
