<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      $response = array();
      $response['status'] = 400;
      $response['message'] = "Your request to follow did not go through at this time.";

      header('Content-Type: application/json');
      echo json_encode($response);

      return;

      break;

    // GET POSTS
    case 'GET':

      //
      if (empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your follows can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['recipient'] = htmlentities($_REQUEST['recipient']);

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getFollowers($request);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        $response['results'] = $result;
        $response['event'] = new_ID('event');
        $response['process'] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      $response = array();
      $response['status'] = 400;
      $response['message'] = "Your request to follow did not go through at this time.";
      header('Content-Type: application/json');
      echo json_encode($response);

      break;

    //
    case 'DELETE':

      //
      $response = array();
      $response['status'] = 400;
      $response['message'] = "Your request to follow did not go through at this time.";
      header('Content-Type: application/json');
      echo json_encode($response);

      break;

    //
    default: header('Location: index.php');

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
