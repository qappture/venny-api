<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['author']) || empty($_REQUEST['title'])) {

        $response['status'] = 400;
        $response['message'] = "You CAN NOT start a group at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // ID
      $request['id'] = $access->generate_ID('crowd');

      // Securing information and storing variables
      if(!empty($_REQUEST['author'])){$request['author']=clean($_REQUEST['author']);}else{$request['author']=NULL;}
      if(!empty($_REQUEST['title'])){$request['title']=clean($_REQUEST['title']);}else{$request['title']=NULL;}
      if(!empty($_REQUEST['headline'])){$request['headline']=clean($_REQUEST['headline']);}else{$request['headline']=NULL;}
      if(!empty($_REQUEST['access'])){$request['access']=clean($_REQUEST['access']);}else{$request['access']=NULL;}
      if(!empty($_REQUEST['administrators'])){$request['administrators']=clean($_REQUEST['administrators']);}else{$request['administrators']=NULL;}
      if(!empty($_REQUEST['contributors'])){$request['contributors']=clean($_REQUEST['contributors']);}else{$request['contributors']=NULL;}
      if(!empty($_REQUEST['images'])){$request['images']=clean($_REQUEST['images']);}else{$request['images']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request); exit;

      // STEP 3. Insert user information
      $result = $access->createCrowd($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        // get current registered user information and store in $user
        $results = $access->getCrowds($request);

        //echo print_r($results);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully requested";
        $response['results'] = $results['crowds'];
        $response['count'] = $results['count'];
        $response['html'] = $results['html'];

        //echo print_r($response);exit;
        //echo json_encode($response);exit;

        //echo $request["author"];exit;

        //
        $getEmail = $access->getPerson('profile',$request["author"]);

        //echo print_r($getEmail);exit;

        $gotEmail = $getEmail['email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "You have created a new crowd on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('newCrowd');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{author}", $request["author"], $template);
        $template = str_replace("{organization}", APP_NAME, $template);
        $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not create crowd at this time.";

      }

      break;

    // GET
    case 'GET':

      //
      if (empty($_REQUEST['author'])) {

        $response["status"] = 400;
        $response["message"] = "Your thread(s) can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['id'] = htmlentities($_REQUEST['id']);
      $request['author'] = htmlentities($_REQUEST['author']);
      $request['app'] = htmlentities($_REQUEST['app']);

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getCrowds($request);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        $response['results'] = $result;
        $response['event'] = new_ID('event');
        $response['process'] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      if (empty($_REQUEST['id']) || empty($_REQUEST['author'])) {

        $response['status'] = 400;
        $response['message'] = "Your crowd can not be updated at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id']=clean($_REQUEST['id']);}else{$request['id']=NULL;}
      if(!empty($_REQUEST['headline'])){$request['headline']=clean($_REQUEST['headline']);}else{$request['headline']=NULL;}
      if(!empty($_REQUEST['title'])){$request['title']=clean($_REQUEST['title']);}else{$request['title']=NULL;}
      if(!empty($_REQUEST['access'])){$request['access']=clean($_REQUEST['access']);}else{$request['access']=NULL;}
      if(!empty($_REQUEST['administrators'])){$request['administrators']=clean($_REQUEST['administrators']);}else{$request['administrators']=NULL;} // No preview needed to begin a thread... only after the first message is sent
      if(!empty($_REQUEST['contributors'])){$request['contributors']=clean($_REQUEST['contributors']);}else{$request['contributors']=NULL;}
      if(!empty($_REQUEST['images'])){$request['images']=clean($_REQUEST['images']);}else{$request['images']=NULL;}
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request);
      //echo print_r($request['id']);exit;

      // STEP 3. Insert user information
      $result = $access->updateCrowd($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        //echo $request['id']; exit;

        // get current registered user information and store in $user
        $result = $access->getCrowds($request);

        //header('Content-Type: application/json');
        //echo json_encode($result);exit;
        //echo print_r($result);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successful";
        $response['results'] = $result;

        //echo $result['threads'][0]['author'];exit;

        //
        $getEmail = $access->getPerson('profile',$result['threads'][0]['author']);

        //echo var_dump($getEmail);exit;
        $gotEmail = $getEmail['person_email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "Your crowd request was updated on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('updateCrowd');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{author}", $result[0]["author"], $template);
        //echo print_r($template);exit;

        $details["body"] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response["status"] = 400;
        $response["message"] = "Could not update thread at this time.";

      }

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
