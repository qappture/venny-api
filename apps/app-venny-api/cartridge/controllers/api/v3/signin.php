<?php

  //
  require 'core.php';

  // STEP 1. Declare variables to store user information
  // array will store all informaiton
  if (empty($_REQUEST['username']) || empty($_REQUEST['password'])) {

    $response['status'] = 400;
    $response['message'] = "Missing required information";
    return;

  }

  //echo $_REQUEST['username'];
  //echo $_REQUEST['password'];
  //exit;

  $request = array();

  // Secure parsing of username and password variables
  //$login = htmlentities($_REQUEST["login"]);
  $request['username'] = htmlentities($_REQUEST['username']);
  $request['password'] = htmlentities($_REQUEST['password']);
  //echo print_r($request);
  //exit;
  //
  // STEP 3. Get user
  //CHANGE ME $sql = "SELECT password,id,username,active,person_ID,user_closed FROM users WHERE ( username = '" . escape($login) . "' OR email = '" . escape($login) . "' ) LIMIT 1";

    //
    /*
    if (filter_var($login, FILTER_VALIDATE_EMAIL)) {

      $user = $access->getUserbyEmail($login);

    }

    else {

      $user = $access->getUserbyUsername($login);

    }
    */

  //echo print_r($request); exit;
  $user = $access->getUser('username',$request['username']);

  //echo print_r($user) . __FUNCTION__; exit;

  if (empty($user)) {

    $response["status"] = 403;
    $response["message"] = "User is not found";
    header('Content-Type: application/json');
    echo json_encode($response);
    return;

  }

  //echo print_r($user);

  // STEP 4. Check validity of entered user information and information from database
  // 4.1 Get password and salt from database
  $secured_password = $user["password"];
  $salt = $user["salt"];

  $profile = $access->getProfile('user',$user['id']);

  //echo print_r($profile);exit;

  /*
  echo $request['password'] . "\n";
  echo $secured_password . "\n";
  echo $salt . "\n";
  echo sha1($request['password'] . $salt);
  exit;
  */

  // 4.2 Check if entered passwords match with password from database
  if ($secured_password == sha1($request['password'] . $salt)) {

    $update_last_login = $access->updateLastLogin($user['id']);
    //echo print_r($update_last_login);exit;

    $response["status"] = 200;
    $response["message"] = "Logged in successfully";
    $response["id"] = $user["id"];
    $response["username"] = $user["alias"];
    $response["lastlogin"] = $user["lastlogin"];
    $response["status"] = $user["status"];
    $response["validation"] = $user["validation"];
    $response["welcome"] = $user["welcome"];
    $response["profile"] = array(
      "id" => $profile['id'],
      "images" => $profile['images'],
      "bio" => $profile['bio'],
      "headline" => $profile['headline'],
      "access" => $profile['access']
    );
    header('Content-Type: application/json');
    echo json_encode($response);
    return;

  }

  else {

    $response["status"] = 403;
    $response["message"] = "Passwords do not match";
    header('Content-Type: application/json');
    echo json_encode($response);
    return;

  }

  // STEP 5. Close connection after registration
  $access->disconnect();

  // STEP 6. JSON data
  header('Content-Type: application/json');
  echo json_encode($response);

?>
