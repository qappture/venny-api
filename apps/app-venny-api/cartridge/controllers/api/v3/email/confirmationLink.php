<?php

  //
  require 'controllers/api/v3/functions.php';
  require 'controllers/api/v3/environment.php';
  require 'controllers/api/v3/properties.php';
  require 'controllers/api/v3/core.php';


  // STEP 1. Chech required & passed inf
  if (empty($_GET["token"])) {
      echo 'Missing required information';
      return;
  }

  $token = htmlentities($_GET["token"]);

  // STEP 3. Get id of user
  // store in $id the result of func
  $id = $access->getToken($token);

  //
  if (empty($id["person_ID"])) {

    echo 'User with this token is not found';
    return;

  }

  // STEP 4. Change status of confirmation and delete token
  // assign result of func executed to $result var
  $result = $access->emailConfirmationStatus(1, $id["person_ID"]);

  if ($result) {

    // 4.1 Delete token from 'emailTokens' table of db in mysql
    $access->deleteToken($token);
    echo 'Thank You! Your email is now confirmed';

  }

  // STEP 5. Close connection
  $access->disconnect();

?>
