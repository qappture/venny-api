<?php
  /**
   * Created by PhpStorm.
   * User: macbookpro
   * Date: 11.06.16
   * Time: 12:28
   */

  // SECOND LOAD OF PAGE

  // STEP 1. Check passed information
  if (!empty($_POST["password_1"]) && !empty($_POST["password_2"]) && !empty($_POST["token"])) {

    $password_1 = htmlentities($_POST["password_1"]);
    $password_2 = htmlentities($_POST["password_2"]);
    $token = htmlentities($_POST["token"]);

    echo $password_1 . $password_2 . $token; exit;

    // STEP 2. Check, do passwords match or not
    if ($password_1 == $password_2) {

      //
      require 'controllers/api/v3/environment.php';
      require 'controllers/api/v3/functions.php';
      require 'controllers/api/v3/properties.php';
      require 'controllers/api/v3/core.php';

      // STEP 4. Get user id via user token we passed to this file
      $user = $access->getUser('token', $token);

      //echo print_r($user); exit;
      //echo print_r($token); exit;

      // STEP 5. Update database
      if (!empty($user)) {

        // 5.1 Generate secured password
        $salt = openssl_random_pseudo_bytes(30);
        $secured_password = sha1($password_1 . $salt);

        //echo $secured_password . $password_1 . $salt; exit;

        // 5.2 Update user password
        $result = $access->updatePassword($user["id"], $secured_password, $salt);

        //echo $result; exit;

        if ($result) {

          // 5.3 Delete unique token
          $access->deleteToken($token);
          $message = "Successfully created new password";

          header("Location:didResetPassword.php?message=" . $message);

        }

        else {

          echo 'User ID is empty';

        }

      }

    }

    else {

      $message = "Passwords do not match";

    }

  }

?>

<!--FIRST LOAD-->
<html>
       <head>
           <!--Title-->
           <title>Create new password</title>

           <!--CSS Style-->
           <style>

               .password_field
               {
                   margin: 10px;
               }

               .button
               {
                   margin: 10px;
               }

           </style>

       </head>


        <body>
            <h1>Create new password</h1>

            <?php
                if (!empty($message)) {
                    echo "</br>" . $message . "</br>";
                }
            ?>

        <!--Forms-->
        <form method="POST" action="<?php $_SERVER['PHP_SELF'];?>">
        <div><input type="password" name="password_1" placeholder="New password:" class="password_field"/></div>
        <div><input type="password" name="password_2" placeholder="Repeat password:" class="password_field"/></div>
        <div><input type="submit" value="Save" class="button"/></div>
        <input type="hidden" value="<?php echo $_GET['token'];?>" name="token">
        </form>

        </body>

</html>
