<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['author']) || empty($_REQUEST['participants'])) {

        $response['status'] = 400;
        $response['message'] = "You can not start a message thread at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // ID
      $request['id'] = $access->generate_ID('thread');

      // Securing information and storing variables
      if(!empty($_REQUEST['author'])){$request['author']=clean($_REQUEST['author']);}else{$request['author']=NULL;}
      if(!empty($_REQUEST['title'])){$request['title']=clean($_REQUEST['title']);}else{$request['title']=NULL;}
      if(!empty($_REQUEST['participants'])){$request['participants']=clean($_REQUEST['participants']);}else{$request['participants']=NULL;}
      //if(!empty($_REQUEST['preview'])){$request['preview']=clean($_REQUEST['preview']);}else{$request['preview']=NULL;} // No preview needed to begin a thread... only after the first message is sent
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request); exit;

      // STEP 3. Insert user information
      $result = $access->createThread($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        // get current registered user information and store in $user
        $results = $access->getThreads($request);

        //echo print_r($results);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully requested";
        $response['results'] = $results['threads'];
        $response['count'] = $results['count'];
        $response['html'] = $results['html'];

        //echo print_r($response);exit;
        //echo json_encode($response);exit;

        //echo $request["author"];exit;

        //
        $getEmail = $access->getPerson('profile',$request['author']);

        //echo print_r($getEmail);exit;

        $gotEmail = $getEmail['email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "You have created a new thread on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('newThread');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{author}", $request['author'], $template);
        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not follow at this time.";

      }

      break;

    // GET
    case 'GET':

      //
      if (empty($_REQUEST['author'])) {

        $response["status"] = 400;
        $response["message"] = "Your thread(s) can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id'] = htmlentities($_REQUEST['id']);}
      if(!empty($_REQUEST['author'])){$request['author'] = htmlentities($_REQUEST['author']);}
      if(!empty($_REQUEST['app'])){$request['app'] = htmlentities($_REQUEST['app']);}

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getThreads($request);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response["status"] = 200;
        $response["message"] = "SUCCESS";
        $response["results"] = $result;
        $response["event"] = new_ID('event');
        $response["process"] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      if (empty($_REQUEST["id"]) || empty($_REQUEST['requestor'])) {

        $response["status"] = 400;
        $response["message"] = "Your thread can not be updated at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id']=clean($_REQUEST['id']);}else{$request['id']=NULL;}
      if(!empty($_REQUEST['requestor'])){$request['requestor']=clean($_REQUEST['requestor']);}else{$request['requestor']=NULL;}
      if(!empty($_REQUEST['title'])){$request['title']=clean($_REQUEST['title']);}else{$request['title']=NULL;}
      if(!empty($_REQUEST['participants'])){$request['participants']=clean($_REQUEST['participants']);}else{$request['participants']=NULL;}
      if(!empty($_REQUEST['preview'])){$request['preview']=clean($_REQUEST['preview']);}else{$request['preview']=NULL;} // No preview needed to begin a thread... only after the first message is sent
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request);
      //echo print_r($request['id']);exit;

      // STEP 3. Insert user information
      $result = $access->updateThread($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        //echo $request['id']; exit;

        // get current registered user information and store in $user
        $result = $access->getThreads($request);

        //header('Content-Type: application/json');
        //echo json_encode($result);exit;
        //echo print_r($result);exit;

        // declare information to feedback to user of App as json
        $response["status"] = 200;
        $response["message"] = "Successful";
        $response["results"] = $result;

        //echo $result['threads'][0]['author'];exit;

        //
        $getEmail = $access->getPerson('profile',$result['threads'][0]['author']);

        //echo var_dump($getEmail);exit;
        $gotEmail = $getEmail['person_email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details["subject"] = "Your thread request was updated on Notearise";
        $details["to"] = $gotEmail;
        $details["organization"] = "Notearise";
        $details["sender"] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('updateThread');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{participant}", $result[0]["participant"], $template);
        $template = str_replace("{organization}", APP_NAME, $template);
        $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

        //echo print_r($template);exit;

        $details["body"] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response["status"] = 400;
        $response["message"] = "Could not update thread at this time.";

      }

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
