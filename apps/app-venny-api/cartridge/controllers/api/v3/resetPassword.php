<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 11.06.16
 * Time: 11:41
 */



// STEP 1. Get information passed to this file
if (empty($_REQUEST["email"])) {

  $response["message"] = "Missing required information";
  echo json_encode($response);
  return;

}

// Secure way to store information in $email var
$email = htmlentities($_REQUEST["email"]);

  //
require 'controllers/api/v3/core.php';

// STEP 3. Check if email is found in db as registered email address
// store all result of func in $user var
$user = $access->getUser('email',$email);
//echo print_r($user); exit;

// if there is any information stoting in $user variable
if (empty($user)) {

  $response["message"] = "Email not found";
  echo json_encode($response);
  return;

}

// STEP 4. Emailing
// include email.php
require 'notification.php';

// store all class in $email var
$send_email = new notification();

// Generate unique string token assoc with user in our db
$token = $send_email->generateToken(20);

// Store unique token in our db
$access->saveToken('passwordTokens', $user['person'], $token, $_REQUEST["app"]);

// Prepare email messsage
$details = array();
$details["subject"] = "Password reset request on Notearise";
$details["to"] = $email;
$details["organization"] = "Notearise";
$details["sender"] = "hello@notearise.com";

// Load html template
$email_template = $send_email->emailTemplate('resetPassword');
//echo print_r($template);exit;

$email_template = str_replace("{token}", $token, $email_template);
$email_template = str_replace("{organization}", APP_NAME, $email_template);
$email_template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $email_template);

$details["body"] = $email_template;

//echo print_r($details['body']);exit;

// Send email to user
$send_email->send_email($details);

// STEP 5. Return message to mobile app
$response["email"] = $email;
$response["message"] = "We have sent you email to reset password";

header('Content-Type: application/json');
echo json_encode($response);

// STEP 6. Close connection
$access->disconnect();

?>
