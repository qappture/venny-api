<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //

      break;

    // GET POSTS
    case 'GET':

      //
      if (empty($_REQUEST["sender"]) || empty($_REQUEST["query"]) || empty($_REQUEST["more"])) {

        $response["status"] = 400;
        $response["message"] = "Your search will not return results.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['sender'] = htmlentities($_REQUEST["sender"]);
      $request['query'] = mysqli_real_escape_string($access->conn,$_REQUEST["query"]);
      $request['more'] = htmlentities($_REQUEST["more"]);

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $results = $access->getSearch($request);

      //echo print_r($results); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($results)) {

        $response["status"] = 200;
        $response["message"] = "SUCCESS";
        $response["results"] = $results;
        $response["event"] = new_ID('event');
        $response["process"] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
