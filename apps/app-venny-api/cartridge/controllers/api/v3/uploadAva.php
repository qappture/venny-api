<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 12.06.16
 * Time: 15:38
 */


// CHAPTER 1. UPLOADING FILE
// STEP 1. Check passed data to this php file
if (empty($_REQUEST["id"])) {
    $returnArray["message"] = "Missing required information";
    return;
}

// pass POST via htmlencrypt and assign to $id
$id = htmlentities($_REQUEST["id"]);



// STEP 2. Create a folder for user with name of his ID
$folder = "/Library/WebServer/Documents/www.venny.io/images/" . $id;

// if it doesn't exist
if (!file_exists($folder)) {
    mkdir($folder, 0777, true);
}



// STEP 3. Move uploaded file
$folder = $folder . "/" . basename($_FILES["file"]["name"]);

if (move_uploaded_file($_FILES["file"]["tmp_name"], $folder)) {
    $returnArray["status"] = "200";
    $returnArray["message"] = "The file has been uploaded";
} else {
    $returnArray["status"] = "300";
    $returnArray["message"] = "Error while uploading";
}


//
require 'core.php';

// STEP 5. Save path to uploaded file in db
$path = "http://localhost/www.venny.io/images/" . $id . "/image.jpg";
$access->updateAvaPath($path, $id);



// STEP 6. Get new user information after updating
$user = $access->selectUserViaID($id);

$returnArray["id"] = $user["id"];
$returnArray["username"] = $user["username"];
$returnArray["fullname"] = $user["fullname"];
$returnArray["email"] = $user["email"];
$returnArray["ava"] = $user["ava"];



// STEP 7. Close connection
$access->disconnect();


// STEP 8. Feedback array to app user
echo json_encode($returnArray);


?>
