<?php

  // Declare classe to access this php file
  class access {

    // connection global variables
    var $host = null;
    var $user = null;
    var $pass = null;
    var $name = null;
    var $conn = null;
    var $result = null;

    // constructing class
    function __construct($dbhost, $dbuser, $dbpass, $dbname) {

        $this->host = $dbhost;
        $this->user = $dbuser;
        $this->pass = $dbpass;
        $this->name = $dbname;

    }

    // connection function
    public function connect() {

      // This is important
      date_default_timezone_set('America/Chicago');

      // establish connection and store it in $conn
      $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->name);

      // if error
      if (mysqli_connect_errno()) {
          echo 'Could not connect to database';
      }

      // support all languages
      $this->conn->set_charset("utf8");

    }

    // disconnection function
    public function disconnect() {

      //
      if ($this->conn != null) {

          $this->conn->close();

      }

    }

    // Insert user details
    public function createPerson($request) {

      //echo print_r($request);exit;

      // Defaults
      $request['entitlements'] = "[]";

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO persons SET person_ID=?, person_first_name=?, person_last_name=?, person_email=?, person_phone=?, person_entitlements=?, app_ID=?";

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      // if error
      if (!$statement) {

        throw new Exception($statement->error);

      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("sssssss", $request['person'], $request['first_name'], $request['last_name'], $request['email'], $request['phone'], $request['entitlements'], $request['app']);

      //echo print_r($statement); exit;

      $response = $statement->execute();

      //echo $response; exit;

      return $response;

    }

    // Select person information
    public function getPerson($key, $value, $abbreviated = false) {

      //
      $response = array();

      //
      switch($key) {

        //
        case 'ID':

          //
          $sql = "SELECT person_ID as id,person_first_name as first_name,person_last_name as last_name,person_email as email,person_phone as phone,person_entitlements as entitlements,app_ID as app,time_finished as timestamp,active FROM persons WHERE person_ID = '" . $value . "' AND active = 1 LIMIT 1";
          break;

        //
        case 'email':

          //
          if($abbreviated==false){
            $segment = ", person_first_name as first_name, person_last_name as last_name, person_phone as phone, person_entitlements as entitlements, app_ID as app, time_finished as timestamp, active";
          }
          else {
            $segment = "";
          }
          //echo $segment; exit;

          $sql = "SELECT person_ID as id, person_email as email {$segment} FROM persons WHERE person_email = '" . $value . "' AND active = 1 LIMIT 1";
          //echo $sql;exit;
          break;

        //
        case 'profile':

          //
          $sql = "SELECT persons.person_ID AS id, persons.person_email AS email, persons.person_first_name AS first_name, persons.person_last_name AS last_name, users.user_ID AS user, users.user_alias AS alias, profiles.profile_ID AS profile, profiles.profile_access AS access
            FROM persons, users, profiles
            WHERE profiles.profile_ID = '{$value}' AND profiles.user_ID = users.user_ID AND users.person_ID = persons.person_ID ";
          break;

      }

      // assign result we got from $sql to $result var
      $result = $this->conn->query($sql);

      // if we have at least 1 result returned
      if ($result != null && (mysqli_num_rows($result) >= 1 )) {

        // assign results we got to $row as associative array
        $row = $result->fetch_array(MYSQLI_ASSOC);

        //
        if (!empty($row)) {

            $response = $row;

        }

      }

      return $response;

    }

    // Insert user details
    public function createUser($request) {

      //echo print_r($request);exit;

      // Defaults
      $status = 'active';

      // sql command
      $sql = "INSERT INTO users SET user_ID=?, user_alias=?, user_password=?, user_status=?, user_salt=?, person_ID=?, app_ID=?";

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("sssssss", $request['user'], $request['username'], $request['secured_password'], $request['status'], $request['salt'], $request['person'], $request['app']);

      $response = $statement->execute();

      //echo $response; exit;

      return $response;

    }

    // Insert user details
    public function registerUser($username, $password, $salt, $email, $fullname) {

        // sql command
        $sql = "INSERT INTO profiles SET username=?, password=?, salt=?, email=?, fullname=?";

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        $statement->bind_param("sssss", $username, $password, $salt, $email, $fullname);

        $response = $statement->execute();

        return $response;

    }

    // Select user information
    public function selectUser($user_ID) {

      $response = array();

      // sql command
      $sql = "SELECT user_ID,user_status,user_lastlogin,user_validation,user_welcome FROM users WHERE user_ID='".$user_ID."' AND active = 1";

      // assign result we got from $sql to $result var
      $result = $this->conn->query($sql);

      // if we have at least 1 result returned
      if ($result != null && (mysqli_num_rows($result) >= 1 )) {

        // assign results we got to $row as associative array
        $row = $result->fetch_array(MYSQLI_ASSOC);

        if (!empty($row)) {

          $response = $row;

        }

      }

      return $response;

    }

    // Get full user information
    public function getUser($key, $value, $abbreviated = NULL) {

      //echo $key . $value . $abbreviated . __FUNCTION__; exit;

      // declare array to store all information we got
      $response = array();

      //
      switch($key) {

        //
        case 'ID':

          //
          $sql = "SELECT user_ID as id,user_alias as alias,user_lastlogin as lastlogin,user_status as status,user_validation as validation,user_welcome as welcome,person_ID as person FROM users WHERE user_ID='" . $value . "' AND active = 1 LIMIT 1";
          break;

        //
        case 'username':

          // sql statement
          $sql = "SELECT user_ID as id,user_alias as alias,user_password as password,user_salt as salt,user_lastlogin as lastlogin,user_status as status,user_validation as validation,user_welcome as welcome, person_ID as person FROM users WHERE user_alias = '" . $value . "' AND active = 1 LIMIT 1";
          //echo $sql; exit;
          break;

        //
        case 'email':

          //echo $value;exit;

          $person = $this->getPerson('email', $value);

          //echo print_r($person['id']); exit;

          // sql command
          $sql = "SELECT user_ID as id,user_alias as alias,user_lastlogin as lastlogin,user_status as status,user_validation as validation,user_welcome as welcome,person_ID as person FROM users WHERE person_ID = '".$person['id']."'";

          break;

        //
        case 'token':

          //
          $sql = "SELECT person_ID as id FROM tokens WHERE token_body='" . $value . "' AND active = 1 LIMIT 1";
          break;

      }

      //echo($sql); exit;

      // execute / query $sql
      $result = $this->conn->query($sql);

      //echo json_encode($result); exit;

      // if we got some result
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

        // assign result to $row as assoc array
        $row = $result->fetch_array(MYSQLI_ASSOC);

        // if assigned to $row. Assign everything $returnArray
        if (!empty($row)) {

          $response = $row;

        }

      }

      //echo json_encode($response); exit;

      return $response;

    }

    // Insert user details
    public function createProfile($request) {

      //echo print_r($request);exit;

      // Defaults
      $request['images'] = "[]";
      $request['bio'] = "Hi, I'm {$request['first_name']}...";
      $request['headline'] = "This is {$request['first_name']}'s headline...";
      $request['access'] = 2;

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO profiles SET profile_ID=?, profile_images=?, profile_bio=?, profile_headline=?, profile_access=?, user_ID=?, app_ID=?";

      //echo ($sql);exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      //echo print_r($statement);exit;

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("ssssiss", $request['profile'], $request['images'], $request['bio'], $request['headline'], $request['access'], $request['user'], $request['app']);

      $response = $statement->execute();

      return $response;

    }

    // Select user information
    public function selectProfile($profile_ID) {

      $response = array();

      // sql command
      $sql = "SELECT profile_bio,profile_headline,profile_access,profile_images FROM profiles WHERE profile_ID='".$profile_ID."' AND active = 1";

      // assign result we got from $sql to $result var
      $result = $this->conn->query($sql);

      // if we have at least 1 result returned
      if ($result != null && (mysqli_num_rows($result) >= 1 )) {

          // assign results we got to $row as associative array
          $row = $result->fetch_array(MYSQLI_ASSOC);

          if (!empty($row)) {
              $response = $row;
          }

      }

      return $response;

    }

    // Select user information
    public function getSearch($request) {

      //echo print_r($request); exit;

      $query = $request['query'];

      $sql = "SELECT post_ID as id, post_detail as detail, post_topic as topic, post_place as place, post_access as access, author_ID as author FROM posts WHERE ";
      $sql .= "active = 1 AND ";

      //
      if($request['more']=='yes') {
        $sql .= "post_access = 2 AND ";
        //echo $sql; exit;
      } else {
        $sql .= "author_ID = '{$request['sender']}' AND ";
        //echo $sql; exit;
      }

      $response = array();

      $terms = explode(" ", $request['query']);
      $i=1;
      foreach($terms as $each) {

        if($i == 1) {
          $sql .= "post_detail LIKE '%$each%' ";
        } else {
          $sql .= "OR post_detail LIKE '%$each%' ";
        }
        $i++;
      }

      $sql .= "OR post_topic LIKE '%$query%' ";
      $sql .= "OR post_place LIKE '%$query%' ";

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

          $response['search'][] = $row;

      }

      return $response;

    }

    // Get full user information
    public function getProfile($key, $value, $abbreviated = NULL) {

      //echo $key . $value . $abbreviated; exit;

      // declare array to store all information we got
      $response = array();

      $columns = "profile_ID as id, profile_images as images, profile_bio as bio, profile_headline as headline, profile_access as access, user_ID as user";

      //
      switch($key) {

        //
        case 'ID':

          //
          $sql = "SELECT {$columns} FROM profiles WHERE profile_ID='" . $value . "' AND active = 1 LIMIT 1";
          //echo $sql;exit;
          break;

        //

        case 'user':

          // sql statement
          $sql = "SELECT {$columns} FROM profiles WHERE user_ID = '" . $value . "' AND active = 1 LIMIT 1";

          //echo $sql;exit;

          break;

      }

      // echo($sql); exit;

      // execute / query $sql
      $result = $this->conn->query($sql);

      //echo json_encode($result); exit;

      // if we got some result
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

        // assign result to $row as assoc array
        $row = $result->fetch_array(MYSQLI_ASSOC);

        // if assigned to $row. Assign everything $returnArray
        if (!empty($row)) {

          $response = $row;

        }

      }

      //echo json_encode($response); exit;

      return $response;

    }

    // Insert user details
    public function createThread($request) {

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO threads SET thread_ID=?, thread_author=?, thread_title=?, thread_participants=?, app_ID=?";

      //echo ($sql);exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      //echo print_r($statement);exit;

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("sssss", $request['id'], $request['author'], $request['title'], $request['participants'], $request['app']);

      $response = $statement->execute();

      return $response;

    }

    // Select user information
    public function getThreads($request) {

      //echo print_r($request);exit;

      //
      if(!empty($request['id'])) {

        $conditions = "AND thread_ID = '" . $request['id'] . "' ";
        $limit = "LIMIT 1 ";

      }

      else {

        $conditions = "";
        $limit = "";

        if(isset($request['author'])){$author=$request['author'];$conditions.="AND thread_author = '".$author."' ";}else{$conditions.="";}
        if(isset($request['title'])){$title=$request['title'];$conditions.="AND thread_title = '".$title."' ";}else{$conditions.="";}
        if(isset($request['participants'])){$participants=$request['participants'];$conditions.="AND thread_participants = '".$participants."' ";}else{$conditions.="";}

      }

      $sql =

        "SELECT
          thread_ID as id,
          thread_author as author,
          thread_title as title,
          thread_participants as participants,
          thread_preview as preview
        FROM threads ";

      $sql .= "WHERE active = 1 ";
      $sql .= "AND app_ID = '" . $request['app'] . "' ";
      $sql .= $conditions;
      $sql .= $limit;

      //echo $sql;exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      //echo var_dump($result); exit;

      $response = array();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['threads'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      //echo print_r($response);exit;

      return $response;

    }

    // Insert user details
    public function updateThread($request) {

      $object = $this->getThreads($request);

      //echo print_r($object);exit;
      //echo $request['requestor'];
      //echo $object['threads'][0]['author'];
      //exit;

      //
      if($request['requestor']==$object['threads'][0]['author'] && !empty($request['title'])) {

        // sql command
        $sql = "UPDATE threads SET thread_title=? WHERE thread_ID=? AND app_ID=?";

        //echo ($sql);exit;

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        //echo print_r($statement);exit;

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        $statement->bind_param("sss", $request['title'], $request['id'], $request['app']);

      }

      else if(!empty($request['preview'])) {

        // sql command
        $sql = "UPDATE threads SET thread_preview=? WHERE thread_ID=? AND app_ID=?";

        //echo ($sql);exit;

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        //echo print_r($statement);exit;

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        $statement->bind_param("sss", $request['preview'], $request['id'], $request['app']);

      }

      //echo print_r($request);exit;

      $response = $statement->execute();

      /*
      $json = ' {
        "type": "donut",
        "name": "Cake",
        "toppings": "[{ \"type\": \"Glazed\" }, { \"type\": \"Maple\" }]"
      }';

      $yummy = json_decode($json);
      $toppings = json_decode($yummy->toppings);

      echo $toppings[0]->type; //Glazed
      exit;
      */

      $sql = "SELECT JSON_EXTRACT(thread_participants, '$.contributors') AS contributors FROM threads WHERE thread_ID = '".$request['id']."' ";
      $query = mysqli_query($this->conn,$sql);
      $row = mysqli_fetch_array($query);
      $rows = $row;
      //echo $folks->contributors; exit;
      $contributors = json_decode($rows['contributors']);

      header('Content-Type: application/json');
      //echo print_r($folks);exit;
      //echo print_r($row);exit;
      //echo print_r($row[0]);exit;
      //echo print_r($row['contributors']);exit;
      foreach ($contributors as $item) {
        echo ($item);
      }
      exit;

      //echo $response; exit;

      return $response;

    }

    // Insert user details
    public function createNotification($request) {

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO notifications SET notification_ID=?,notification_subject=?,notification_body=?,notification_type=?,notification_opened=?,notification_viewed=?,notification_recipient=?,notification_sender=?, object_ID=?, app_ID=? ";

      //echo ($sql);exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      //echo print_r($statement);exit;

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("ssssiissss", $request['id'], $request['subject'], $request['body'], $request['type'], $request['opened'], $request['viewed'], $request['recipient'], $request['sender'], $request['object'], $request['app']);

      $response = $statement->execute();

      return $response;

    }

    // Select user information
    public function getNotifications($request) {

      //echo print_r($request);exit;

      //
      if(!empty($request['id'])) {

        $conditions = "AND notification_ID = '" . $request['id'] . "' ";
        $limit = "LIMIT 1 ";

      }

      else {

        $conditions = "";
        $limit = "";

        if(isset($request['subject'])){$subject=$request['subject'];$conditions.="AND notification_subject = '".$subject."' ";}else{$conditions.="";}
        if(isset($request['body'])){$body=$request['body'];$conditions.="AND notification_body = '".$body."' ";}else{$conditions.="";}
        if(isset($request['type'])){$type=$request['type'];$conditions.="AND notification_type = '".$type."' ";}else{$conditions.="";}
        if(isset($request['opened'])){$opened=$request['opened'];$conditions.="AND notification_opened = '".$opened."' ";}else{$conditions.="";}
        if(isset($request['viewed'])){$viewed=$request['viewed'];$conditions.="AND notification_viewed = '".$viewed."' ";}else{$conditions.="";}
        if(isset($request['recipient'])){$recipient=$request['recipient'];$conditions.="AND notification_recipient = '".$recipient."' ";}else{$conditions.="";}
        if(isset($request['sender'])){$sender=$request['sender'];$conditions.="AND notification_sender = '".$sender."' ";}else{$conditions.="";}
        if(isset($request['object'])){$object=$request['object'];$conditions.="AND notification_object = '".$object."' ";}else{$conditions.="";}

      }

      $sql =

        "SELECT
          notification_ID as id,
          notification_subject as subject,
          notification_type as type,
          notification_body as body,
          notification_opened as opened,
          notification_viewed as viewed,
          notification_recipient as recipient,
          notification_sender as sender,
          object_ID as object
        FROM notifications ";

      $sql .= "WHERE active = 1 ";
      $sql .= "AND app_ID = '" . $request['app'] . "' ";
      $sql .= $conditions;
      $sql .= $limit;

      //echo $sql;exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      //echo var_dump($result); exit;

      $response = array();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['notifications'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      //echo print_r($response);exit;

      return $response;

    }

    // Insert user details
    public function updateNotification($request) {

      $object = $this->getNotifications($request);

      //echo print_r($object);exit;
      //echo $request['requestor'];
      //echo $object['threads'][0]['author'];
      //exit;

      //
      if($request['recipient'] && $request['viewed']) {

        // sql command
        $sql = "UPDATE notifications SET notification_viewed=? WHERE notification_ID=? AND app_ID=?";

        //echo ($sql);exit;

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        //echo print_r($statement);exit;

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        $statement->bind_param("iss", $request['viewed'], $request['id'], $request['app']);

      }

      if($request['recipient'] && $request['opened']) {

        // sql command
        $sql = "UPDATE notifications SET notification_opened=? WHERE notification_ID=? AND app_ID=?";

        //echo ($sql);exit;

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        //echo print_r($statement);exit;

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        $statement->bind_param("iss", $request['opened'], $request['id'], $request['app']);

      }

      //echo print_r($statement); exit;

      $response = $statement->execute();

      //echo print_r($response)."YO!"; exit;

      return $response;

    }

    // Insert user details
    public function createCrowd($request) {

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO crowds SET crowd_ID=?, crowd_access=?, crowd_author=?, crowd_title=?, crowd_headline=?, crowd_administrators=?, crowd_contributors=?, crowd_images=?, app_ID=?";

      //echo ($sql);exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      //echo print_r($statement);exit;

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("sisssssss", $request['id'], $request['access'], $request['author'], $request['title'], $request['headline'], $request['administrators'], $request['contributors'], $request['images'], $request['app']);

      $response = $statement->execute();

      return $response;

    }

    // Select user information
    public function getCrowds($request) {

      //echo print_r($request);exit;

      //
      if(!empty($request['id'])) {

        $conditions = "AND crowd_ID = '" . $request['id'] . "' ";
        $limit = "LIMIT 1 ";

      }

      else {

        $conditions = "";
        $limit = "";

        if(isset($request['author'])){$author=$request['author'];$conditions.="AND crowd_author = '".$author."' ";}else{$conditions.="";}
        if(isset($request['title'])){$title=$request['title'];$conditions.="AND crowd_title = '".$title."' ";}else{$conditions.="";}
        if(isset($request['headline'])){$headline=$request['headline'];$conditions.="AND crowd_headline = '".$headline."' ";}else{$conditions.="";}
        if(isset($request['access'])){$access=$request['access'];$conditions.="AND crowd_access = '".$access."' ";}else{$conditions.="";}
        if(isset($request['administrators'])){$administrators=$request['administrators'];$conditions.="AND crowd_administrators = '".$administrators."' ";}else{$conditions.="";}
        if(isset($request['contributors'])){$contributors=$request['contributors'];$conditions.="AND crowd_contributors = '".$contributors."' ";}else{$conditions.="";}
        if(isset($request['images'])){$images=$request['images'];$conditions.="AND crowd_images = '".$images."' ";}else{$conditions.="";}
        if(isset($request['app'])){$app=$request['app'];$conditions.="AND app_ID = '".$app."' ";}else{$conditions.="";}

      }

      $sql =

        "SELECT
          crowd_ID as id,
          crowd_author as author,
          crowd_title as title,
          crowd_headline as headline,
          crowd_access as access,
          crowd_administrators as administrators,
          crowd_contributors as contributors,
          crowd_images as images,
          time_finished as time_finished
        FROM crowds ";

      $sql .= "WHERE active = 1 ";
      $sql .= "AND app_ID = '" . $request['app'] . "' ";
      $sql .= $conditions;
      $sql .= $limit;

      //echo $sql;exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      //echo var_dump($result); exit;

      $response = array();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['crowds'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      //echo print_r($response);exit;

      return $response;

    }

    // Insert user details
    public function updateCrowd($request) {

      $object = $this->getCrowds($request);

      if(!empty($request['title'])){$title=$request['title'];}else{$title=$object['title'];}
      if(!empty($request['headline'])){$headline=$request['headline'];}else{$headline=$object['headline'];}
      if(!empty($request['access'])){$access=$request['access'];}else{$access=$object['access'];}
      //if(!empty($request['contributors'])){$contributors=$request['contributors'];}else{$contributors=NULL;}
      //if(!empty($request['administrators'])){$administrators=$request['administrators'];}else{$administrators=NULL;}
      //if(!empty($request['images'])){$images=$request['images'];}else{$images=NULL;}

      //
      if(!empty($request['id']) && !empty($request['author'])) {

        // sql command
        $sql = "UPDATE crowds SET crowd_title={$title} crowd_title={$headline} crowd_title={$access} WHERE crowd_ID={$request['id']} AND app_ID={$request['app']}";

        //echo ($sql);exit;

        // store query result in $statement
        $statement = $this->conn->prepare($sql);

        //echo print_r($statement);exit;

        // if error
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind 5 param of type string to be placed in $sql command
        //$statement->bind_param("sss", $request['title'], $request['id'], $request['app']);

      }

      //echo print_r($request);exit;

      $response = $statement->execute();

      /*
      $json = ' {
        "type": "donut",
        "name": "Cake",
        "toppings": "[{ \"type\": \"Glazed\" }, { \"type\": \"Maple\" }]"
      }';

      $yummy = json_decode($json);
      $toppings = json_decode($yummy->toppings);

      echo $toppings[0]->type; //Glazed
      exit;
      */

      $sql = "SELECT JSON_EXTRACT(crowd_contributors, '$.contributors') AS contributors FROM crowds WHERE crowd_ID = '".$request['id']."' ";
      $query = mysqli_query($this->conn,$sql);
      $row = mysqli_fetch_array($query);
      $rows = $row;
      //echo $folks->contributors; exit;
      $contributors = json_decode($rows['contributors']);

      header('Content-Type: application/json');
      //echo print_r($folks);exit;
      //echo print_r($row);exit;
      //echo print_r($row[0]);exit;
      //echo print_r($row['contributors']);exit;
      foreach ($contributors as $item) {
        echo ($item);
      }
      exit;

      //echo $response; exit;

      return $response;

    }

    // Insert user details
    public function createMessage($request) {

      //echo print_r($request);exit;

      // sql command
      $sql = "INSERT INTO messages SET message_ID=?, message_body=?, message_images=?, profile_ID=?, thread_ID=?, app_ID=?";

      //echo ($sql);exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      //echo print_r($statement);exit;

      // if error
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("ssssss", $request['id'], $request['body'], $request['images'], $request['author'], $request['thread'], $request['app']);

      $response = $statement->execute();

      return $response;

    }

    // Select user information
    public function getMessages($request) {

      //echo print_r($request);exit;

      //
      if(!empty($request['id'])) {

        $conditions = "AND message_ID = '" . $request['id'] . "' ";
        $limit = "LIMIT 1 ";
        $order = "";

      }

      else {

        $conditions = "";
        $limit = "";
        $order = "ORDER BY time_finished ";

        if(isset($request['author'])){$author=$request['author'];$conditions.="AND author_ID = '".$author."' ";}else{$conditions.="";}
        if(isset($request['thread'])){$thread=$request['thread'];$conditions.="AND thread_ID = '".$thread."' ";}else{$conditions.="";}
        if(isset($request['body'])){$body=$request['body'];$conditions.="AND message_body LIKE '%".$body."%' ";}else{$conditions.="";}

      }

      $sql =

        "SELECT
          message_ID as id,
          profile_ID as author,
          thread_ID as thread,
          message_images as images,
          message_deleted as deleted,
          message_body as body
        FROM messages ";

      $sql .= "WHERE active = 1 ";
      $sql .= "AND app_ID = '" . $request['app'] . "' ";
      $sql .= $conditions;
      $sql .= $order;
      $sql .= $limit;

      //echo $sql;exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      //echo var_dump($result); exit;

      $response = array();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['messages'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      //echo print_r($response);exit;

      return $response;

    }

    // Insert user details
    public function createFollowship($request) {

      //
      if($this->isPublic($request['recipient'])==2) {

        // sql command
        $sql = "INSERT INTO followships SET followship_ID=?, followship_sender=?, followship_recipient=?, followship_status='accepted', app_ID=? ";
        //echo $sql;exit;

      }

      else {

        // sql command
        $sql = "INSERT INTO followships SET followship_ID=?, followship_sender=?, followship_recipient=?, followship_status='pending', app_ID=? ";
        //echo $sql;exit;

      }

      //echo $sql;exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      // if error
      if (!$statement) {

        throw new Exception($statement->error);

      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("ssss", $request['id'], $request['sender'], $request['recipient'], $request['app']);

      //echo print_r($statement); exit;

      $response = $statement->execute();

      //echo print_r($response); exit;

      return $response;

    }

    // Insert user details
    public function updateFollowship($request) {

      /*
      echo $request['status'];
      echo $request['id'];
      echo $request['recipient'];
      exit;
      */

      // sql command
      $sql = "UPDATE followships SET followship_status=? WHERE followship_ID=? AND followship_recipient=? AND active = 1";
      //echo $sql;exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      // if error
      if (!$statement) {
        throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      $statement->bind_param("sss", $request['status'], $request['id'], $request['recipient']);

      //echo print_r($statement); exit;

      $response = $statement->execute();

      //echo print_r($response)."YO!"; exit;

      return $response;

    }

    // Select person information
    public function getFollowships($key, $value, $abbreviated = NULL) {

      //echo print_r($value);exit;

      //
      $response = array();

      //
      switch($key) {

        //
        case 'ID':

          //echo $value;exit;
          //
          //
          $conditions = " AND followship_ID = '" . $value . "' ";
          $limit = " LIMIT 1";

          // declare array to store selected information
          $response = array();

          // sql JOIN
          $sql =

            "SELECT
              followship_ID as id,
              followship_recipient as recipient,
              followship_sender as sender,
              followship_status as status,
              time_finished as timestamp
            FROM followships ";

          $sql .= "WHERE active = 1 ";
          $sql .= $conditions;
          $sql .= "ORDER BY time_finished ASC";
          $sql .= $limit;

          break;

          //echo $sql; exit;

        //
        case 'profile':

          //
          $sql =
            "SELECT
              followship_ID as id,
              followship_recipient as recipient,
              followship_sender as sender,
              followship_status as status
            FROM followships
            WHERE followship_recipient = '" . $value . "'
            AND active = 1 ";
          //echo $sql;exit;
          break;

      }

      //echo $sql; exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['followships'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Select person information
    public function getFollowers($request, $abbreviated = NULL) {

      //echo print_r($value);exit;

      //
      $response = array();

      //
      $sql =
        "SELECT
          followship_ID as id,
          followship_recipient as recipient,
          followship_sender as sender,
          followship_status as status
        FROM followships
        WHERE followship_recipient = '" . $request['recipient'] . "'
        AND active = 1
      ";

      //echo $sql; exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['followers'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Select person information
    public function getFollowings($request, $abbreviated = NULL) {

      //echo print_r($value);exit;

      //
      $response = array();

      //
      $sql =
        "SELECT
          followship_ID as id,
          followship_recipient as recipient,
          followship_sender as sender,
          followship_status as status
        FROM followships
        WHERE followship_sender = '" . $request['sender'] . "'
        AND active = 1
      ";

      //echo $sql; exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['followings'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Select person information
    public function getProfiles($key, $value, $abbreviated = NULL) {

      //echo print_r($value); echo __FUNCTION__; exit;

      //
      /*
      if($this->isPublic($request['recipient'])==2) {

        // sql command
        $sql = "INSERT INTO followships SET followship_ID=?, followship_sender=?, followship_recipient=?, followship_status='accepted', app_ID=? ";
        //echo $sql;exit;

      }
      */

      //
      $response = array();

      //
      switch($key) {

        //
        case 'ID':

          //echo $value;exit;
          //
          //
          $conditions = " AND profile_ID = '" . $value . "' ";
          $limit = " LIMIT 1";

          // declare array to store selected information
          $response = array();

          // sql JOIN
          $sql =

            "SELECT
              profile_ID as id,
              profile_images as images,
              profile_bio as bio,
              profile_headline as headline,
              profile_access as access,
              user_ID as user
            FROM profiles ";

          $sql .= "WHERE active = 1 ";
          $sql .= $conditions;
          $sql .= $limit;

          break;

          //echo $sql; exit;

        //
        case 'profile':

          //
          $sql = "SELECT followship_ID as id, followship_recipient as recipient, followship_sender as sender, followship_status as status FROM followships WHERE followship_recipient = '" . $value . "' AND active = 1 ";
          break;
          //echo $sql;exit;

      }

      //echo $sql; exit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['profile'] = $row;

      }

      $person = $this->getPerson('profile', $value);
      //echo print_r($person); exit;

      //$q = '';
      $q['category'] = "topic";
      $q['author'] = $value;

      $response['profile']['name'] = $person['first_name'] . ' ' . $person['last_name'];
      $response['profile']['username'] = $person['alias'];
      $response['profile']['posts'] = $this->getCount('posts',$value);
      $response['profile']['followers'] = $this->getCount('followers',$value);
      $response['profile']['following'] = $this->getCount('following',$value);
      $response['profile']['topics'] = $this->getCategories($q);
      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Save email confiramtion message's token
    public function saveToken($type, $person_ID, $body, $app_ID) {

      $token_ID = $this->generate_ID('token');

      /*
      echo $type;
      echo $person_ID;
      echo $body;
      echo $app_ID;
      echo $token_ID;
      exit;
      */

      // sql statement
      $sql = "INSERT INTO tokens SET token_type=?, token_ID=?, person_ID=?, token_body=?, app_ID=?";

      // prepare statement to be executed
      $statement = $this->conn->prepare($sql);

      //echo $sql; exit;

      // error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind paramateres to sql statement
      $statement->bind_param("sssss", $type, $token_ID, $person_ID, $body, $app_ID);

      // launch / execute and store feedback in $response
      $response = $statement->execute();

      return $response;

    }

    //
    public function generate_ID($type) {

      $length = 9;

      // some characters
      $characters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";

      // get length of characters string
      $charactersLength= strlen($characters);

      $ID = $type;

      // generate random char from $characters every time until it is less than $charactersLength
      for ($i = 0; $i < $length; $i++) {
          $ID .= $characters[rand(0, $charactersLength-1)];
      }

      return $ID;

    }

    // Get ID of user via $emailToken he received via email's $_GET
    function getToken($token) {

      $response = array();

      // sql statement
      $sql = "SELECT person_ID FROM tokens WHERE token_body = '" . $token . "' AND active = 1";

      // launch sql statement
      $result = $this->conn->query($sql);

      // if $result is not empty ant storing some content
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

        // content from $result convert to assoc array and store in $row
        $row = $result->fetch_array(MYSQLI_ASSOC);

        //
        if (!empty($row)) {

            $response = $row;

        }

      }

      return $response;

    }

    // Get ID of user via $emailToken he received via email's $_GET
    public function isPublic($profile) {

      $response = array();

      // sql statement
      $sql = "SELECT profile_access as access FROM profiles WHERE profile_ID = '{$profile}' AND active = 1 LIMIT 1 ";

      //echo $sql; exit;
      // launch sql statement
      $result = $this->conn->query($sql);

      // if $result is not empty ant storing some content
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

        // content from $result convert to assoc array and store in $row
        $row = $result->fetch_array(MYSQLI_ASSOC);

        //
        if (!empty($row)) {

            $response = $row;

        }

      }

      return $response['access'];

    }

    // Change status of emailConfirmation column
    function emailConfirmationStatus($status, $id) {

      $sql = "UPDATE persons SET active = ? WHERE person_ID=?";
      $statement = $this->conn->prepare($sql);

      if (!$statement) {
          throw new Exception($statement->error);
      }

      $statement->bind_param("is", $status, $id);

      $response = $statement->execute();

      return $response;

    }

    // Delete token once email is confirmed
    function deleteToken($token) {

      $sql = "DELETE FROM tokens WHERE token_body = ?";
      $statement = $this->conn->prepare($sql);

      if (!$statement) {
          throw new Exception($statement->error);
      }

      $statement->bind_param("s", $token);

      $response = $statement->execute();

      return $response;

    }

    // Get full user information
    public function getCount($type,$profile) {

      // declare array to store all information we got
      $response = array();

      //
      switch($type) {

        //
        case 'posts':

          //
          $sql = "SELECT count(*) as `count` FROM posts WHERE active = 1 AND author_ID = '{$profile}' ";

          break;

        //
        case 'followers':

          //
          $sql = "SELECT count(*) as `count` FROM followships WHERE active = 1 AND followship_status = 'accepted' AND followship_recipient = '{$profile}' ";
          //echo $sql; exit;
          break;

        //
        case 'following':

          //
          $sql = "SELECT count(*) as `count` FROM followships WHERE active = 1 AND followship_status = 'accepted' AND followship_sender = '{$profile}' ";

          break;

        //
        default:

          //

          break;
      }

      // sql statement

      // execute / query $sql
      $result = $this->conn->query($sql);
      //$result = $results->fetch_array(MYSQLI_ASSOC);

      // if we got some result
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

          // assign result to $row as assoc array
          $row = $result->fetch_array(MYSQLI_ASSOC);

          // if assigned to $row. Assign everything $returnArray
          if (!empty($row)) {
            $response = $row;
          }
      }

      //echo json_encode($response); exit;

      return $response['count'];

    }

    // Get full user information
    public function getCountofUhh($username) {

      // declare array to store all information we got
      $returnArray = array();

      // sql statement
      $sql_id = "SELECT id FROM profiles WHERE username='" . $username . "'";
      $result_id = $this->conn->query($sql_id);
      $result = $result_id->fetch_array(MYSQLI_ASSOC);
      //echo json_encode($row); exit;

      $sql = "SELECT count(*) as count FROM postings WHERE author = '" . $result['id'] . "' AND text LIKE '%uhh%' ";

      // execute / query $sql
      $result = $this->conn->query($sql);
      //$result = $results->fetch_array(MYSQLI_ASSOC);

      // if we got some result
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

          // assign result to $row as assoc array
          $row = $result->fetch_array(MYSQLI_ASSOC);

          // if assigned to $row. Assign everything $returnArray
          if (!empty($row)) {
            $returnArray = $row;
          }
      }

      //echo json_encode($returnArray); exit;

      return $returnArray;

    }

    // Get full user information
    public function getCountofUmm($username) {

      // declare array to store all information we got
      $returnArray = array();

      // sql statement
      $sql_id = "SELECT id FROM profiles WHERE username='" . $username . "'";
      $result_id = $this->conn->query($sql_id);
      $result = $result_id->fetch_array(MYSQLI_ASSOC);
      //echo json_encode($row); exit;

      $sql = "SELECT count(*) as count FROM postings WHERE author = '" . $result['id'] . "' AND text LIKE '%umm%' ";

      // execute / query $sql
      $result = $this->conn->query($sql);
      //$result = $results->fetch_array(MYSQLI_ASSOC);

      // if we got some result
      if ($result != null && (mysqli_num_rows($result) >= 1)) {

          // assign result to $row as assoc array
          $row = $result->fetch_array(MYSQLI_ASSOC);

          // if assigned to $row. Assign everything $returnArray
          if (!empty($row)) {
            $returnArray = $row;
          }
      }

      //echo json_encode($returnArray); exit;

      return $returnArray;

    }

    // Updating password via link we got on via 'reset password email'
    public function updatePassword($id, $password, $salt) {

      $sql = "UPDATE users SET user_password=?, user_salt=? WHERE user_ID=?";
      $statement = $this->conn->prepare($sql);

      if (!$statement) {
          throw new Exception($statement->error);
      }

      $statement->bind_param("sss", $password, $salt, $id);

      $response = $statement->execute();

      return $response;

    }

    // Saving ava path in db
    function updateAvaPath($path, $id) {

        // sql statement
        $sql = "UPDATE profiles SET ava=? WHERE id=?";

        // prepare to be executed
        $statement = $this->conn->prepare($sql);

        // error occured
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // bind parameters to sql statement
        $statement->bind_param("si", $path, $id);

        // assign execution result to $response
        $response = $statement->execute();

        return $response;

    }

    // Select user information with Id
    public function selectUserViaID($id) {

      $returArray = array();

      // sql command
      $sql = "SELECT * FROM profiles WHERE id='".$id."'";

      // assign result we got from $sql to $result var
      $result = $this->conn->query($sql);

      // if we have at least 1 result returned
      if ($result != null && (mysqli_num_rows($result) >= 1 )) {

          // assign results we got to $row as associative array
          $row = $result->fetch_array(MYSQLI_ASSOC);

          if (!empty($row)) {
              $returArray = $row;
          }

      }

      return $returArray;

    }

    // Insert post in database
    public function createPost($request) {

      //echo print_r($request); exit;

      $request['id'] = $this->generate_ID('post');

        // sql statement
      $sql = "INSERT INTO posts SET post_ID=?,post_detail=?,author_ID=?,host_ID=?,post_access=?,post_latitude=?,post_longitude=?,post_altitude=?,post_topic=?,post_place=?,post_stage=?,app_ID=?";

      // prepare sql to be executed
      $statement = $this->conn->prepare($sql);

      // error occured
      if (!$statement) {
        throw new Exception($statement->error);
      }

      $new_post = date("h:i:sa")." | ".$request['detail'];

      // binding param in place of "?"
      $statement->bind_param("ssssiiiissss",$request['id'],$new_post,$request['author'],$request['host'],$request['access'],$request['latitude'],$request['longitude'],$request['altitude'],$request['topic'],$request['place'],$request['stage'],$request['app']);

      // execute statement and assign result of execution to $response
      $response = $statement->execute();

      //
      $this->prepareTrend($post,$post_ID);

      return $response;

    }

    // Insert trend in database
    public function prepareTrend($post,$object_ID) {

      //echo print_r($post); exit;

      $trend_ID = $this->generate_ID('trend');

      $trend['id']=$trend_ID;
      $trend['author']=$post['author'];
      $trend['app']=$post['app'];
      $trend['object']=$object_ID;

      $stopWords = "a about above across after again against all almost alone along already
       also although always among am an and another any anybody anyone anything anywhere are
       area areas around as ask asked asking asks at away b back backed backing backs be became
       because become becomes been before began behind being beings best better between big
       both but by c came can cannot case cases certain certainly clear clearly come could
       d did differ different differently do does done down down downed downing downs during
       e each early either end ended ending ends enough even evenly ever every everybody
       everyone everything everywhere f face faces fact facts far felt few find finds first
       for four from full fully further furthered furthering furthers g gave general generally
       get gets give given gives go going good goods got great greater greatest group grouped
       grouping groups h had has have having he her here herself high high high higher
         highest him himself his how however i im if important in interest interested interesting
       interests into is it its itself j just k keep keeps kind knew know known knows
       large largely last later latest least less let lets like likely long longer
       longest m made make making man many may me member members men might more most
       mostly mr mrs much must my myself n necessary need needed needing needs never
       new new newer newest next no nobody non noone not nothing now nowhere number
       numbers o of off often old older oldest on once one only open opened opening
       opens or order ordered ordering orders other others our out over p part parted
       parting parts per perhaps place places point pointed pointing points possible
       present presented presenting presents problem problems put puts q quite r
       rather really right right room rooms s said same saw say says second seconds
       see seem seemed seeming seems sees several shall she should show showed
       showing shows side sides since small smaller smallest so some somebody
       someone something somewhere state states still still such sure t take
       taken than that the their them then there therefore these they thing
       things think thinks this those though thought thoughts three through
           thus to today together too took toward turn turned turning turns two
       u under until up upon us use used uses v very w want wanted wanting
       wants was way ways we well wells went were what when where whether
       which while who whole whose why will with within without work
       worked working works would x y year years yet you young younger
       youngest your yours z lol haha omg hey ill iframe wonder else like
             hate sleepy reason for some little yes bye choose";

      //Convert stop words into array - split at white space
      $stopWords = preg_split("/[\s,]+/", $stopWords);

      //
      $body_array = preg_split("/\s+/", $post['text']);

      //echo $post['text'];exit;
      $capture_text = implode(" ", $body_array);

      // Remove all punctionation
      $no_punctuation = preg_replace("/[^a-zA-Z 0-9]+/", "", $capture_text);

      //echo print_r($no_punctuation); exit;

      // Predict whether user is posting a url. If so, do not check for trending words
      if(strpos($no_punctuation, "height") === false && strpos($no_punctuation, "width") === false
        && strpos($no_punctuation, "http") === false && strpos($no_punctuation, "youtube") === false) {

        //Convert users note (with punctuation removed) into array - split at white space
        $keywords = preg_split("/[\s,]+/", $no_punctuation);

        //echo print_r($keywords); exit;

        //
        foreach($stopWords as $value) {

          //echo print_r($stopWords); exit;

          //
          foreach($keywords as $key => $value2) {

            //echo print_r($keywords); exit;

            //
            if(strtolower($value) == strtolower($value2)) {

              //echo print_r($value);

              $keywords[$key] = "";

            }

          }

        }

        //
        foreach ($keywords as $value) {

          /*
          if($value != '') {
              $sql = "INSERT INTO trends (trend_ID,trend_body,object_ID,author_ID,app_ID)
          VALUES ('83','try now','wait a minutes','exxon','83838383')";
          mysqli_query($this->conn,$sql);
          }
          */

          //echo print_r($keywords); exit;
          //echo print_r($value); exit;

          $this->insertTrend(lcfirst($value),$trend);
          //echo $i; $i++;exit;

        }

      }

    }

    //
    public function insertTrend($term,$trend) {

      //echo var_dump($term);
      //echo print_r($trend);
      //exit;

      //
      if($term != '') {

        //echo "YI"; exit;

        //$sql = "INSERT INTO trends SET trend_ID=$trend['id'],trend_body={$term},object_ID={$trend['object']},author_ID={$trend['author']},app_ID={$trend['app']}";
        $sql = "INSERT INTO trends (trend_ID,trend_body,object_ID,author_ID,app_ID)
        VALUES ('{$trend["id"]}','{$term}','{$trend["object"]}','{$trend["author"]}','{$trend["app"]}')";
        //echo $sql;exit;
        mysqli_query($this->conn,$sql);

        /*
        return;
        // sql statement
        $sql = "INSERT INTO trends SET trend_ID=?,trend_body=?,object_ID=?,author_ID=?,app_ID=?";
        //echo $sql; exit;
        // prepare sql to be executed
        $statement = $this->conn->prepare($sql);

        // error occured
        if (!$statement) {
          throw new Exception($statement->error);
        }

        // binding param in place of "?"
        $statement->bind_param("sssss",$trend['id'],lcfirst($term),$trend['object'],$trend['author'],$trend['app']);

        // execute statement and assign result of execution to $response
        $statement->execute();
        */

      }

    }

    //
    public function clean($object) {
      return $object;
    }

    // Select person information
    public function getPosts($request, $abbreviated = false) {

      //echo print_r($request); exit;

      //
      $response = array();

      if(!empty($request['id'])) {
        //
        $sql = "SELECT
            post_ID AS id,
            post_images AS images,
            post_detail AS detail,
            post_author_closed AS author_closed,
            post_deleted AS deleted,
            post_access AS access,
            post_stage AS stage,
            post_latitude AS latitude,
            post_longitude AS longitude,
            post_altitude AS altitude,
            post_topic AS topic,
            post_place AS place,
            host_ID AS host,
            author_ID AS author,
            app_ID as app,
            time_finished as timestamp,
            active
          FROM posts
          WHERE post_ID = '" . $request['id'] . "'
          {$conditions}
          AND active = 1
          LIMIT 1
        ";

        //echo $sql;exit;
      }

      elseif(!empty($request['author'])) {

        //
        $conditions = '';
        if(!empty($request['detail'])) {$conditions .= "AND post_detail LIKE '%" . $request['detail'] . "%' ";}
        if(!empty($request['stage'])) {$conditions .= "AND post_stage LIKE '%" . $request['stage'] . "%' ";}
        if(!empty($request['topic'])) {$conditions .= "AND post_topic LIKE '%" . $request['topic'] . "%' ";}
        if(!empty($request['place'])) {$conditions .= "AND post_place LIKE '%" . $request['place'] . "%' ";}
        //echo $conditions; exit;
        //
        $sql = "SELECT
            post_ID AS id,
            post_images AS images,
            post_detail AS detail,
            post_author_closed AS author_closed,
            post_deleted AS deleted,
            post_access AS access,
            post_stage AS stage,
            post_latitude AS latitude,
            post_longitude AS longitude,
            post_altitude AS altitude,
            post_topic AS topic,
            post_place AS place,
            host_ID AS host,
            author_ID AS author,
            app_ID as app,
            time_finished as timestamp,
            DATE_FORMAT(`time_finished`, '%a, %b %D') as date,
            active
          FROM posts
          WHERE author_ID = '" . $request['author'] . "'
          {$conditions}
          AND active = 1
          ORDER BY time_finished DESC

        ";

        //echo $sql; exit;

      }

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {

        throw new Exception($statement->error);

      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response['posts'][] = $row;

      }

      return $response;

    }

    // Select unique categories in posts + user information made by user with relevant $id
    public function getCategories($request) {

      //echo __FUNCTION__; echo print_r($request); exit;

      //
      $category = $request['category'];
      if(!empty($request['criteria'])){$criteria = $request['criteria'];}
      $author = $request['author'];

      $conditions =  "AND author_ID = '{$author}' ";

      //
      if(!empty($criteria)) {

        $conditions .= "AND post_{$category} LIKE '%{$criteria}%' ";}else{$conditions.=NULL;

      }

      $columns = "DISTINCT COUNT(*) as count, DATE_FORMAT(`time_finished`, '%a, %b %D') as date, post_topic as topic, post_place as place";

      //
      switch($category) {

        //
        case 'topic':

          $sql = "SELECT {$columns}
          	FROM posts
          	WHERE active = 1
            {$conditions}
          	GROUP BY date, {$category}
          	ORDER BY time_finished DESC
          ";

          //echo $sql . __FUNCTION__; exit;

          break;

        //
        case 'place':

          $sql = "SELECT {$columns}
            FROM posts
            WHERE active = 1
            {$conditions}
            GROUP BY date, {$category}
            ORDER BY time_finished DESC
          ";

          echo $sql . __FUNCTION__; exit;

          break;

        //
        default:

          //

          break;

      }

      //echo $sql . __FUNCTION__; exit;

      // declare array to store selected information
      $response = array();

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      //echo var_dump($statement); exit;

      // error ocured
      if (!$statement) {

        throw new Exception($statement->error);

      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

        $response[] = $row;

      }

      return $response;

    }

    // Select all posts + user information made by user with relevant $id
    public function getComments($request) {

      //
      if(isset($request['id'])) {

        $conditions = "AND " . substr($domain,0,-1) . "_ID = '" . $request['id'] . "' ";
        $limit = " LIMIT 1";

      }

      else {

        $conditions = "";
        $limit = "";

        if(isset($request['author'])){$author=$this->clean($request['author']);$conditions.="AND profile_ID = '".$author."' ";}else{$conditions.="";}
        if(isset($request['text'])){$text=$this->clean($request['text']);$conditions.="AND ".substr($domain,0,-1)."_text LIKE '%".$text."%' ";}else{$conditions.="";}
        if(isset($request['thread'])){$thread=$this->clean($request['thread']);$conditions.="AND ".substr($domain,0,-1)."_thread = '".$thread."' ";}else{$conditions.="";}
        if(isset($request['object'])){$object=$this->clean($request['object']);$conditions.="AND object_ID = '".$object."' ";}else{$conditions.="";}
        if(isset($request['app'])){$app=$this->clean($request['app']);$conditions.="AND app_ID = '".$app."' ";}else{$conditions.="";}

      }

      // declare array to store selected information
      $response = array();

      // sql JOIN
      $sql =

        "SELECT
          comment_ID as id,
          comment_text as text,
          comment_thread as thread,
          object_ID as object,
          app_ID as app,
          profile_ID as author,
          time_finished as timestamp
        FROM comments ";

      $sql .= "WHERE active = 1 ";
      $sql .= $conditions;
      $sql .= "ORDER BY time_finished ASC";
      $sql .= $limit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

          $response['comments'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Select all posts + user information made by user with relevant $id
    public function selectComments($post_id) {

      // declare array to store selected information
      $returnArray = array();

      // sql JOIN
      $sql = "SELECT * FROM comments WHERE post_id = '{$post_id}' ORDER by date_added DESC";

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {
          $returnArray[] = $row;
      }

      return $returnArray;

    }

    // Insert comment in database
    public function insertComment($post_body, $posted_by, $posted_to, $date_added, $removed, $post_id) {

      // sql statement
      $sql = "INSERT INTO comments SET post_body=?, posted_by=?, posted_to=?, date_added=?, removed=?, post_id=?";

      // prepare sql to be executed
      $statement = $this->conn->prepare($sql);

      // error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // binding param in place of "?"
      $statement->bind_param("ssssss", $post_body, $posted_by, $posted_to, $date_added, $removed, $post_id);

      // execute statement and assign result of execution to $response
      $response = $statement->execute();

      return $response;

    }

    // Insert comment in database
    public function createComment($request) {

      $ID = $this->generate_ID('comment');

      // sql statement
      $sql = "INSERT INTO comments
        SET
          comment_ID=?,
          comment_text=?,
          comment_thread=?,
          object_ID=?,
          profile_ID=?,
          app_id=?
        "
      ;

      // prepare sql to be executed
      $statement = $this->conn->prepare($sql);

      // error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // binding param in place of "?"
      $statement->bind_param("ssssss", $ID, $request['text'], $request['thread'], $request['object'], $request['author'], $request['app']);

      // execute statement and assign result of execution to $response
      $response = $statement->execute();

      return $response;

    }

    // Select all posts + user information made by user with relevant $id
    public function getLikes($request) {

      //
      if(isset($request['id'])) {

        $conditions = "AND like_ID = '" . $request['id'] . "' ";
        $limit = " LIMIT 1";

      }

      else {

        $conditions = "";
        $limit = "";

        if(isset($request['profile'])){$profile=$this->clean($request['profile']);$conditions.="AND profile_ID = '".$profile."' ";}else{$conditions.="";}
        if(isset($request['object'])){$object=$this->clean($request['object']);$conditions.="AND object_ID = '".$object."' ";}else{$conditions.="";}
        if(isset($request['type'])){$type=$this->clean($request['type']);$conditions.="AND type_ID = '".$type."' ";}else{$conditions.="";}
        if(isset($request['app'])){$app=$this->clean($request['app']);$conditions.="AND app_ID = '".$app."' ";}else{$conditions.="";}

      }

      // declare array to store selected information
      $response = array();

      // sql JOIN
      $sql =

        "SELECT
          like_ID as id,
          like_type as type,
          profile_ID as author,
          object_ID as object,
          app_ID as app,
          time_finished as timestamp
        FROM likes ";

      $sql .= "WHERE active = 1 ";
      $sql .= $conditions;
      $sql .= "ORDER BY time_finished ASC";
      $sql .= $limit;

      // prepare to be executed
      $statement = $this->conn->prepare($sql);

      // error ocured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // execute sql
      $statement->execute();

      // result we got in execution
      $result = $statement->get_result();

      // each time append to $returnArray new row one by one when it is found
      while ($row = $result->fetch_assoc()) {

          $response['likes'][] = $row;

      }

      $response['count'] = mysqli_num_rows($result); // derive count of records after query run
      $response['html'] = "[]"; // create HTML attribute for later use

      return $response;

    }

    // Insert comment in database
    public function createLike($request) {

      $ID = $this->generate_ID('like');

      // sql statement
      $sql = "INSERT INTO likes
        SET
          like_ID=?,
          like_type=?,
          profile_ID=?,
          object_ID=?,
          app_id=?
        "
      ;

      // prepare sql to be executed
      $statement = $this->conn->prepare($sql);

      // error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // binding param in place of "?"
      $statement->bind_param("sssss", $ID, $request['type'], $request['author'], $request['object'], $request['app']);

      // execute statement and assign result of execution to $response
      $response = $statement->execute();

      return $response;

    }

    // Insert user details
    public function updateLike($request) {

      if(!empty($request['id'])) {
        if(!empty($request['active']) && $request['active'] == 1) {
          //$sql = "UPDATE likes SET active = IF(active=1, 0, 1)"; // TOGGLE ACTION
          $sql = "UPDATE likes SET active = 0 WHERE active = 1 AND like_ID='" . $request['id'] . "' AND profile_ID = '" . $request['author']. "'";
          echo $sql;exit;
        }
        elseif(!empty($request['type'])) {
          $sql = "UPDATE likes SET like_type = '" . $request['type'] . "' WHERE active = 1 AND like_ID='" . $request['id'] . "' AND profile_ID='" . $request['author'] . "' AND app_ID='" . $request['app'] . "'" ;
        }
      }

      // sql command
      //$sql = "UPDATE likes SET active = 0 WHERE like_ID=? AND profile_ID=? AND active = 1 AND app_ID=?";
      //echo $sql;exit;

      // store query result in $statement
      $statement = $this->conn->prepare($sql);

      // if error
      if (!$statement) {
        throw new Exception($statement->error);
      }

      // bind 5 param of type string to be placed in $sql command
      //$statement->bind_param("sss", $request['status'], $request['id'], $request['recipient']);

      //echo print_r($statement); exit;

      $response = $statement->execute();

      //echo print_r($response)."YO!"; exit;

      return $response;

    }

    // Delete post according to passed uui
    public function deletePost($uuid) {

      // sql statement to be executed
      $sql = "DELETE FROM postings WHERE uuid = ?";

      // prepare to be executed after binded params in place of ?
      $statement = $this->conn->prepare($sql);

      // error occured while preparation or sql statement
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // bind param in place of ? and assign var
      $statement->bind_param("s", $uuid);
      $statement->execute();

      // assign numb of affected rows to $response, to see did deleted or not
      $response = $statement->affected_rows;

      return $response;

    }

    // Search / Select user
    public function selectUsers($word, $username) {

        // var to store all returned inf from db
        $returnArray = array();

        // sql statement to be executed if not entered word
        $sql = "SELECT id, username, email, fullname, ava FROM profiles WHERE NOT username = '".$username."'";

        // if word entered alter sql statement for wider search
        if (!empty($word)) {
            $sql .= " AND ( username LIKE ? OR fullname LIKE ? )";
        }

        // prepare to be executed as soon as vars are binded
        $statement = $this->conn->prepare($sql);

        // error occured
        if (!$statement) {
            throw new Exception($statement->error);
        }

        // if word entered bind params
        if (!empty($word)) {
            $word = '%' . $word . '%'; // %bob%
            $statement->bind_param("ss", $word, $word);
        }

        // execute statement
        $statement->execute();

        // assign returned results to $result var
        $result = $statement->get_result();

        // every time when we convert $result to assoc array append to $row
        while ($row = $result->fetch_assoc()) {

            // store all append $rows in $returnArray
            $returnArray[] = $row;
        }

        // feedback result
        return $returnArray;

    }

    // Update user function in our db via $id
    public function updateUser($username, $fullname, $email, $id) {

      // sql statement
      $sql = "UPDATE profiles SET username=?, fullname=?, email=? WHERE id=?";

      // prepare to be executed as soon as we bind param in place of '?'
      $statement = $this->conn->prepare($sql);

      // if error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // binding param in place of '?'
      $statement->bind_param("sssi", $username, $fullname, $email, $id);

      // assign execution result to $response
      $response = $statement->execute();

      return $response;

    }

    // Last time a user logged into the app
    public function updateLastLogin($user_ID) {

      // sql statement
      $sql = "UPDATE users SET user_lastlogin = NOW() WHERE user_ID=?";

      // prepare to be executed as soon as we bind param in place of '?'
      $statement = $this->conn->prepare($sql);

      // if error occured
      if (!$statement) {
          throw new Exception($statement->error);
      }

      // binding param in place of '?'
      $statement->bind_param("s", $profile_ID);

      // assign execution result to $response
      $response = $statement->execute();

      return $response;

    }

  }

?>
