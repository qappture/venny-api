<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //

      break;

    // GET POSTS
    case 'GET':

      //echo print_r($_GET); exit;

      // STEP 2.2 Select posts + user related to $id
      $categories = $access->getCategories($_GET);

      //echo print_r($categories); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($categories)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        $response['results'] = $categories;
        $response['event'] = new_ID('event');
        $response['process'] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //

      break;

    //
    case 'DELETE':

      //

      break;

    //
    default: header('Location: index.php');

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
