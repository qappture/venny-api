<?php

  // STEP 1. Build connection
  // Secure way to build conn
  $file = parse_ini_file(APP_ST_INI);   // accessing the file with connection infromation

  /*
  echo "Root: " . APP_ENV_ROOT . "<br/>";
  echo "Name: " . APP_ST_NAME . "<br/>";
  echo "Include: " . APP_ST_INCL . "<br/>";
  echo "INI: " . APP_ST_INI . "<br/>";
  echo APP_DB_HOST . "<br/>";
  echo APP_DB_NAME . "<br/>";
  echo APP_DB_USER . "<br/>";
  echo APP_DB_PASS . "<br/>";
  */

  // store in php var inf from ini var
  $db_host = APP_DB_HOST;
  $db_username = APP_DB_USER;
  $db_password = APP_DB_PASS;
  $db_name = APP_DB_NAME;

  // include access.php to call func from access.php file
  require 'access.php';
  $access = new access($db_host, $db_username, $db_password, $db_name);
  $access->connect();   // launch opend connection function

  // Catch request method...
  $method = $_SERVER['REQUEST_METHOD'];

  // Catch parameters of request...
  $request = explode("/", substr(@$_SERVER['PATH_INFO'], 1));

?>
