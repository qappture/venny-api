<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['sender']) || empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your request to follow did not go through at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['id'] = $access->generate_ID('followship');
      $request['sender'] = htmlentities($_REQUEST['sender']);
      $request['recipient'] = htmlentities($_REQUEST['recipient']);
      $request['app'] = htmlentities($_REQUEST['app']);

      //echo print_r($request);exit;

      // STEP 3. Insert user information
      $result = $access->createFollowship($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        // get current registered user information and store in $user
        $followship = $access->getFollowships('ID', $request['id']);

        //echo print_r($followship);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully requested";
        $response['id'] = $followship['followships']['id'];
        $response['sender'] = $followship['followships']['sender'];
        $response['recipient'] = $followship['followships']['recipient'];

        //
        $getEmail = $access->getPerson('profile',$followship['followships']['recipient']);
        //echo print_r($getEmail);exit;
        $gotEmail = $getEmail['email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "You have a new follower on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('newFollower');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{recipient}", $followship['recipient'], $template);
        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not follow at this time.";

      }

      break;

    // GET POSTS
    case 'GET':

      //
      if (empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your follows can not be retrieved at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['recipient'] = htmlentities($_REQUEST['recipient']);

      //echo print_r($_GET); exit;

      //echo $request['recipient']; exit;

      // STEP 2.2 Select posts + user related to $id
      $result = $access->getFollowships('profile',$request['recipient']);

      //echo print_r($result); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($result)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        $response['results'] = $result;
        $response['event'] = new_ID('event');
        $response['process'] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      if (empty($_REQUEST['sender']) || empty($_REQUEST['recipient'])) {

        $response['status'] = 400;
        $response['message'] = "Your request to follow did not go through at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      // Securing information and storing variables
      $request['id'] = htmlentities($_REQUEST['id']);
      //echo $request['id'];exit;
      $request['sender'] = htmlentities($_REQUEST['sender']);
      $request['recipient'] = htmlentities($_REQUEST['recipient']);
      $request['status'] = htmlentities($_REQUEST['status']);
      $request['app'] = htmlentities($_REQUEST['app']);

      //echo print_r($request);
      //echo print_r($request['id']);exit;

      // STEP 3. Insert user information
      $result = $access->updateFollowship($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        //echo $request['id']; exit;

        // get current registered user information and store in $user
        $result = $access->getFollowships('ID',$request['id']);

        //header('Content-Type: application/json');
        //echo json_encode($result);exit;

        // declare information to feedback to user of App as json
        $response['status'] = 200;
        $response['message'] = "Successfully {$request['status']}";
        $response['results'] = $result;

        //
        $getEmail = $access->getPerson('profile',$result[0]['sender']);

        //echo var_dump($getEmail);exit;
        $gotEmail = $getEmail['person_email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details['subject'] = "Your follow request was {$result[0]['status']} on Notearise";
        $details['to'] = $gotEmail;
        $details['organization'] = "Notearise";
        $details['sender'] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('followResponse');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{sender}", $result[0]['sender'], $template);
        $template = str_replace("{organization}", APP_NAME, $template);
        $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

        //echo print_r($template);exit;

        $details['body'] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response['status'] = 400;
        $response['message'] = "Could not follow at this time.";

      }

      break;

    //
    case 'DELETE':

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
