<?php

  //
  require 'core.php';

  $request = array();

  if(!empty($_REQUEST['id'])){$request['id']=strip_tags(htmlentities($_REQUEST['id']));}else{$request['id']=NULL;}
  if(!empty($_REQUEST['detail'])){$request['detail']=strip_tags(htmlentities($_REQUEST['detail']));}else{$request['detail']=NULL;}
  if(!empty($_REQUEST['access'])){$request['access']=$_REQUEST['access'];}else{$request['access']=NULL;}
  if(!empty($_REQUEST['author'])){$request['author']=htmlentities($_REQUEST['author']);}else{$request['author']=NULL;}
  if(!empty($_REQUEST['host'])){$request['host']=htmlentities($_REQUEST['host']);}else{$request['host']=NULL;}
  if(!empty($_REQUEST['latitude'])){$request['latitude']=$_REQUEST['latitude'];}else{$request['latitude']=NULL;}
  if(!empty($_REQUEST['longitude'])){$request['longitude']=$_REQUEST['longitude'];}else{$request['longitude']=NULL;}
  if(!empty($_REQUEST['altitude'])){$request['altitude']=$_REQUEST['altitude'];}else{$request['altitude']=NULL;}
  if(!empty($_REQUEST['topic'])){$request['topic']=htmlentities($_REQUEST['topic']);}else{$request['topic']=NULL;}
  if(!empty($_REQUEST['place'])){$request['place']=htmlentities($_REQUEST['place']);}else{$request['place']=NULL;}
  if(!empty($_REQUEST['stage'])){$request['stage']=htmlentities($_REQUEST['stage']);}else{$request['stage']=NULL;}
  if(!empty($_REQUEST['app'])){$request['app']=htmlentities($_REQUEST['app']);}else{$request['app']=NULL;}

  //echo $method; exit;

  //
  switch($method) {

    //
    case 'POST':

      //
      if (empty($_REQUEST['detail']) || empty($_REQUEST['author'])) {

        $response['status'] = 400;
        $response['message'] = "You CAN NOT post at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      if (!empty($_REQUEST['detail']) && !empty($_REQUEST['author'])) {

        // STEP 2.1 Pass POST / GET via html encryp and assign to vars
        //$id = htmlentities($_REQUEST["id"]);
        //$uuid = substr(md5(uniqid(microtime(true),true)),0,8);

        // STEP 2.4 Save path and other post details in db
        //echo $id . $uuid . $text . $path; exit;
        $access->createPost($request);

        $response['code'] = 200;
        $response['status'] = "SUCCESS";
        $response['message'] = "Post has been made successfully";
        $response['event'] = RAND();
        $response['process'] = RAND();

      }

      else {

      }

      break;

    // GET POSTS
    case 'GET':

      //
      //echo print_r($_GET); exit;

      // STEP 2.2 Select posts + user related to $id
      $posts = $access->getPosts($request);

      //echo print_r($posts); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($posts)) {

        $response['status'] = 200;
        $response['message'] = "SUCCESS";
        //$response['count'] = $posts['count'] . " of " . $posts['count'] . " " . $domain;
        //$response['html attributes"] = $posts['html'];
        $response['results'] = $posts;
        $response['event'] = RAND();
        $response['process'] = RAND();

      }

      break;

    //
    case 'PUT':

      break;

    //
    case 'DELETE':

      // DELETE
      if (!empty($_REQUEST['uuid']) && empty($_REQUEST['id'])) {

        // STEP 2.1 Get uuid of post and path to post picture passed to this php file via swift POST
        $uuid = htmlentities($_REQUEST["uuid"]);
        $path = htmlentities($_REQUEST["path"]);
        $author = htmlentities($_REQUEST["author"]);

        // STEP 2.2 Delete post according to uuid
        $result = $access->deletePost($uuid);

        if (!empty($result)) {

          $response['message'] = "Successfully deleted";
          $response['result'] = $result;

          // STEP 2.3 Delete file according to its path and if it exists
          if (!empty($path)) {

            // /Applications/XAMPP/xamppfiles/htdocs/Twitter/posts/46/image.jpg
            $path = str_replace("http://localhost/", "/Applications/XAMPP/xamppfiles/htdocs/", $path);

            // file deleted successfully
            if (unlink($path)) {
                $response['status'] = "1000";
            // could not delete file
            } else {
                $response['status'] = 400;
            }

          }

        }

        else {

          $response['message'] = "Could not delete post";

        }

      }

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
