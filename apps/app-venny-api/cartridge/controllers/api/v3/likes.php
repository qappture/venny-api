<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (!empty($_REQUEST["object"]) && !empty($_REQUEST["author"]) && !empty($_REQUEST["object"])) {

        // STEP 2.1 Pass POST / GET via html encryp and assign to vars
        //$id = htmlentities($_REQUEST["id"]);
        //$uuid = substr(md5(uniqid(microtime(true),true)),0,8);

        $request = array();

        if(!empty($_REQUEST["author"])){$request['author']=htmlentities($_REQUEST["author"]);}else{$request['author']=NULL;}
        if(!empty($_REQUEST["object"])){$request['object']=htmlentities($_REQUEST["object"]);}else{$request['object']=NULL;}
        if(!empty($_REQUEST["type"])){$request['type']=htmlentities($_REQUEST["type"]);}else{$request['type']=NULL;}
        if(!empty($_REQUEST["app"])){$request['app']=htmlentities($_REQUEST["app"]);}else{$request['app']=NULL;}

        // STEP 2.4 Save path and other post details in db
        //echo $id . $uuid . $text . $path; exit;
        $access->createLike($request);

        $response["code"] = 200;
        $response["status"] = "SUCCESS";
        $response["message"] = "Like has been made successfully";
        $response["event"] = RAND();
        $response["process"] = RAND();

      }

      else {

      }

      break;

    // GET POSTS
    case 'GET':

      //echo print_r($_GET); exit;

      // STEP 2.2 Select posts + user related to $id
      $likes = $access->getLikes($_GET);

      //echo print_r($posts); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($likes)) {

        $response["status"] = 200;
        $response["message"] = "SUCCESS";
        $response["count"] = $likes['count'] . " of " . $likes['count'] . " " . $domain;
        $response["html attributes"] = $likes['html'];
        $response["results"] = $likes['likes'];
        $response["event"] = new_ID('event');
        $response["process"] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      //
      if (empty($_REQUEST["id"]) || empty($_REQUEST['author'])) {

        $response["status"] = 400;
        $response["message"] = "Your like can not be updated at this time.";

        header('Content-Type: application/json');
        echo json_encode($response);

        return;

      }

      //
      $request = array();

      // Securing information and storing variables
      if(!empty($_REQUEST['id'])){$request['id']=clean($_REQUEST['id']);}else{$request['id']=NULL;}
      if(!empty($_REQUEST['author'])){$request['author']=clean($_REQUEST['author']);}else{$request['author']=NULL;}
      if(!empty($_REQUEST['type'])){$request['type']=clean($_REQUEST['type']);}else{$request['type']=NULL;}
      if(!empty($_REQUEST['object'])){$request['object']=clean($_REQUEST['object']);}else{$request['object']=NULL;}
      if(!empty($_REQUEST['active'])){$request['active']=clean($_REQUEST['active']);}else{$request['active']=NULL;} // No preview needed to begin a thread... only after the first message is sent
      if(!empty($_REQUEST['app'])){$request['app']=clean($_REQUEST['app']);}else{$request['app']=NULL;}

      //echo print_r($request);
      //echo print_r($request['id']);exit;

      // STEP 3. Insert user information
      $result = $access->updateLike($request);

      //echo print_r($result);exit;

      // successfully registered
      if ($result) {

        //echo print_r($result);exit;

        //echo $request['id']; exit;

        // get current registered user information and store in $user
        $result = $access->getLikes($request);

        //header('Content-Type: application/json');
        //echo json_encode($result);exit;
        //echo print_r($result);exit;

        // declare information to feedback to user of App as json
        $response["status"] = 200;
        $response["message"] = "Successful";
        $response["results"] = $result;

        //echo $result['threads'][0]['author'];exit;

        //
        $getEmail = $access->getPerson('profile',$request['author']);

        //echo var_dump($getEmail);exit;
        $gotEmail = $getEmail['person_email'];
        //echo $gotEmail; exit;

        // STEP 4. Emailing
        // include email.php
        require 'notification.php';

        // store all class in $email var
        $send_email = new notification();

        // refer emailing information
        $details = array();
        $details["subject"] = "Your like request was updated on Notearise";
        $details["to"] = $gotEmail;
        $details["organization"] = "Notearise";
        $details["sender"] = "hello@notearise.com";

        //echo print_r($details);exit;

        // access template file
        $template = $send_email->emailTemplate('updateLike');
        //echo print_r($template);exit;

        // replace {token} from confirmationTemplate.html by $token and store all content in $template var
        $template = str_replace("{author}", $request['author'], $template);
        $template = str_replace("{organization}", APP_NAME, $template);
        $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

        //echo print_r($template);exit;

        $details["body"] = $template;

        //echo print_r($details);exit;

        $send_email->send_email($details);

      }

      else {

        $response["status"] = 400;
        $response["message"] = "Could not update like at this time.";

      }

      break;

    //
    case 'DELETE':

      // DELETE

      break;

    //
    default: header("Location: http://www.notearise.com/index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
