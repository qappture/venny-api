<?php

  //
  require 'core.php';

  //
  switch($method) {

    //
    case 'POST':

      //
      if (!empty($_REQUEST["text"]) && !empty($_REQUEST["author"]) && !empty($_REQUEST["object"])) {

        // STEP 2.1 Pass POST / GET via html encryp and assign to vars
        //$id = htmlentities($_REQUEST["id"]);
        //$uuid = substr(md5(uniqid(microtime(true),true)),0,8);

        $request = array();

        if(!empty($_REQUEST["text"])){$request['text']=htmlentities($_REQUEST["text"]);}else{$request['text']=NULL;}
        if(!empty($_REQUEST["object"])){$request['object']=htmlentities($_REQUEST["object"]);}else{$request['object']=NULL;}
        if(!empty($_REQUEST["thread"])){$request['thread']=htmlentities($_REQUEST["thread"]);}else{$request['thread']=NULL;}
        if(!empty($_REQUEST["author"])){$request['author']=htmlentities($_REQUEST["author"]);}else{$request['author']=NULL;}
        if(!empty($_REQUEST["app"])){$request['app']=htmlentities($_REQUEST["app"]);}else{$request['app']=NULL;}

        // STEP 2.4 Save path and other post details in db
        //echo $id . $uuid . $text . $path; exit;
        $access->createComment($request);

        $response["code"] = 200;
        $response["status"] = "SUCCESS";
        $response["message"] = "Comment has been made successfully";
        $response["event"] = RAND();
        $response["process"] = RAND();

      }

      else {

      }

      break;

    // GET POSTS
    case 'GET':

      //echo print_r($_GET); exit;

      // STEP 2.2 Select posts + user related to $id
      $comments = $access->getComments($_GET);

      //echo print_r($posts); exit;

      // STEP 2.3 If posts are found, append them to $returnArray
      if (!empty($comments)) {

        $response["status"] = 200;
        $response["message"] = "SUCCESS";
        $response["count"] = $comments['count'] . " of " . $comments['count'] . " " . $domain;
        $response["html attributes"] = $comments['html'];
        $response["results"] = $comments['comments'];
        $response["event"] = new_ID('event');
        $response["process"] = new_ID('process');

      }

      break;

    //
    case 'PUT':

      break;

    //
    case 'DELETE':

      // DELETE
      if (!empty($_REQUEST["uuid"]) && empty($_REQUEST["id"])) {

        // STEP 2.1 Get uuid of post and path to post picture passed to this php file via swift POST
        $uuid = htmlentities($_REQUEST["uuid"]);
        $path = htmlentities($_REQUEST["path"]);
        $author = htmlentities($_REQUEST["author"]);

        // STEP 2.2 Delete post according to uuid
        $result = $access->deletePost($uuid);

        if (!empty($result)) {

          $returnArray["message"] = "Successfully deleted";
          $returnArray["result"] = $result;

          // STEP 2.3 Delete file according to its path and if it exists
          if (!empty($path)) {

            // /Applications/XAMPP/xamppfiles/htdocs/Twitter/posts/46/image.jpg
            $path = str_replace("http://localhost/", "/Applications/XAMPP/xamppfiles/htdocs/", $path);

            // file deleted successfully
            if (unlink($path)) {
                $returnArray["status"] = "1000";
            // could not delete file
            } else {
                $returnArray["status"] = 400;
            }

          }

        }

        else {

          $returnArray["message"] = "Could not delete post";

        }

      }

      break;

    //
    default: header("Location: index.php");

  }

  // STEP 3. Close connection
  $access->disconnect();

  // STEP 4. Feedback information
  header('Content-Type: application/json');

  echo json_encode($response);

?>
