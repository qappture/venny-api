<?php

  //
  require 'core.php';

  //
  if (empty($_REQUEST["first_name"]) || empty($_REQUEST["email"])) {

    $response["status"] = 400;
    $response["message"] = "Missing required information";

    header('Content-Type: application/json');
    echo json_encode($response);

    return;

  }

  $request = array();

  // Securing information and storing variables
  $request['id'] = $access->generate_ID('person');
  $request['first_name'] = htmlentities($_REQUEST['first_name']);
  $request['email'] = htmlentities($_REQUEST['email']);
  $request['app'] = htmlentities($_REQUEST['app']);
  if(!empty($_REQUEST['last_name'])){$request['last_name']=htmlentities($_REQUEST['last_name']);}else{$request['last_name']=NULL;}
  if(!empty($_REQUEST['phone'])){$request['phone']=htmlentities($_REQUEST['phone']);}else{$request['phone']=NULL;}

  // STEP 3. Insert user information
  $result = $access->createPerson($request);

  //echo json_encode($result); exit;

  // successfully registered
  if ($result) {

    // get current registered user information and store in $user
    $person = $access->getPerson('ID',$request['id']);

    //echo json_encode($person); exit;

    // declare information to feedback to user of App as json
    $response['status'] = 200;
    $response['message'] = "Successfully registered";
    $response['id'] = $person['id'];
    $response['first_name'] = $person['first_name'];
    $response['last_name'] = $person['last_name'];
    $response['email'] = $person['email'];
    $response['phone'] = $person['phone'];
    $response['entitlements'] = $person['entitlements'];

    // STEP 4. Emailing
    // include email.php
    require 'notification.php';

    // store all class in $email var
    $send_email = new notification();

    // store generated token in $token var
    $token = $send_email->generateToken(20);

    //echo $person['person_ID'];exit;

    // save inf in 'emailTokens' table
    $access->saveToken('email', $person['id'], $token, $app);

    // refer emailing information
    $details = array();
    $details['subject'] = "Email confirmation on Venny";
    $details['to'] = $request['email'];
    $details['organization'] = "Notearise";
    $details['sender'] = "hello@notearise.com";

    // access template file
    $template = $send_email->emailTemplate('createPerson');

    // replace {token} from confirmationTemplate.html by $token and store all content in $template var
    $template = str_replace("{token}", $token, $template);
    $template = str_replace("{organization}", APP_NAME, $template);
    $template = str_replace("{website}", APP_ENV_SRVR . APP_ST_NAME, $template);

    $details["body"] = $template;

    $send_email->send_email($details);

  }

  else {

    $response["status"] = 400;
    $response["message"] = "Could not subscribe with provided information";

  }

  // STEP 5. Close connection
  $access->disconnect();

  // STEP 6. JSON data
  header('Content-Type: application/json');
  echo json_encode($response);

?>
