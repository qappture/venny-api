# Venny

## MVP (Prototype & Alpha)

[Venny](https://www.venny.co/) is a multiple platform resource supporting an inclusive community of creative professionals

A barebones PHP app that makes use of the [Venny](https://www.venny.co/) social framework, which can easily be deployed to Heroku to build rich community experiences.

## Data Management

Here you'll find the latest list of MySQL database tables. We're using these tables as temporary data buckets in preparation of a NoSQL migration this summer for AI & ML usage (better Gradient scoring, faster APIs for public usage, etc.). These tables meet our MVP standards and will provide the ample testing data we need to inform the impending migration.

|Table|Scope|Description|
| ------------- | ------------- | ------------- |
|academics|gradient_1|Any formal education or training the user has completed|
|accomplishments|gradient_1|Honors awards or accolades of any kind|
|activities|gradient_1|Represents community involvement or social responsibilities participated in|
|answers|gradient_1|Questions to be asked of potential candidates or participants|
|categories|gradient_1|Represents industry categories in projects and experience|
|institutions|gradient_1|Institutions are created for non-profits and universities|
|interests|gradient_1|Interests represent special abilities and / or hobbies the member wants to share|
|members|gradient_1|Members represent persons who have decided to create system login credentials with Gradient in order to begin using our services.|
|opportunities|gradient_1|Opportunity would be considered an open position with an organization|
|organizations|gradient_1|Organizations represent companies created by partners and members|
|partners|gradient_1|Partners represent a paid membership which unlocks a number of content contributor features|
|projects|gradient_1|Projects represent the work of any user interested in sharing their accomplishments|
|questions|gradient_1|Questions to be asked of potential candidates or participants|
|requirements|gradient_1|Requirement added to an opportunity|
|responses|gradient_1|Responses are from participants who have answered the questions|
|responsibilities|gradient_1|Responsibility added to an opportunity|
|skills|gradient_1|Skills represent any career ability that a member would like to make publicly available.|
|surveys|gradient_1|Questions to be asked of potential candidates or participants|
|tags|gradient_1|Keywords associated with an opportunity|
|views|gradient_1|Every instance a member visits an object|
|comments|venny_1|Comments made by users in the public UI|
|friend_requests|venny_1|Added when users makde requests to other users|
|likes|venny_1|Likes made by users in the public UI / same base architecture as verifications|
|messages|venny_1|Message objects sent by a single user to another / soon to be depricated in favor of groups model|
|notifications|venny_1|Represent user actions against logged in user objects|
|posts|venny_1|Articles posted by users / will serve base architecture for "articles" in gradient_1|
|trends|venny_1|Word salad pulled from each post to later decipher content trends|
|users|venny_1|General user access / Soon to be depricated in favor of upgraded PERSON model|
|apps|venny_2|Apps represent applications and or software created by partners to make public (and regulated) use of the Gradient API|
|events|venny_2|Events tell the story of API calls|
|images|venny_2|Image references are stored in this database|
|persons|venny_2|Persons records are used as the bedrock for all user data and communications.|
|positions|venny_2|Positions represent the official titles you served in at a specific organization|
|profiles|venny_2|Profiles represent and reflect any publicly available information provided by a member.|
|tokens|venny_2|Tokens are tied to a partner app and provide stateless credentials to all API calls made on behalf of the app.|
|verifications|venny_2|Verifications are the lifeblood of the system. These are the affirmations given by other users to attest to another user's information|

* Keep in mind the migration may not be required as soon originally thought considering [FANN (Fast Artificial Neural Network)](http://php.net/manual/en/book.fann.php) could accommodate our initial AI & ML needs.
* Sensitive environment keys will need to be moved to a non-public directory for Beta launch.

## Deploying

Install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone git@github.com:heroku/gradient-test.git # or clone your own fork
$ cd com-gradient-www
$ heroku create
$ git push heroku master
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## API

The new Gradient TalentBank API offers more uptime and thousands of the most qualified candidates for your roles. Below you'll find each of the API resources with some sample data to make some test calls.

- [Gradient TalentBank API](https://documenter.getpostman.com/view/2396336/gradient-talentbank-api/RVu8gmuv)

Call us today to get premium access to our enterprise API.

* App token (NWU1MWQ4NzIwOTY0OGNjMTNkZWI1MjNiMjA1ZA==) to be replaced during any public beta testing.
* Official SSL certificate pending because of a person verfication blocker at Expidited SSL.

## Documentation

For more information about using this app please refer to the following resources:

- [Email Solution Provider - Mailgun](https://www.mailgun.com)
- [CDN - IMGix (Zebrafish Labs)](https://www.imgix.com/)
- [Cloud Storage - Amazon Web Services](https://aws.amazon.com)
- [Database - ClearDB MySQL](https://addons-sso.heroku.com/apps/d4bf4fce-70fa-4c24-86b9-be10a18d88b1/addons/488c7ab9-b4c6-42b7-8bb0-4e6d3bcda714)
- [Cloud Hosting - Heroku](https://dashboard.heroku.com/apps/gradient-test) This app is using hobby dynos and should be upgrade for public launch

# Notearise (Odyssey)

![Banner](https://notearise.com/assets/images/facebook/notearise-facebook-cover-photo.png)

Connect with friends and the world around you with Notearise.

* Take better notes than ever before.
* Discover more topics & discussions.
* Elevate your thoughts into meaningful conversations with friends.

- [Security](#security)
- [Architecture](#architecture)
- [Features](#features)
- [Installation](#installation)
- [Support](#support)

- - -

## Security

* SSL - Thawte

## Architecture

* Configuration
* PHP
* MySQL
* Host: MediaTemple
* AWS
* IP Address:
```
205.186.128.172
```

## Features

* User Authentication (Cookies & Sessions)
* Capture Notes
* Organize Notes
* Review Notes
* Make Friends
* Direct Messages
* Check Out Groups
* Receive Notifications
* Grow Network

## Installation

* ensure legacy sites runs successfully before running odyssey
* create DB dump
* match columns during migration
* ensure events are being logged with functions
* ensure trends are coming through
* ensure location is loading correctly
* include_path in PHP.ini
* configure cookies for production
* ensure DB is installed and passwords match
* check mailgun for production quota
* check mailgun for staging quota
* Change CHmod to accomodate profile image upload
* max file size for uploading images should match server
* Ensure terms & conditions / Cookie use & Privacy Policy don't need to be updated
* post_max_size | 8M (Default) | The maximum size in bytes of data that can be posted with the POST method. Typically, should be larger than upload_max_filesize and smaller than memory_limit. (http://php.net/manual/en/ini.core.php#ini.post-max-size)

## Testing

* End to End testing

## Deployment

* AWS

## Built Wit

* [ARBrush](https://github.com/laanlabs/ARBrush) - Quick demo of 3d drawing in ARKit using metal + SceneKit
* [ARBrush](https://github.com/laanlabs/ARBrush) - Quick demo of 3d drawing in ARKit using metal + SceneKit
* [ARBrush](https://github.com/laanlabs/ARBrush) - Quick demo of 3d drawing in ARKit using metal + SceneKit

## Contributing

* [ARBrush](https://github.com/laanlabs/ARBrush) - Quick demo of 3d drawing in ARKit using metal + SceneKit

## Versioning

We use [BitBucket](http://bitbucket.org) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/notearise/odyssey).

## Authors

* [Al Nolan](http://hustle.university)
* [Marcus A. Hill](https://www.linkedin.com/in/marcus-hill-382a05aa)
* Frantz Arty
* Javier Navarro
* J.W. Washington

## License

* [ARBrush](https://github.com/laanlabs/ARBrush) - Quick demo of 3d drawing in ARKit using metal + SceneKit

## Acknowledgments

* support@notearise.com

## Support

* [Slack](http://notearise.slack.com)
* [Facebook](http://facebook.com/notearise)
* [Twitter](http://twitter.com/notearise)
* [Email](support@notearise.com)

DEFINE A PROCFILE
https://devcenter.heroku.com/articles/getting-started-with-php#define-a-procfile

# Gradient

A barebones PHP app that makes use of the [Silex](http://silex.sensiolabs.org/) web framework, which can easily be deployed to Heroku.

[Gradient](http://test.gradient.global/) is a multiple platform resource supporting an inclusive community of creative professionals

This application supports the [Getting Started with PHP on Heroku](https://devcenter.heroku.com/articles/getting-started-with-php) article - check it out.

## Deploying

Install the [Heroku Toolbelt](https://toolbelt.heroku.com/).

```sh
$ git clone git@github.com:heroku/php-getting-started.git # or clone your own fork
$ cd php-getting-started
$ heroku create
$ git push heroku master
$ heroku open
```

or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using PHP on Heroku, see these Dev Center articles:

- [Getting Started with PHP on Heroku](https://devcenter.heroku.com/articles/getting-started-with-php)
- [PHP on Heroku](https://devcenter.heroku.com/categories/php)
